package com.abouelgoukh.sporteye.model;

import java.util.ArrayList;
import java.util.List;

import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;

/**
 * Created by User on 6/5/2017.
 */

public class ModelCart {
    private ArrayList<Product> cartItems = new ArrayList<>();
    private List<Product> favoritsItems = new ArrayList<>();

    public ArrayList<Product> getCartItems() {
        return cartItems;
    }
    public List<Product> getFavoritsItems() {
        return favoritsItems;
    }

    public void setCartItems(ArrayList<Product> cartItems) {
        this.cartItems = cartItems;
    }
    public void setFavoritsItems(ArrayList<Product> favoritsItems) {
        this.favoritsItems = favoritsItems;
    }


    public Product getProducts(int position){
        return cartItems.get(position);
    }

    public int getCartsize(){
        return cartItems.size();
    }

    public void setProducts(Product Products){
        cartItems.add(Products);
    }
    public void setFavoritsProducts(Product Products){
        favoritsItems.add(Products);
    }
    public void deleteProducts(Product Products){
        cartItems.remove(Products);
    }

    public boolean CheckProductInCart(Product aproduct){
        return cartItems.contains(aproduct);
    }
}