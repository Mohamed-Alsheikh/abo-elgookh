package com.abouelgoukh.sporteye.model;

import java.util.ArrayList;
import java.util.List;

import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;

/**
 * Created by Mohamed.Alsheikh on 6/8/2017.
 */

public class ModelCompare {
    private ArrayList<Product> compareItems = new ArrayList<>();
    private List<Product> favoritsItems = new ArrayList<>();

    public ArrayList<Product> getCompareItems() {
        return compareItems;
    }
    public List<Product> getFavoritsItems() {
        return favoritsItems;
    }

    public void setCartItems(ArrayList<Product> cartItems) {
        this.compareItems = cartItems;
    }
    public void setFavoritsItems(ArrayList<Product> favoritsItems) {
        this.favoritsItems = favoritsItems;
    }


    public Product getProducts(int position){
        return compareItems.get(position);
    }

    public int getCartsize(){
        return compareItems.size();
    }

    public void setProducts(Product Products){
        compareItems.add(Products);
    }
    public void setFavoritsProducts(Product Products){
        favoritsItems.add(Products);
    }
    public void deleteProducts(Product Products){
        compareItems.remove(Products);
    }

    public boolean CheckProductInCart(Product aproduct){
        return compareItems.contains(aproduct);
    }
}
