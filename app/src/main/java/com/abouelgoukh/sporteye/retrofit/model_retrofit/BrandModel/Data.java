package com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel;

/**
 * Created by Mohamed.Alsheikh on 6/14/2017.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("brands")
    @Expose
    private List<Brand> brands = null;

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

}