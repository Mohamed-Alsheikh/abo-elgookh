package com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 7/4/2017.
 */

public class ProfileAnswerReponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("profile")
    @Expose
    private Profile profile;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}
