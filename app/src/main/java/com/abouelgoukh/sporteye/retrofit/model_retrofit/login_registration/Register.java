package com.abouelgoukh.sporteye.retrofit.model_retrofit.login_registration;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohamed.Alsheikh on 6/15/2017.
 */

public class Register {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("apiToken")
    @Expose
    private String apiToken;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Register withStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Register withApiToken(String apiToken) {
        this.apiToken = apiToken;
        return this;
    }

}
