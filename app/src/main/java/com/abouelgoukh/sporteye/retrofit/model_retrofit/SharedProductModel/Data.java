package com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel;

/**
 * Created by Mohamed.Alsheikh on 6/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class Data {

    @SerializedName("product")
    @Expose
    private List<Product> product = null;

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

}