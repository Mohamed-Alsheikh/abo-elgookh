package com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel;

/**
 * Created by Mohamed.Alsheikh on 6/12/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Product implements Parcelable {

    //Transient
    public boolean isCartImageChanged;

    public boolean isCartImageChanged() {
        return isCartImageChanged;
    }

    public void setCartImageChanged(boolean imageChanged) {
        isCartImageChanged = imageChanged;
    }

    public boolean isCompareImageChanged;

    public boolean isCompareImageChanged() {
        return isCompareImageChanged;
    }

    public void setCompareImageChanged(boolean isCompareImageChanged) {
       this.isCompareImageChanged = isCompareImageChanged;
    }

    private Product(Parcel in) {
        super();
        this.id = in.readInt();
        this.name = in.readString();
        this.image = in.readString();
        this.price = in.readDouble();

        this.mount = in.readInt();
        this.description = in.readString();
        this.type = in.readInt();
        this.isFavorated=in.readInt();
        this.isCartImageChanged=in.readByte() != 0;
        this.isCompareImageChanged=in.readByte() != 0;
        this.isfavoritImageChanged=in.readByte() != 0;
            this.price_after_discount = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(getId());
        parcel.writeString(getName());
        parcel.writeString(getImage());
        parcel.writeDouble(getPrice());

        parcel.writeInt(getMount());
        parcel.writeString(getDescription());
        parcel.writeInt(getType());
        parcel.writeInt(getIsFavorated());
        parcel.writeByte((byte) (isCartImageChanged ? 1 : 0));
        parcel.writeByte((byte) (isCompareImageChanged ? 1 : 0));
        parcel.writeByte((byte) (isfavoritImageChanged ? 1 : 0));
        if(price_after_discount!=null) {
            parcel.writeDouble(getPrice_after_discount());
        }
//        parcel.writeInt(getStasus());




    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @SerializedName("isfavoritImageChanged")
    @Expose(serialize = false)
    public boolean isfavoritImageChanged;

    public boolean isfavoritImageChanged() {
        return isfavoritImageChanged;
    }

    public void setFavoritImageChanged(boolean imageChanged) {
        isfavoritImageChanged = imageChanged;
    }
    //************************************************************************

    public Product() {
        super();
    }


    @SerializedName("status")
    @Expose()
    private Integer status;

    @SerializedName("data")
    @Expose(serialize = false)
    private Data data;


    public Integer getStasus() {
        return status;
    }

    public void setStasus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    @SerializedName("id")
    @Expose
    private Integer id;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @SerializedName("description")
    @Expose(serialize = false)
    private String description;


    @SerializedName("type")
    @Expose
    private Integer type;

    @SerializedName("mount")
    @Expose()
    private int mount=1;


    private String name;

    @SerializedName("image")
    @Expose(serialize = false)
    private String image;


    @Expose(serialize = false)
    private Double price;

    @SerializedName("price_after_discount")
    @Expose(serialize = false)
    private Double price_after_discount;

    @SerializedName("isFavorated")
    @Expose(serialize = false)
    private Integer isFavorated;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getPrice_after_discount() {
        return price_after_discount;
    }

    public void setPrice_after_discount(Double price_after_discount) {
        this.price_after_discount = price_after_discount;
    }

    public Integer getIsFavorated() {
        return isFavorated;
    }

    public void setIsFavorated(Integer isFavorated) {
        this.isFavorated = isFavorated;
    }

    public int getMount() {
        return mount;
    }

    public void setMount(int mount) {
        this.mount = mount;
    }


}
