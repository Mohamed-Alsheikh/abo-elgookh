package com.abouelgoukh.sporteye.retrofit.model_retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohamed.Alsheikh on 6/18/2017.
 */

public class SimpleResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @Expose
    private Integer id;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}