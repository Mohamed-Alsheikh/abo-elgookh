package com.abouelgoukh.sporteye.retrofit.model_retrofit;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 7/10/2017.
 */

public class PagesModel {


        @SerializedName("page_id")
        @Expose
        private Integer pageId;
        @SerializedName("product_name")
        @Expose
        private String title;
        @SerializedName("body")
        @Expose
        private String body;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("meta_desc")
        @Expose
        private String metaDesc;
        @SerializedName("meta_keys")
        @Expose
        private String metaKeys;
        @SerializedName("addedin")
        @Expose
        private String addedin;
        @SerializedName("addedby")
        @Expose
        private String addedby;
        @SerializedName("lastupdate")
        @Expose
        private String lastupdate;
        @SerializedName("lastupdateby")
        @Expose
        private String lastupdateby;
        @SerializedName("hits")
        @Expose
        private Integer hits;
        @SerializedName("published")
        @Expose
        private Integer published;
        @SerializedName("notes")
        @Expose
        private String notes;

        public Integer getPageId() {
            return pageId;
        }

        public void setPageId(Integer pageId) {
            this.pageId = pageId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getMetaDesc() {
            return metaDesc;
        }

        public void setMetaDesc(String metaDesc) {
            this.metaDesc = metaDesc;
        }

        public String getMetaKeys() {
            return metaKeys;
        }

        public void setMetaKeys(String metaKeys) {
            this.metaKeys = metaKeys;
        }

        public String getAddedin() {
            return addedin;
        }

        public void setAddedin(String addedin) {
            this.addedin = addedin;
        }

        public String getAddedby() {
            return addedby;
        }

        public void setAddedby(String addedby) {
            this.addedby = addedby;
        }

        public String getLastupdate() {
            return lastupdate;
        }

        public void setLastupdate(String lastupdate) {
            this.lastupdate = lastupdate;
        }

        public String getLastupdateby() {
            return lastupdateby;
        }

        public void setLastupdateby(String lastupdateby) {
            this.lastupdateby = lastupdateby;
        }

        public Integer getHits() {
            return hits;
        }

        public void setHits(Integer hits) {
            this.hits = hits;
        }

        public Integer getPublished() {
            return published;
        }

        public void setPublished(Integer published) {
            this.published = published;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }

    }