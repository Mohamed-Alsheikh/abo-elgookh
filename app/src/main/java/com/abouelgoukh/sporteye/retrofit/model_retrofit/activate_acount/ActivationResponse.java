package com.abouelgoukh.sporteye.retrofit.model_retrofit.activate_acount;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActivationResponse {

        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("user")
        @Expose
        private User user;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

    }