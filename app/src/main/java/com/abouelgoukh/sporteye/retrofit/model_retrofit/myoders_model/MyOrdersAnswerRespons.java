package com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model;

/**
 * Created by User on 7/2/2017.
 */



import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class MyOrdersAnswerRespons {

        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("orders")
        @Expose
        private List<Order> orders = null;

    @SerializedName("order")
    @Expose
    private Order order = null;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }


    public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public List<Order> getOrders() {
            return orders;
        }

        public void setOrders(List<Order> orders) {
            this.orders = orders;
        }

    }