package com.abouelgoukh.sporteye.retrofit;


/**
 * Created by Mohamed.Alsheikh on 6/12/2017.
 */

public class ApiUtils {
    public static final String BASE_URL = "HTTPs://sporteyestore.com/";

    public static ApiServiceInterface getRetrofitObject() {
        return RetrofitClient.getClient(BASE_URL).create(ApiServiceInterface.class);
    }
}