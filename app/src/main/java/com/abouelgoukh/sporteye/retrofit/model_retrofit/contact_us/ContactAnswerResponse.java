package com.abouelgoukh.sporteye.retrofit.model_retrofit.contact_us;

/**
 * Created by User on 7/20/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactAnswerResponse {


    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("contacts")
    @Expose
    private Contact contacts;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Contact getContacts() {
        return contacts;
    }

    public void setContacts(Contact contacts) {
        this.contacts = contacts;
    }

}