package com.abouelgoukh.sporteye.retrofit;

/**
 * Created by Mohamed.Alsheikh on 6/12/2017.
 */

import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Address;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.GalleryAnswerRespone;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.AddressAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.BrandAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.ProductAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.activate_acount.ActivationResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.ads_model.AdsAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.contact_us.ContactAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.login_registration.Login;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.login_registration.Register;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model.MyOrdersAnswerRespons;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model.ProfileAnswerReponse;

public interface ApiServiceInterface {

    /***
     *
     * getHome service
     * ***/


    @POST("/api/getHome")
    Call<ProductAnswerResponse> getHome(@Query("lang") String lan);

    @POST("/api/getHome")
    Call<ProductAnswerResponse> getHome(@Query("apiToken") String apiToken, @Query("lang") String lan);

    @POST("/api/getFavoratedList")
    Call<ProductAnswerResponse> getFavoratedList(@Query("apiToken") String apiToken);

    @POST("/api/getBicyclesBrands")
    Call<BrandAnswerResponse> getBicyclesBrands();

    @POST("/api/getAccessoriesBrands")
    Call<BrandAnswerResponse> getAccessoriesBrands();

    @POST("/api/getOtherProductsCatogery")
    Call<BrandAnswerResponse> getOtherProducts(@Query("lang") String language);

    @POST("/api/getOtherProductsSubCatogery")
    Call<BrandAnswerResponse> getOtherSubProducts(@Query("brand_id") int brand_id, @Query("lang") String language);

    @POST("/api/getAccessories")
    Call<ProductAnswerResponse> getAccessories(@Query("brand_id") int brand_id, @Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/getAccessories")
    Call<ProductAnswerResponse> getAccessories(@Query("brand_id") int brand_id, @Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("apiToken") String apiToken, @Query("lang") String lang);

    @POST("/api/getOtherProducts")
    Call<ProductAnswerResponse> getOtherProduct(@Query("brand_id") int brand_id, @Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/getOtherProducts")
    Call<ProductAnswerResponse> getOtherProduct(@Query("brand_id") int brand_id, @Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("apiToken") String apiToken, @Query("lang") String lang);

    @POST("/api/getOtherProducts")
    Call<ProductAnswerResponse> getAllOtherProduct(@Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/getOtherProducts")
    Call<ProductAnswerResponse> getAllOtherProduct(@Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("apiToken") String apiToken, @Query("lang") String lang);

    @POST("/api/getBicycles")
    Call<ProductAnswerResponse> getBicycles(@Query("brand_id") int brand_id, @Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/getBicycles")
    Call<ProductAnswerResponse> getBicycles(@Query("brand_id") int brand_id, @Query("page") int page, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("apiToken") String apiToken, @Query("lang") String lang);

    @POST("/api/register")
    Call<Register> register(@Query("device")String deviceID, @Query("email") String email, @Query("mobile") String mobile, @Query("password") String password, @Query("username") String userName);

    @POST("/api/login")
    Call<Login> login(@Query("mobile") String mobile, @Query("password") String password);

    @POST("/api/favoratePruduct")
    Call<SimpleResponse> getfavoratePruduct(@Query("product_id") int product_id, @Query("apiToken") String apiToken, @Query("type") int status);

    @POST("/api/favorateAccessory")
    Call<SimpleResponse> favorateaccessory(@Query("accessory_id") int accessory_id, @Query("apiToken") String apiToken, @Query("type") int status);

    @POST("/api/favorateOtherPruduct")
    Call<SimpleResponse> favorateOtherProduct(@Query("product_id") int accessory_id, @Query("apiToken") String apiToken, @Query("type") int status);

    @POST("/api/getUserAddresses")
    Call<AddressAnswerResponse> getUserAddresses(@Query("apiToken") String apitoken, @Query("lang") String lang);

    @Headers("Content-type: application/json")
    @POST("/api/makeOrder")
    Call<SimpleResponse> makeOrder(
            @Body JsonObject apiToken
    );

    @Headers("Content-type: application/json")
    @POST("/api/updateOrderStatus")
    Call<SimpleResponse> updateOrderStatus(
            @Body JsonObject apiToken
    );

    @POST("/api/getAllGovernments")
    Call<Address> getAllGovernments(@Query("apiToken") String apitoken, @Query("lang") String lang
    );

    @POST("/api/addAddress")
    Call<SimpleResponse> addAddress(@Query("apiToken") String apiToken, @Query("address") String addressTxt, @Query("government_id") int governmentId);

    @POST
    Call<GalleryAnswerRespone> getProductDetails(@Url String Url, @Query("product_id") int product_id, @Query("lang") String lang);

    @POST("/api/getAccessoryDetails")
    Call<GalleryAnswerRespone> getAccessoryDetails(@Query("accessory_id") int product_id, @Query("lang") String lang);

    @POST("/api/getOtherProductDetails")
    Call<GalleryAnswerRespone> getOtherProductDetails(@Query("product_id") int product_id, @Query("lang") String lang);

    @POST("/api/getAds")
    Call<AdsAnswerResponse> getAds();

    @POST("/api/getOrders")
    Call<MyOrdersAnswerRespons> getMyOrderts(@Query("apiToken") String apiToken);

    @POST("/api/orderDetails")
    Call<MyOrdersAnswerRespons> getOrderdetails(@Query("apiToken") String apiToken, @Query("order_id") int order_id, @Query("lang") String en);

    @POST("/api/getProfile")
    Call<ProfileAnswerReponse> getProfileDetails(@Query("apiToken") String apiToken, @Query("lang") String lang);

    @POST("/api/deleteAddress")
    Call<AddressAnswerResponse> deleteAddress(@Query("apiToken") String apiToken, @Query("address_id") int address_id);

    @POST("/api/resetPassword")
    Call<SimpleResponse> resetPassword(@Query("apiToken") String apiToken, @Query("old_password") String old_password, @Query("new_password") String new_password);

    @POST("/api/editProfile")
    Call<SimpleResponse> resetProfileData(@Query("apiToken") String apiToken, @Query("username") String str_username, @Query("email") String str_email, @Query("mobile") String str_mobile);

    @Multipart
    @POST("/api/addMaintence")
    Call<SimpleResponse> maintainance(@Part("apiToken") RequestBody apiToken, @Part("product_name") RequestBody product_name, @Part("problem") RequestBody problem, @Part MultipartBody.Part imagebody);

    @Multipart
    @POST("/api/addMaintence")
    Call<SimpleResponse> maintainanced(@Part("apiToken") RequestBody apiToken, @Part("product_name") RequestBody product_name, @Part("problem") RequestBody problem);

    @POST("/api/search")
    Call<ProductAnswerResponse> getSearchList(@Query("apiToken") String apitoken, @Query("search") String bi);

    @POST("/api/getAllBicycles")
    Call<ProductAnswerResponse> getAllBicycles(@Query("apiToken") String apitoken, @Query("page") int currentPage, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/getAllBicycles")
    Call<ProductAnswerResponse> getAllBicycles(@Query("page") int currentPage, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/getAllAccessories")
    Call<ProductAnswerResponse> getAllAccessories(@Query("apiToken") String apitoken, @Query("page") int currentPage, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/getAllAccessories")
    Call<ProductAnswerResponse> getAllAccessories(@Query("page") int currentPage, @Query("price_from") String price_from, @Query("price_to") String price_to, @Query("size_from") String size_from, @Query("size_to") String size_to, @Query("lang") String lang);

    @POST("/api/contactUs")
    Call<ContactAnswerResponse> ContactUsService();


    @POST("/api/feedback")
    Call<SimpleResponse> FeedBackService(@Query("name") String yourname, @Query("mobile") String yourmobile, @Query("title") String yourmessaagetile, @Query("message") String yourfeedback);
    @POST("/api/userVerified")
    Call<ActivationResponse> ActivationResponseService(@Query("code")String verificationCode,@Query("mobile") String phone);

    @POST("/api/forgetPassword")
    Call<SimpleResponse> forgetPassword(@Query("email")String email);


// @GET("/answers?order=desc&sort=activity&site=stackoverflow")
//    Call<ProductAnswerResponse> getAnswers(@Query("tagged") String tags);
//    @GET("api/{email}/{password}")
//    Call<Login> authenticate(@Path("email") String email, @Path("password") String password);
//
//    @POST("api/{email}/{password}")
//    Call<Login> registration(@Path("email") String email, @Path("password") String password);
}