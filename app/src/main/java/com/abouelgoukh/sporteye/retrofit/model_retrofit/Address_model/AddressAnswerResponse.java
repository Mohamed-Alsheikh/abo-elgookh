package com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model;

/**
 * Created by Mohamed.Alsheikh on 6/20/2017.
 */



import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AddressAnswerResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("address")
    @Expose
    private List<Address> address = null;


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

}