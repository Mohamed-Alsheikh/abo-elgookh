package com.abouelgoukh.sporteye.retrofit.model_retrofit.activate_acount;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("apiToken")
    @Expose
    private String apiToken;
    @SerializedName("verified")
    @Expose
    private Integer verified;

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

}