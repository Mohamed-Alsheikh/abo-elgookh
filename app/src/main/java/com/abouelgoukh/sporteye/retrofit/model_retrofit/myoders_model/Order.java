package com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model;

/**
 * Created by User on 7/2/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;

public class Order implements Parcelable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("address")
    @Expose
    private String address;

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    @SerializedName("government")
    @Expose
    private String government;

    @SerializedName("mobile_first")
    @Expose
    private String mobileFirst;

    @SerializedName("mobile_second")
    @Expose
    private String mobileSecond;

    @SerializedName("order_status")
    @Expose
    private Integer orderStatus;

    @SerializedName("order_code")
    @Expose
    private String orderCode;

    @SerializedName("code")
    @Expose
    private String code;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("shipping_cost")
    @Expose
    private String shippingCost;

    @SerializedName("shipping_time")
    @Expose
    private String shippingTime;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("payment_method_status")
    @Expose
    private String paymentMethodStatus;
    @SerializedName("product")
    @Expose
    private List<Product> product = null;
    @SerializedName("total_price")
    @Expose
    private Integer totalPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileFirst() {
        return mobileFirst;
    }

    public void setMobileFirst(String mobileFirst) {
        this.mobileFirst = mobileFirst;
    }

    public String getMobileSecond() {
        return mobileSecond;
    }

    public void setMobileSecond(String mobileSecond) {
        this.mobileSecond = mobileSecond;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodStatus() {
        return paymentMethodStatus;
    }

    public void setPaymentMethodStatus(String paymentMethodStatus) {
        this.paymentMethodStatus = paymentMethodStatus;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }
    public String getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingTime() {
        return shippingTime;
    }

    public void setShippingTime(String shippingTime) {
        this.shippingTime = shippingTime;
    }


public Order(int id,int status,String code,String date){
       this.id=id;
       this.status=status;
       this.code=code;
       this.date=date;
    }
    private Order(Parcel in) {
        super();
        this.id = in.readInt();
        this.address = in.readString();
        this.government = in.readString();
        this.orderCode = in.readString();
        this.code= in.readString();
        this.paymentMethod = in.readString();
        this.mobileFirst = in.readString();
        this.mobileSecond = in.readString();
        this.orderStatus = in.readInt();
        this.status = in.readInt();


    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(getId());
        parcel.writeString(getAddress());
        parcel.writeString(getGovernment());
        parcel.writeString(getOrderCode());
        parcel.writeString(getCode());
        parcel.writeString(getPaymentMethod());
        parcel.writeString(getMobileFirst());
        parcel.writeString(getMobileSecond());
        parcel.writeInt(getOrderStatus());
        parcel.writeInt(getStatus());
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

}