package com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model;

/**
 * Created by Mohamed.Alsheikh on 6/20/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Address {

    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("government")
    @Expose
    private Government government;

    @SerializedName("governments")
    @Expose
    private List<Government> governments = null;

    public List<Government> getGovernments() {
        return governments;
    }

    public void setGovernments(List<Government> governments) {
        this.governments = governments;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Government getGovernment() {
        return government;
    }

    public void setGovernment(Government government) {
        this.government = government;
    }

}