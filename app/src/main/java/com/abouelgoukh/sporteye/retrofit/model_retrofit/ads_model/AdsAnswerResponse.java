package com.abouelgoukh.sporteye.retrofit.model_retrofit.ads_model;

/**
 * Created by Mohamed.Alsheikh on 7/2/2017.
 */



import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AdsAnswerResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("ads")
    @Expose
    private List<Ad> ads = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Ad> getAds() {
        return ads;
    }

    public void setAds(List<Ad> ads) {
        this.ads = ads;
    }

}