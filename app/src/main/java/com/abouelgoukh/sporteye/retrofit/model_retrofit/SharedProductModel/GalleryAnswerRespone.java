package com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mohamed.Alsheikh on 7/2/2017.
 */

public class GalleryAnswerRespone {


        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("gallery")
        @Expose
        private List<Gallery> gallery = null;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public List<Gallery> getGallery() {
            return gallery;
        }

        public void setGallery(List<Gallery> gallery) {
            this.gallery = gallery;
        }


}
