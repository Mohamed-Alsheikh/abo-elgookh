package com.abouelgoukh.sporteye.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

/**
 * This class is used to change your application locale and persist this change for the next time
 * that your app is going to be used.
 * <p/>
 * You can also change the locale of your application on the fly by using the setLocale method.
 * <p/>
 * Created by gunhansancar on 07/10/15.
 */
public class LocaleHelper {

	private static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";

	public static void onAttach(Context context) {
		String lang = getPersistedData(context, Locale.getDefault().getLanguage());
		 updateResources(context, lang);
	}

	public static void onAttach(Context context, String defaultLanguage) {
		String lang = getPersistedData(context, defaultLanguage);
		 updateResources(context, lang);
	}

	public static String getLanguage(Context context) {
		return getPersistedData(context, Locale.getDefault().getLanguage());
	}


	public static void updateResources(Context context, String language) {
		persist(context, language);
		Locale newLocale = new Locale(language);
		Locale.setDefault(newLocale);


		Resources activityRes = context.getResources();
		Resources applicationRes = context.getApplicationContext().getResources();

		Configuration activityConf = activityRes.getConfiguration();
		Configuration applicationConf = applicationRes.getConfiguration();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			activityConf.setLocale(newLocale);
			applicationConf.setLocale(newLocale);

		} else {
			activityConf.locale = newLocale;
			applicationConf.locale = newLocale;

		}

		activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());
		applicationRes.updateConfiguration(applicationConf, applicationRes.getDisplayMetrics());


	}
	private static String getPersistedData(Context context, String defaultLanguage) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		return preferences.getString(SELECTED_LANGUAGE, defaultLanguage);
	}

	public static void persist(Context context, String language) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putString(SELECTED_LANGUAGE, language);
		editor.apply();
	}



	@SuppressWarnings("deprecation")
	private static Context updateResourcesLegacy(Context context, String language) {
		Locale locale = new Locale(language);
		Locale.setDefault(locale);

		Resources resources = context.getResources();

		Configuration configuration = resources.getConfiguration();
		configuration.locale = locale;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			configuration.setLayoutDirection(locale);
		}

		resources.updateConfiguration(configuration, resources.getDisplayMetrics());

		return context;
	}
}