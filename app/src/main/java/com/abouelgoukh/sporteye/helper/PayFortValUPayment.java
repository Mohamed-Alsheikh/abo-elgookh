package com.abouelgoukh.sporteye.helper;

/**
 * Created by User on 8/15/2017.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager;
import com.payfort.sdk.android.dependancies.base.FortInterfaces;
import com.payfort.sdk.android.dependancies.models.FortRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.abouelgoukh.sporteye.Interfaces.IPaymentRequestCallBack;
import com.abouelgoukh.sporteye.model.PaymentProduct;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;

public class PayFortValUPayment {

    //Request key for response
    public static final int RESPONSE_GET_TOKEN = 111;
    public static final int RESPONSE_CustomerNOT_REGISTERD_FOR_VALU = 666;
    public static final int RESPONSE_PURCHASE = 222;
    public static final int RESPONSE_PURCHASE_CANCEL = 333;
    public static final int RESPONSE_PURCHASE_SUCCESS = 444;
    public static final int RESPONSE_PURCHASE_FAILURE = 555;

    //WS params
    private final static String KEY_MERCHANT_IDENTIFIER = "merchant_identifier";
    private final static String KEY_MERCHANT_REFERENCE = "merchant_reference";
    private final static String KEY_SERVICE_COMMAND = "service_command";
    private final static String KEY_DEVICE_ID = "device_id";
    private final static String KEY_LANGUAGE = "language";
    private final static String KEY_ACCESS_CODE = "access_code";
    private final static String KEY_SIGNATURE = "signature";

    //Commands
    public final static String AUTHORIZATION = "AUTHORIZATION";
    public final static String PURCHASE = "PURCHASE";
    private final static String SDK_TOKEN = "SDK_TOKEN";
    private final static String CUSTOMER_VERIFY = "CUSTOMER_VERIFY";
    private final static String OTP_VERIFY = "OTP_VERIFY";
    private final static String OTP_GENERATE = "OTP_GENERATE";

    //Test token url
    private final static String TEST_TOKEN_URL = "https://sbpaymentservices.payfort.com/FortAPI/paymentApi";
    //Live token url
    public final static String LIVE_TOKEN_URL = "https://paymentservices.payfort.com/FortAPI/paymentApi";
    //Make a change for live or test token url from WS_GET_TOKEN variable
    private final static String WS_GET_TOKEN = TEST_TOKEN_URL;

    //Statics
    private final static String MERCHANT_IDENTIFIER = "e54638eb";
    private final static String ACCESS_CODE = "hRRVGXrIpHSYoH19Ebwt";
    private final static String PHONE_NUMBER = "01008606003";
    private final static String SHA_TYPE = "SHA-256";
    private final static String SHA_REQUEST_PHRASE = "sportEyeByAbouElgoukh";
    public final static String SHA_RESPONSE_PHRASE = "sportEyeByAbouElgoukh";
    public final static String CURRENCY_TYPE = "EGP";
    public final static String LANGUAGE_TYPE = "en";//Arabic - ar //English - en
    String signature;
    private final Gson gson;
    private Activity context;
    private IPaymentRequestCallBack iPaymentRequestCallBack;
    private FortCallBackManager fortCallback = null;
    private ProgressDialog progressDialog;
    private String sdkToken;
    private PayFortData payFortData;
private String transactionId;
    public PayFortValUPayment(Activity context, FortCallBackManager fortCallback, IPaymentRequestCallBack iPaymentRequestCallBack) {
        this.context = context;
        this.fortCallback = fortCallback;
        this.iPaymentRequestCallBack = iPaymentRequestCallBack;

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing your payment\nPlease wait...");
        progressDialog.setCancelable(false);
        sdkToken = "";
        gson = new Gson();
    }

    public void requestForValUPayment(PayFortData payFortData) {
        this.payFortData = payFortData;
        new customerVerifyFromServer().execute(WS_GET_TOKEN);
    }

    private void requestPurchase() {
        try {
            FortSdk.getInstance().registerCallback(context, getPurchaseFortRequest(), FortSdk.ENVIRONMENT.TEST, RESPONSE_PURCHASE,
                    fortCallback, true, new FortInterfaces.OnTnxProcessed() {
                        @Override
                        public void onCancel(Map<String, Object> map, Map<String, Object> responseMap) {
                            JSONObject response = new JSONObject(responseMap);
                            PayFortData payFortData = gson.fromJson(response.toString(), PayFortData.class);
                            payFortData.paymentResponse = response.toString();
                            Log.e("Cancel Response", response.toString());
                            if (iPaymentRequestCallBack != null) {
                                iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_PURCHASE_CANCEL, payFortData);
                            }
                        }

                        @Override
                        public void onSuccess(Map<String, Object> map, Map<String, Object> fortResponseMap) {
                            JSONObject response = new JSONObject(fortResponseMap);
                            PayFortData payFortData = gson.fromJson(response.toString(), PayFortData.class);
                            payFortData.paymentResponse = response.toString();
                            Log.e("Success Response", response.toString());
                            if (iPaymentRequestCallBack != null) {
                                iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_PURCHASE_SUCCESS, payFortData);
                            }
                        }

                        @Override
                        public void onFailure(Map<String, Object> map, Map<String, Object> fortResponseMap) {
                            JSONObject response = new JSONObject(fortResponseMap);
                            PayFortData payFortData = gson.fromJson(response.toString(), PayFortData.class);
                            payFortData.paymentResponse = response.toString();
                            Log.e("Failure Response", response.toString());
                            if (iPaymentRequestCallBack != null) {
                                iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_PURCHASE_FAILURE, payFortData);
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private FortRequest getPurchaseFortRequest() {
        FortRequest fortRequest = new FortRequest();
        if (payFortData != null) {
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("command", payFortData.command);
            parameters.put("merchant_reference", payFortData.merchantReference);
            parameters.put("amount", payFortData.amount);
            parameters.put("currency", payFortData.currency);
            parameters.put("language", payFortData.language);
            parameters.put("customer_email", payFortData.customerEmail);
            parameters.put("sdk_token", sdkToken);

//    parameters.put("command", payFortData.command);
//            parameters.put("access_code", ACCESS_CODE);
//            parameters.put("merchant_identifier", MERCHANT_IDENTIFIER);
//            parameters.put("merchant_reference", payFortData.merchantReference);
//            parameters.put("amount", payFortData.amount);
//            parameters.put("currency", payFortData.currency);
//            parameters.put("language", payFortData.language);
//            parameters.put("customer_email", payFortData.customerEmail);
//            parameters.put("token_name", payFortData.sdkToken);
//            parameters.put("signature", signature);
//            parameters.put("card_security_code", payFortData.authorizationCode);
//            parameters.put("expiry_date", payFortData.expiryDate);
//            parameters.put("card_number", payFortData.cardNumber);
//            parameters.put("payment_option","VISA");

            fortRequest.setRequestMap(parameters);
        }
        return fortRequest;
    }

    private class customerVerifyFromServer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... postParams) {
            String response = "";
            try {
                HttpURLConnection conn;
                URL url = new URL(postParams[0].replace(" ", "%20"));
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("content-type", "application/json");

                String str = getCustomerVerifyParams();
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();
                conn.connect();

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = conn.getInputStream();
                    response = convertStreamToString(inputStream);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.hide();
            Log.e("Response", response + "");
            try {
                PayFortData payFortData = gson.fromJson(response, PayFortData.class);


                if (payFortData.responseCode.equals("90000")) {
                    new OTPGenerateFromServer().execute(WS_GET_TOKEN);
                } else {
                    payFortData.paymentResponse = response;
                    iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_CustomerNOT_REGISTERD_FOR_VALU, payFortData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class OTPGenerateFromServer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... postParams) {
            String response = "";
            try {
                HttpURLConnection conn;
                URL url = new URL(postParams[0].replace(" ", "%20"));
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("content-type", "application/json");

                String str = getOTPGenerateParams();
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();
                conn.connect();

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = conn.getInputStream();
                    response = convertStreamToString(inputStream);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.hide();
            Log.e("Response", response + "");
            try {
                PayFortData payFortData = gson.fromJson(response, PayFortData.class);
                if (payFortData.responseCode.equals("88000")) {
                    transactionId = payFortData.transactionId;
                    new OTPVerifyFromServer().execute(WS_GET_TOKEN);
                } else {
                    payFortData.paymentResponse = response;
                    iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_CustomerNOT_REGISTERD_FOR_VALU, payFortData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class OTPVerifyFromServer extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... postParams) {
            String response = "";
            try {
                HttpURLConnection conn;
                URL url = new URL(postParams[0].replace(" ", "%20"));
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("content-type", "application/json");

                String str = getOtpVerifyParams();
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();
                conn.connect();

                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = conn.getInputStream();
                    response = convertStreamToString(inputStream);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressDialog.hide();
            Log.e("Response", response + "");
            try {
                PayFortData payFortData = gson.fromJson(response, PayFortData.class);


                if (payFortData.responseCode.equals("92182")) {

                } else {
                    payFortData.paymentResponse = response;
                    iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_CustomerNOT_REGISTERD_FOR_VALU, payFortData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getCustomerVerifyParams() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put(KEY_SERVICE_COMMAND, CUSTOMER_VERIFY);
            jsonObject.put(KEY_MERCHANT_REFERENCE, payFortData.merchantReference);
            jsonObject.put(KEY_MERCHANT_IDENTIFIER, MERCHANT_IDENTIFIER);
            jsonObject.put(KEY_ACCESS_CODE, ACCESS_CODE);
            jsonObject.put(KEY_LANGUAGE, LANGUAGE_TYPE);
            jsonObject.put("payment_option", "VALU");
            jsonObject.put("phone_number", PHONE_NUMBER);

            String concatenatedString = SHA_REQUEST_PHRASE +
                    KEY_ACCESS_CODE + "=" + ACCESS_CODE +
                    KEY_LANGUAGE + "=" + LANGUAGE_TYPE +
                    KEY_MERCHANT_IDENTIFIER + "=" + MERCHANT_IDENTIFIER +
                    KEY_MERCHANT_REFERENCE + "=" + payFortData.merchantReference +
                    "payment_option" + "=" + "VALU" +
                    "phone_number" + "=" + PHONE_NUMBER +
                    KEY_SERVICE_COMMAND + "=" + CUSTOMER_VERIFY +
                    SHA_REQUEST_PHRASE;
            signature = getSignatureSHA256(concatenatedString);
            jsonObject.put(KEY_SIGNATURE, signature);


            Log.e("concatenatedString", concatenatedString);
            Log.e("signature", signature);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JsonString", String.valueOf(jsonObject));
        return String.valueOf(jsonObject);
    }

    Gson g = new Gson();

    public String getOTPGenerateParams() {
        List<PaymentProduct> products = new ArrayList<>();
        PaymentProduct paymentProduct = new PaymentProduct();
        for (Product product : payFortData.getProducts()) {

            if (paymentProduct.product_name == null || paymentProduct.product_name.isEmpty())
                paymentProduct.product_name = product.getName();
            else
                paymentProduct.product_name = String.format("%sand%s", paymentProduct.product_name, product.getName());

            paymentProduct.product_name = paymentProduct.product_name.replaceAll("[^\\d^a-zA-Z]", "");

                paymentProduct.product_price = payFortData.amount;

            if (paymentProduct.product_category == null || paymentProduct.product_category.isEmpty())
                paymentProduct.product_category = String.valueOf(product.getType());
            else
                paymentProduct.product_category = String.format("%s %s", paymentProduct.product_category, product.getType());

        }
        products.add(paymentProduct);


        JsonObject jsonObject = new JsonObject();


        jsonObject.addProperty(KEY_ACCESS_CODE, ACCESS_CODE);
        jsonObject.addProperty("amount", payFortData.amount);

        jsonObject.addProperty("currency", "EGP");
        jsonObject.addProperty(KEY_LANGUAGE, LANGUAGE_TYPE);
        jsonObject.addProperty(KEY_MERCHANT_IDENTIFIER, MERCHANT_IDENTIFIER);
        jsonObject.addProperty("merchant_order_id", payFortData.merchantReference);
        jsonObject.addProperty(KEY_MERCHANT_REFERENCE, payFortData.merchantReference);
        jsonObject.addProperty("payment_option", "VALU");
        jsonObject.addProperty("phone_number", PHONE_NUMBER);
        jsonObject.add("products", g.toJsonTree(products));
        jsonObject.addProperty(KEY_SERVICE_COMMAND, OTP_GENERATE);

        List<String> elephantList = Arrays.asList(g.toJsonTree(products).toString().split(","));
        String c = "[{";

        for (String item : elephantList) {

            if (!c.equals("[{")) {
                c = c + ", ";
            }
            if (item.contains("product_name")) {
                c = String.format("%sproduct_name=%s", c, paymentProduct.product_name);

            } else if (item.contains("product_category")) {
                c = String.format("%sproduct_category=%s", c, paymentProduct.product_category);
            } else if (item.contains("product_price")) {
                c = String.format("%sproduct_price=%s", c, paymentProduct.product_price);
            }

        }
        c = c + "}]";
        String concatenatedString = SHA_REQUEST_PHRASE +
                KEY_ACCESS_CODE + "=" + ACCESS_CODE +
                "amount" + "=" + payFortData.amount +
                "currency" + "=" + "EGP" +
                KEY_LANGUAGE + "=" + LANGUAGE_TYPE +
                KEY_MERCHANT_IDENTIFIER + "=" + MERCHANT_IDENTIFIER +
                "merchant_order_id" + "=" + payFortData.merchantReference +
                KEY_MERCHANT_REFERENCE + "=" + payFortData.merchantReference +
                "payment_option" + "=" + "VALU" +
                "phone_number" + "=" + PHONE_NUMBER +
                "products" + "=" + c +
                KEY_SERVICE_COMMAND + "=" + OTP_GENERATE +
                SHA_REQUEST_PHRASE;
        signature = getSignatureSHA256(concatenatedString);
        jsonObject.addProperty(KEY_SIGNATURE, signature);


        Log.e("concatenatedString", concatenatedString);
        Log.e("signature", signature);

        Log.e("JsonString", String.valueOf(jsonObject));
        return String.valueOf(jsonObject);
    }

    public String getOtpVerifyParams() {
        JSONObject jsonObject = new JSONObject();
        try {


            jsonObject.put(KEY_MERCHANT_IDENTIFIER, MERCHANT_IDENTIFIER);
            jsonObject.put(KEY_MERCHANT_REFERENCE, payFortData.merchantReference);
            jsonObject.put("merchant_order_id", payFortData.merchantReference);

            jsonObject.put(KEY_ACCESS_CODE, ACCESS_CODE);
            jsonObject.put(KEY_LANGUAGE, LANGUAGE_TYPE);
            jsonObject.put("amount", payFortData.amount);
            jsonObject.put("currency", payFortData.currency);
            jsonObject.put("payment_option", "VALU");
            jsonObject.put(KEY_SERVICE_COMMAND, OTP_VERIFY);
            jsonObject.put("total_downpayment", "0");
            jsonObject.put("otp", "123456");
            jsonObject.put("phone_number", PHONE_NUMBER);

            String concatenatedString = SHA_REQUEST_PHRASE +
                    KEY_ACCESS_CODE + "=" + ACCESS_CODE +
                    "amount" + "=" + payFortData.amount +
                    "currency" + "=" + payFortData.currency +
                    KEY_LANGUAGE + "=" + LANGUAGE_TYPE +
                    KEY_MERCHANT_IDENTIFIER + "=" + MERCHANT_IDENTIFIER +
                    "merchant_order_id" + "=" + payFortData.merchantReference +
                    KEY_MERCHANT_REFERENCE + "=" + payFortData.merchantReference +
                    "otp" + "=" + "123456" +
                    "payment_option" + "=" + "VALU" +
                    "phone_number" + "=" + PHONE_NUMBER +
                    KEY_SERVICE_COMMAND + "=" + OTP_VERIFY +
                    "total_downpayment" + "=" + "0" +
                    SHA_REQUEST_PHRASE;
            signature = getSignatureSHA256(concatenatedString);
            jsonObject.put(KEY_SIGNATURE, signature);


            Log.e("concatenatedString", concatenatedString);
            Log.e("signature", signature);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JsonString", String.valueOf(jsonObject));
        return String.valueOf(jsonObject);
    }

    private static String convertStreamToString(InputStream inputStream) {
        if (inputStream == null)
            return null;
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream), 1024);
            String line;
            while ((line = r.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private static String getSignatureSHA256(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            return String.format("%0" + (messageDigest.length * 2) + 'x', new BigInteger(1, messageDigest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}