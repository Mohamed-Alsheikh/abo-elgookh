package com.abouelgoukh.sporteye.Fragments.NavBarFragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.abouelgoukh.sporteye.Activities.EditProfileActivity;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Deleget;
import com.abouelgoukh.sporteye.Fragments.DataChanagePasswordFragment;
import com.abouelgoukh.sporteye.Fragments.PasswordDialogFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.dialog.AddAddress;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.AddressAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Government;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model.Address;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model.Profile;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model.ProfileAnswerReponse;
import com.abouelgoukh.sporteye.utils.Validation;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener, Deleget {

    ApiServiceInterface apiServiceInterface;
    View view;
    TextView txtmobilephone, txtEmail, txtusername;
    Spinner useraddressspinner;

    ImageButton btnaddnewaddress;
    ImageButton btndeleteaddress;
    ImageView spinnericon;

    ArrayAdapter<String> address_Adapter;
    List<String> addressListstr;
    List<Address> addressList;
    public static Profile profiledetails;
    ProgressBar main_progress;
    FloatingActionButton fab_edit_profile;
    private PasswordDialogFragment passwordDialogFragment;
    private DataChanagePasswordFragment dataChanagePasswordFragment;
    public Deleget delegate;
    boolean flag_dataretrieved;
    FrameLayout noInternet_image;
    ConstraintLayout parentlayout;
    CoordinatorLayout profileCoordinatorLayout;
    Snackbar snackbar;
    SharedPreferences session;
    private Gson gson;

    public MyProfileFragment() {


    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_my_profile, container, false);
            noInternet_image = (FrameLayout) view.findViewById(R.id.imageview);
            session = getContext().getSharedPreferences("ProfileData", MODE_PRIVATE);
            parentlayout = (ConstraintLayout) view.findViewById(R.id.parentlayout);
            profileCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.profileCoordinatorLayout);
            fab_edit_profile = (FloatingActionButton) view.findViewById(R.id.fab_edit_profile);
            fab_edit_profile.setOnClickListener(this);
            addressListstr = new ArrayList<>();
            addressList = new ArrayList<>();
            apiServiceInterface = ApiUtils.getRetrofitObject();
            main_progress = (ProgressBar) view.findViewById(R.id.main_progress);
            txtmobilephone = (TextView) view.findViewById(R.id.txtmobilephone);
            txtEmail = (TextView) view.findViewById(R.id.txtEmail);
            txtusername = (TextView) view.findViewById(R.id.txtusername);
            useraddressspinner = (Spinner) view.findViewById(R.id.useraddressspinner);
            btnaddnewaddress = (ImageButton) view.findViewById(R.id.add_profile_address);
            btndeleteaddress = (ImageButton) view.findViewById(R.id.delete_profile_address);
            btnaddnewaddress.setOnClickListener(this);
            btndeleteaddress.setOnClickListener(this);
            spinnericon = (ImageView) view.findViewById(R.id.spinnericon);
            spinnericon.setOnClickListener(this);

            dataChanagePasswordFragment = new DataChanagePasswordFragment();
            passwordDialogFragment = new PasswordDialogFragment();

            final FragmentManager fragmentManager = getFragmentManager();
            final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            setSpinnerValues();
            noInternet_image.setVisibility(View.GONE);
            parentlayout.setVisibility(View.VISIBLE);
            if (MainActivity.controller.isNetworkAvailable()) {

                getUserdata();

            } else {
                loadOfflineProfileData();
                if (profiledetails == null) {
                    noInternet_image.setVisibility(View.VISIBLE);
                    parentlayout.setVisibility(View.GONE);
                }

                snackbar = Snackbar.make(profileCoordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG).
                        setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        });
                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        }
        return view;
    }

    private void resetprofile() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(getContext());

        adb.setTitle("Do you want to Reset your values?");

        //adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String str_username = txtusername.getText().toString();
                String str_email = txtEmail.getText().toString();
                String str_mobile = txtmobilephone.getText().toString();
                if (checkValues(str_username, str_email, str_mobile)) {
                    callResetprofile(str_username, str_email, str_mobile);
                }


            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();

    }

    private boolean checkValues(String str_username, String str_email, String str_mobile) {

        if (!Validation.validate_name(str_username)) {
            txtusername.setError("Invalid Name ");
            return false;
        }
        if (!Validation.validate_Email(str_email)) {
            txtEmail.setError("Invalid Email ");
            return false;
        }
        if (!Validation.validate_mobile(str_mobile)) {
            txtmobilephone.setError("Invalid Mobile Number ");
            return false;
        }
        return true;
    }

    private void callResetprofile(String str_username, String str_email, String str_mobile) {


        callResetProfileDataService(str_username, str_email, str_mobile).enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        Toast.makeText(getContext(), "profile Data Changed", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {


            }

        });

    }

    private Call<SimpleResponse> callResetProfileDataService(String str_username, String str_email, String str_mobile) {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        return apiServiceInterface.resetProfileData(apiToken, str_username, str_email, str_mobile);
    }

    private void saveOffLineProfileData(Profile profile) {
        gson = new Gson();
        String json = gson.toJson(profile);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(profile.getMobile() + "", json);
        editor.apply();
    }

    private void loadOfflineProfileData() {
        // Loop over all geofence keys in prefs and add to namedGeofences

        gson = new Gson();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            profiledetails = gson.fromJson(jsonString, Profile.class);
            if (profiledetails != null) {
                prepareAddressAndGoven(profiledetails);
            }
        }
        main_progress.setVisibility(View.GONE);
    }

    private void getUserdata() {

        callgetMyOrdersDetailsService().enqueue(new Callback<ProfileAnswerReponse>() {
            @Override
            public void onResponse(Call<ProfileAnswerReponse> call, Response<ProfileAnswerReponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        profiledetails = response.body().getProfile();
                        main_progress.setVisibility(View.GONE);
                        flag_dataretrieved = true;
                        saveOffLineProfileData(profiledetails);
                        prepareAddressAndGoven(profiledetails);
                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        main_progress.setVisibility(View.GONE);
                        flag_dataretrieved = false;
                        Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    int statusCode = response.code();
                    main_progress.setVisibility(View.GONE);
                    flag_dataretrieved = false;
                    Toast.makeText(getContext(), "connection error", Toast.LENGTH_SHORT).show();

                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<ProfileAnswerReponse> call, Throwable t) {
                main_progress.setVisibility(View.GONE);
                if (getContext() != null) {
                    Toast.makeText(getContext(), "Connection Failed", Toast.LENGTH_SHORT).show();
                }
                flag_dataretrieved = false;
            }

        });

    }

    private void calldeleteAddress(int address_id, final int selecteditem) {

        callgetdeleteAddressService(address_id).enqueue(new Callback<AddressAnswerResponse>() {
            @Override
            public void onResponse(Call<AddressAnswerResponse> call, Response<AddressAnswerResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {

                        Toast.makeText(getContext(), "Address Deleted ", Toast.LENGTH_SHORT).show();
                        addressListstr.remove(selecteditem);
                        address_Adapter.notifyDataSetChanged();
                        addressList.remove(selecteditem);
                        if (addressList.isEmpty()) {
                            btndeleteaddress.setVisibility(View.GONE);
                        }else {
                            btndeleteaddress.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getStatus() == 402) {
                        //something is required

                        Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                    }


                } else {

                    int statusCode = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<AddressAnswerResponse> call, Throwable t) {


            }

        });

    }

    private Call<ProfileAnswerReponse> callgetMyOrdersDetailsService() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());
        return apiServiceInterface.getProfileDetails(apiToken, lang);
    }

    private Call<AddressAnswerResponse> callgetdeleteAddressService(int address_id) {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        return apiServiceInterface.deleteAddress(apiToken, address_id);
    }


    private void setSpinnerValues() {
        address_Adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, addressListstr);
        // Drop down layout style
        address_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        useraddressspinner.setAdapter(address_Adapter);

    }

    private void prepareAddressAndGoven(Profile profiledetails) {
        addressList = profiledetails.getAddress();
        for (int x = 0; x < addressList.size(); x++) {
            addressListstr.add(addressList.get(x).getAddress());
        }
        if (addressList.isEmpty()) {
            btndeleteaddress.setVisibility(View.GONE);
        }else {
            btndeleteaddress.setVisibility(View.VISIBLE);
        }
        address_Adapter.notifyDataSetChanged();
        txtusername.setText(this.profiledetails.getUsername().toString());
        txtEmail.setText(this.profiledetails.getEmail().toString());
        txtmobilephone.setText(this.profiledetails.getMobile().toString());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab_edit_profile:
                if (flag_dataretrieved) {
                    Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), "try to get User Data first", Toast.LENGTH_SHORT).show();

                }
                break;
            case R.id.spinnericon:
                showDropDown();
                break;
            case R.id.add_profile_address:
                if (MainActivity.controller.isNetworkAvailable()) {
                    openAddressDialog();
                } else
                    snackbar.show();
                break;
            case R.id.delete_profile_address:
                if (useraddressspinner.getSelectedItemPosition() >= 0) {
                    if (MainActivity.controller.isNetworkAvailable()) {
                        deleteAddress();
                    } else
                        snackbar.show();
                }
                break;
            case R.id.btnrestprofileData:
                if (MainActivity.controller.isNetworkAvailable()) {
                    resetprofile();
                } else
                    snackbar.show();

                break;

        }
    }

    private void deleteAddress() {


        final AlertDialog.Builder adb = new AlertDialog.Builder(getContext());

        adb.setTitle("Do you want to Delete this address?");

        //adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int address_id = addressList.get(useraddressspinner.getSelectedItemPosition()).getId();
                int selecteditem = useraddressspinner.getSelectedItemPosition();
                calldeleteAddress(address_id, selecteditem);
            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        view = null;
    }

    private void openAddressDialog() {
        //  use  to decorate it
//       AddAddress addAddress=new AddAddress(CheckOutActivity.this,R.style.DialogTheme);
        AddAddress addAddress = new AddAddress(getActivity(), "Profile");
        addAddress.deleget = this;
        addAddress.show();

        WindowManager manager = (WindowManager) getContext().getSystemService(Activity.WINDOW_SERVICE);
        int width, height;
        Toolbar.LayoutParams params;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
            width = manager.getDefaultDisplay().getWidth();
            height = (manager.getDefaultDisplay().getHeight()) - 320;
        } else {
            Point point = new Point();
            manager.getDefaultDisplay().getSize(point);
            width = point.x;
            height = (point.y) - 320;
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(addAddress.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;
        addAddress.getWindow().setAttributes(lp);
    }

    public void onSaveAddress(Integer Address_id, String addressTxt, Government government, String goven_name) {
        Address addres = new Address();
        addres.setId(Address_id);
        addres.setAddress(addressTxt);
//        addres.setGovernment(government);
        addressList.add(addres);
        addressListstr.add(addressTxt);
        address_Adapter.notifyDataSetChanged();
        if (addressList.isEmpty()) {
            btndeleteaddress.setVisibility(View.GONE);
        }else {
            btndeleteaddress.setVisibility(View.VISIBLE);
        }
//        govern_Adapter.notifyDataSetChanged();
//        Toast.makeText(getContext(), addressTxt + "address", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDeleteCartitem(Double deleted_item_price, String operation, int amount) {

    }

    public void showDropDown() {
        useraddressspinner.performClick();
    }
}
