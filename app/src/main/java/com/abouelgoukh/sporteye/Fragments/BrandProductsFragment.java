package com.abouelgoukh.sporteye.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Interfaces.ExecuteServiceDelegate;
import com.abouelgoukh.sporteye.ProductBrandDelegate;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.Brand;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.BrandAnswerResponse;
import com.abouelgoukh.sporteye.utils.ViewDialog;

import static com.abouelgoukh.sporteye.adapter.BrandsAdapter.main_Product_id;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BrandProductsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BrandProductsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BrandProductsFragment extends Fragment implements ExecuteServiceDelegate, ProductBrandDelegate {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int brandid;
    private String Brand_type;
ProgressBar progressbar;
    int position = -1;
    SharedProductsFragment sharedProductsFragmentFragment;
    boolean flag_fire = false;
    ArrayList<String> brands_names;
    ArrayList<String> brands_images;
    ArrayList<String> brands_Item_Count;
    List<Brand> brandsList;
    private OnFragmentInteractionListener mListener;
    ExecuteServiceDelegate this_delegate;
    ProductBrandDelegate productBrandDelegate;
    FrameLayout maincontent;
    FrameLayout imageview;
    boolean iscanceled = false;
    ApiServiceInterface brandsApiService;
    int All_Content;
    View view;
    ViewDialog alert;
    FloatingActionButton filter;
    public MainActivity mainActivity;
    Call<BrandAnswerResponse> getBrandsService;
    public BrandProductsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BrandProductsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BrandProductsFragment newInstance(String param1, String param2) {
        BrandProductsFragment fragment = new BrandProductsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            brandid = getArguments().getInt("brandid");
//            Brand_type = getArguments().getString("Brand_type");
            brandid = MainActivity.brandid;
            Brand_type = MainActivity.Brand_type;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_brand_products, container, false);
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if( keyCode == KeyEvent.KEYCODE_BACK ) {

                        // leave this blank in order to disable the back press
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            Handler handler=new Handler();
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    view.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            if( keyCode == KeyEvent.KEYCODE_BACK ) {

                                // leave this blank in order to disable the back press
                                return false;
                            } else {
                                return false;
                            }
                        }
                    });
                }
            };
            handler.postDelayed(runnable,300);
            brandid = MainActivity.brandid;
            Brand_type = MainActivity.Brand_type;
            brandsList = MainActivity.brandsalbumlist;
            filter = (FloatingActionButton) view.findViewById(R.id.filter);
            progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
            progressbar.setVisibility(View.INVISIBLE);
            filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (alert == null) {
                        alert = new ViewDialog();
                        alert.productBrandDelegate = productBrandDelegate;
                    }

                    alert.showDialog(getActivity(), brands_names, brands_images, brands_Item_Count, position, All_Content, Brand_type);
                }
            });

            brandsApiService = ApiUtils.getRetrofitObject();
            brands_names = new ArrayList<>();
            brands_images = new ArrayList<>();
            brands_Item_Count = new ArrayList<>();
            maincontent = (FrameLayout) view.findViewById(R.id.child_fragment);
            imageview = (FrameLayout) view.findViewById(R.id.imageview);
            All_Content = 0;
            this_delegate = this;
            productBrandDelegate = this;
            if (Brand_type.equals("OtherProductsSubCatogery")) {
                filter.setVisibility(View.GONE);
            } else {
                filter.setVisibility(View.VISIBLE);
            }

            if ((brandsList == null || brandsList.size() == 0) && MainActivity.controller.isNetworkAvailable()) {
                getBicyclesBrands();
            } else {
                continu();
            }
        }
        return view;
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void continu() {

        if (MainActivity.controller.isNetworkAvailable()) {
            for (Brand item : brandsList) {
                brands_names.add(item.getName());
                brands_images.add(item.getLogo());
                brands_Item_Count.add(item.getItems() + "");
                All_Content = All_Content + item.getItems();
                if (item.getBrandId() == brandid) {
                    position = brandsList.indexOf(item);
                }
            }

            if (!Brand_type.equals("OtherProductsSubCatogery")) {
                brands_names.add("All Brands");
            }

//            if (getActivity() instanceof MainActivity) {
//                MainActivity mainActivity = (MainActivity) getActivity();
//                BrandProductsCollapsingFragment slideimage = new BrandProductsCollapsingFragment();
//                Bundle slideimagearguments = new Bundle();
//                slideimagearguments.putStringArrayList("brands_names", brands_names);
//                slideimagearguments.putStringArrayList("brands_images", brands_images);
//                slideimagearguments.putStringArrayList("brands_Item_Count", brands_Item_Count);
//                slideimagearguments.putInt("position", position);
//                slideimagearguments.putInt("All_Content", All_Content);
//                slideimagearguments.putString("Brand_type", Brand_type);
//                slideimage.setArguments(slideimagearguments);
//                slideimage.productBrandDelegate = productBrandDelegate;
//                mainActivity.switchContentAddSlideShow(slideimage, brands_images.size());
//            }
            if (position >= 0) {
                onChangBrand(position, null, null, null, null);

            } else {
                position = brands_names.size() - 1;
                onChangBrand(position, null, null, null, null);

            }
        } else {
            changeBrand(0, 0, null, null, null, null);
        }


    }

    private void changeBrand(int position, int brand_id, String price_from, String price_to, String size_from, String size_to) {

        sharedProductsFragmentFragment = new SharedProductsFragment();
        sharedProductsFragmentFragment.delegate = this_delegate;

        Bundle arguments = new Bundle();
        arguments.putString("target", Brand_type);

        arguments.putInt("brandid", brand_id);
        arguments.putString("price_from", price_from);
        arguments.putString("price_to", price_to);
        arguments.putString("size_from", size_from);
        arguments.putString("size_to", size_to);

        sharedProductsFragmentFragment.setArguments(arguments);

        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.child_fragment, sharedProductsFragmentFragment).commitAllowingStateLoss();

        }
        progressbar.setVisibility(View.INVISIBLE);
    }

    public void getBicyclesBrands() {
        progressbar.setVisibility(View.VISIBLE);
         getBrandsService = null;
        if (Brand_type.equals("Bicycles Brands")) {
            getBrandsService = getBicyclesBrandsService();
        } else if (Brand_type.equals("Accessories Brands")) {
            getBrandsService = getAccessoriesBrandsService();
        } else if (Brand_type.equals("OtherProductsSubCatogery")) {
            getBrandsService = getOtherSubProductsService(main_Product_id);
        }

        getBrandsService.enqueue(new Callback<BrandAnswerResponse>() {
            @Override
            public void onResponse(Call<BrandAnswerResponse> call, Response<BrandAnswerResponse> response) {

                if (response.isSuccessful()) {
//                    adapter.updateAnswers(response.body().getData().getBrands());

                    brandsList = response.body().getData().getBrands();
                    filter.setVisibility(View.VISIBLE);
                    continu();

                } else {
                    showMassage();
                    progressbar.setVisibility(View.INVISIBLE);
                    int statusCode = response.code();
                    showMassage();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<BrandAnswerResponse> call, Throwable t) {
                if(!iscanceled) {
                    showMassage();
                    progressbar.setVisibility(View.INVISIBLE);
                }

            }


        });
    }

    void showMassage() {
        new AlertDialog.Builder(getContext())
                .setTitle("We're Sorry")
                .setIcon(R.drawable.ic_error_outline_black_24dp)
                .setMessage("An error has occurred. Please try again")
                .setCancelable(true)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isNetworkAvailable()) {
                            getBicyclesBrands();
                            imageview.setVisibility(View.GONE);
                            maincontent.setVisibility(View.VISIBLE);
                        } else {
                            imageview.setVisibility(View.VISIBLE);
                            maincontent.setVisibility(View.GONE);
                        }
                    }
                }).show();
    }
    Call<BrandAnswerResponse> getBicyclesBrandsService() {
        return brandsApiService.getBicyclesBrands();
    }

    Call<BrandAnswerResponse> getAccessoriesBrandsService() {
        return brandsApiService.getAccessoriesBrands();
    }

    Call<BrandAnswerResponse> getOtherSubProductsService(int brandid) {
        return brandsApiService.getOtherSubProducts(brandid, LocaleHelper.getLanguage(getContext()));
    }


    // TODO: dRename metho, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        view = null;
    }

    @Override
    public void onPause() {

//
//        MainActivity.frameLayout.setVisibility(View.GONE);
//        MainActivity.filter.setVisibility(View.GONE);

        if(getBrandsService!=null&&getBrandsService.isExecuted()){
            getBrandsService.cancel();
            iscanceled =true;
        }
        super.onPause();
        progressbar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void executeSevice() {
//        Toast.makeText(getContext(), "Accessories", Toast.LENGTH_SHORT).show();

    }

    private void onBrandSelected(int position, String price_from, String price_to, String size_from, String size_to) {

        if (position == brandsList.size()) {
            changeBrand(position, 0, price_from, price_to, size_from, size_to);
        } else {
            changeBrand(position, brandsList.get(position).getBrandId(), price_from, price_to, size_from, size_to);
        }
    }


    @Override
    public void onChangBrand(int addressTxt, String price_from, String price_to, String size_from, String size_to) {

        if (addressTxt == brands_names.size() - 1) {
            MainActivity.brandid = 0;
            MainActivity.brand_name = Brand_type;
        } else if (addressTxt != brands_names.size() - 1 && addressTxt < brands_names.size()) {
            MainActivity.brandid = brandsList.get(addressTxt).getBrandId();
            MainActivity.brand_name = brandsList.get(addressTxt).getName();

        } else {

        }
        mainActivity.getSupportActionBar().setTitle(MainActivity.brand_name);
        onBrandSelected(addressTxt, price_from, price_to, size_from, size_to);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
