package com.abouelgoukh.sporteye.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MyProfileFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.utils.Validation;

import static com.abouelgoukh.sporteye.Activities.MainActivity.swipeRefreshLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class DataChanagePasswordFragment extends Fragment {


    ApiServiceInterface apiServiceInterface = ApiUtils.getRetrofitObject();
    private Button btnsavedatapassword;
    //    private Button btncanceldatapassword;
    EditText txtoldpassword, txtnewpassword, txtconfirmpassword;
    FragmentManager fragmentManager;

    FragmentTransaction fragmentTransaction;
    Call<SimpleResponse> callLoginService;
    String old_password;
    String new_password;
    String confirm_password;
    boolean truevalues = false;
    ProgressDialog progressDialog;
    NestedScrollView datachanagepasswordfragment;
    private MyProfileFragment profileFragment;

    public DataChanagePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_chanage_password, container, false);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Reset Profile Data ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (callLoginService != null && callLoginService.isExecuted()) {
                    callLoginService.cancel();
                    Toast.makeText(getContext(), "reset password data canceled", Toast.LENGTH_SHORT).show();
                }
            }
        });

        fragmentManager = getFragmentManager();
        datachanagepasswordfragment = (NestedScrollView) view.findViewById(R.id.datachanagepasswordfragment);
        fragmentTransaction = fragmentManager.beginTransaction();
        profileFragment = new MyProfileFragment();
        txtoldpassword = (EditText) view.findViewById(R.id.txtoldpassword);
        txtnewpassword = (EditText) view.findViewById(R.id.txtnewpassword);
        txtconfirmpassword = (EditText) view.findViewById(R.id.txtconfirmpassword);


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.controller.isNetworkAvailable()) {
                    resetPassword();
                } else {
                    Snackbar snackbar = Snackbar.make(datachanagepasswordfragment, "No internet connection!", Snackbar.LENGTH_LONG).
                            setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                }
                            });
                    // Changing message text color
                    snackbar.setActionTextColor(Color.RED);
                    // Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }
            }
        });
//        btncanceldatapassword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//            }
//        });
        return view;


    }

    private void resetPassword() {
        old_password = txtoldpassword.getText().toString();
        new_password = txtnewpassword.getText().toString();
        confirm_password = txtconfirmpassword.getText().toString();

        if (checkValues()) {
            final AlertDialog.Builder adb = new AlertDialog.Builder(getContext());

            adb.setTitle("Do you want to Reset Password?");

            //adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    callResetPassword(old_password, new_password);

                }
            });

            adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            adb.show();

        }

    }

    private void callResetPassword(String old_password, String new_password) {
        progressDialog.show();
        callLoginService = callResetPasswordService(old_password, new_password);
        if (callLoginService != null) {
            callLoginService.enqueue(new Callback<SimpleResponse>() {
                @Override
                public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {

                        if (response.body().getStatus() == 200) {
                            Toast.makeText(getContext(), "password Changed", Toast.LENGTH_SHORT).show();
                            fragmentTransaction.replace(R.id.frame, profileFragment);
                            fragmentTransaction.commit();

                        } else if (response.body().getStatus() == 402) {
                            //something is required
                            Toast.makeText(getContext(), "something is required for password reset", Toast.LENGTH_SHORT).show();
                        }else if(response.body().getStatus() ==410){
                            Toast.makeText(getContext(), "old password not correct", Toast.LENGTH_SHORT).show();

                        }


                    } else {
                        int statusCode = response.code();
                        // handle request errors depending on status code
                    }
                }

                @Override
                public void onFailure(Call<SimpleResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "error in reset password", Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    private Call<SimpleResponse> callResetPasswordService(String old_password, String new_password) {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        return apiServiceInterface.resetPassword(apiToken, old_password, new_password);
    }


    private boolean checkValues() {
        if (!Validation.validate_Password(old_password)) {
            txtoldpassword.setError("Invalid Password");
            return false;
        }
        if (!Validation.validate_Password(new_password)) {
            txtnewpassword.setError("Invalid Password");
            return false;
        }

        if (!confirm_password.equals(new_password)) {
            txtconfirmpassword.setError("Invalid Password Confirmation");
            return false;
        }
        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
        swipeRefreshLayout.setEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.setEnabled(false);
    }
}
