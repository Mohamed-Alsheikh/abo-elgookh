package com.abouelgoukh.sporteye.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Gallery;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.GalleryAnswerRespone;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;

import static android.content.Context.MODE_PRIVATE;

public class ProductDetailFragment extends Fragment {

    TextView pdtIdTxt;
    TextView pdtNameTxt;
    TextView pdt_Disc;
    TextView pdt_Price;
    TextView pdt_new_Price;

    TextView pdt_id_title;
    TextView pdt_new_Price_title;
    TextView pdt_Price_title;
    TextView pdt_Disc_title;
    RelativeLayout pdt_new_Price_layout;
    CoordinatorLayout pd_main_content;
    Button carticon;
    ImageView favoritIcon;
//    ImageView compareIcon;
    ImageView pdtImg;
    Activity activity;
    SharedPreferences session;
    private Gson gson;
    Controller controller;
    Resources resources;
    Snackbar snackbar;
    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options;
    private ImageLoadingListener imageListener;
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();
    List<Gallery> galleryList = new ArrayList<>();
    boolean flag;

    Product product;
    Runnable onFavoritsClicked;
    Handler favoritshandler = new Handler();

    public static final String ARG_ITEM_ID = "pdt_detail_fragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.mipmap.ic_launcher)
                .showStubImage(R.mipmap.ic_launcher)
                .showImageForEmptyUri(R.mipmap.ic_launcher).cacheInMemory()
                .cacheOnDisc().build();
        imageListener = (ImageLoadingListener) new ImageDisplayListener();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LocaleHelper.updateResources(getContext(), LocaleHelper.getLanguage(getContext()));
        View view = inflater.inflate(R.layout.fragment_pdt_detail, container, false);

        resources = getContext().getResources();
       findViewById(view);

        Bundle bundle = this.getArguments();

        pdt_new_Price_layout = (RelativeLayout) view.findViewById(R.id.pdt_new_Price_layout);
        session = getContext().getSharedPreferences("Cart", MODE_PRIVATE);
        pd_main_content = (CoordinatorLayout) view.findViewById(R.id.pd_main_content);
        if (bundle != null) {
            product = bundle.getParcelable("singleProduct");
            setProductItem(product);
            flag = product.isfavoritImageChanged();
        }

          snackbar = Snackbar.make(pd_main_content, "No internet connection!", Snackbar.LENGTH_LONG).
                setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        controller = (Controller) getContext().getApplicationContext();
        if(MainActivity.controller.isNetworkAvailable()){
            manageCart();
        }else{
            prepareProductDetails();
            snackbar.show();
        }

        getGallery(product.getId(), product.getType());


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    private void getGallery(int product_id, int type) {
        callProductDetailsService(type, product_id).enqueue(new Callback<GalleryAnswerRespone>() {
            @Override
            public void onResponse(Call<GalleryAnswerRespone> call, Response<GalleryAnswerRespone> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {

                        galleryList = response.body().getGallery();
                        if (isAdded()) {
                            prepareProductDetails();
                        }

                    } else if (response.body().getStatus() == 300) {
                        //the pruduct id is wrong
                        if(getContext()!=null) {
                            Toast.makeText(getContext(), "the product id is wrong", Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        if(getContext()!=null){
                            Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();

                        }

                    }


                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<GalleryAnswerRespone> call, Throwable t) {
                prepareProductDetails();
            }

        });
    }

    private void prepareProductDetails() {
        ArrayList<String> productImages = new ArrayList<>();
        productImages.add(product.getImage());

        for (int x = 0; x < galleryList.size(); x++) {
            productImages.add(galleryList.get(x).getPath());
        }

        Bundle arguments = new Bundle();
        arguments.putStringArrayList("singleProduct", productImages);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        Fragment slideimage = new ProductSlideShowFragment();
        slideimage.setArguments(arguments);
        transaction.replace(R.id.productslideshow, slideimage).commitAllowingStateLoss();
    }

    private void manageCart() {

        if (product.isCartImageChanged()) {
             carticon.setBackground(AppCompatDrawableManager.get().getDrawable(getContext(),R.drawable.btn_cart_clicked_bg));
        }
//        if (controller.checkCompareInProducts(product.getId())) {
//            compareIcon.setImageResource(R.drawable.ic_compare);
//        }
        if (product.isfavoritImageChanged()) {
            favoritIcon.setImageResource(R.drawable.ic_favorite);
        }


        carticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!controller.CheckProductInCart(product.getId(), product.getType())) {
                    int indexof_added_element = controller.getcartProductsArraylistsize();
                    controller.setCartOfProducts(product.getId(), product.getType());
                    (controller.getCart()).setProducts(product);
                    product.setCartImageChanged(true);
                    carticon.setBackground(AppCompatDrawableManager.get().getDrawable(getContext(),R.drawable.btn_cart_clicked_bg));
                    saveGeofence(product);
                } else {
                    int indexof_deleted_element = controller.deleteCartProducts(product.getId(), product.getType(), product);
                    product.setCartImageChanged(false);
                    carticon.setBackground(AppCompatDrawableManager.get().getDrawable(getContext(),R.drawable.btn_cart_bg));

                    removeSavedGeofences(product);
                }
                MainActivity.setupMessagesBadge(controller.getcartProductsArraylistsize());

            }
        });
//        compareIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!controller.checkCompareInProducts(product.getId())) {
//                    controller.setCompareProducts(product.getId());
//                    (controller.getCompareList()).setProducts(product);
//                    compareIcon.setImageResource(R.drawable.ic_compare);
//
//                } else {
//                    (controller.getCompareList()).deleteProducts(product);
//                    controller.deleteCompareProducts(product.getId());
//                    compareIcon.setImageResource(R.drawable.ic_compare_black_24dp);
//                }
//            }
//
//        });

        favoritIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.controller.isNetworkAvailable()){
                    if (flag) {
                        favoritIcon.setImageResource(R.drawable.ic_favorite_black_24dp);
                        flag = false;
//					product.setFavoritImageChanged(false);
                    } else {
                        favoritIcon.setImageResource(R.drawable.ic_favorite);
                        flag = true;
//					product.setFavoritImageChanged(true);
                    }
                    favoritshandler.removeCallbacks(onFavoritsClicked);
                    onFavoritsClicked = new Runnable() {
                        @Override
                        public void run() {
                            if (flag) {
                                onRunFavoritsClicked(product, 1);
                            } else {
                                onRunFavoritsClicked(product, 0);
                            }
                        }
                    };

                    favoritshandler.postDelayed(onFavoritsClicked, 2000);
                }else{

                    snackbar.show();
                }

            }


        });
    }
    private void saveGeofence(Product product) {

        gson = new Gson();
        String json = gson.toJson(product);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(product.getId().toString() + product.getType().toString()+"", json);
        editor.apply();
    }

    private void removeSavedGeofences(Product product) {

        SharedPreferences.Editor editor = session.edit();
        editor.remove(product.getId().toString()+product.getType().toString() + "");
        editor.apply();
    }
    public void onRunFavoritsClicked(Product product, int status) {
        if (controller.getprefrancesUserApiToken() == (null)) {

            if (!controller.CheckProductInFavorites(product.getId(), product.getType())) {
                controller.setFavoriteProducts(product.getId(), product.getType());
            } else {
                controller.deletefavoriteProducts(product.getId(), product.getType());

            }
        } else {

            setfavorits(product.getType(), product.getId(), status, product);

        }
    }

    private void setfavorits(int type, int prduct_id, int status, final Product product) {

        callfavoriteService(type, prduct_id, status).enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        product.setFavoritImageChanged(true);
                        if(ProductDetailFragment.this.isVisible()) {
                            Toast.makeText(getContext(), "Added To Favorite List", Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.body().getStatus() == 300) {
                        product.setFavoritImageChanged(false);
                        if(ProductDetailFragment.this.isVisible()) {
                            Toast.makeText(getContext(), "Removed From Favorite List", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (product.isfavoritImageChanged()) {
//							product.setFavoritImageChanged(false);
                            favoritIcon.setImageResource(R.drawable.ic_favorite);
                        } else {
//							product.setFavoritImageChanged(true);
                            favoritIcon.setImageResource(R.drawable.ic_favorite_black_24dp);
                        }
                        if(ProductDetailFragment.this.isVisible()) {
                            Toast.makeText(getContext(), "another Status", Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {
                    int statusCode = response.code();
                    if (product.isfavoritImageChanged()) {
//						product.setFavoritImageChanged(false);
                        favoritIcon.setImageResource(R.drawable.ic_favorite);
                    } else {
//						product.setFavoritImageChanged(true);
                        favoritIcon.setImageResource(R.drawable.ic_favorite_black_24dp);
                    }
                    if(ProductDetailFragment.this.isVisible()) {
                        Toast.makeText(getContext(), "there is error in connection", Toast.LENGTH_SHORT).show();
                    }
                    // handle request errors depending on status code
                }


            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                if (product.isfavoritImageChanged()) {
//					product.setFavoritImageChanged(false);
                    favoritIcon.setImageResource(R.drawable.ic_favorite);
                } else {
//					product.setFavoritImageChanged(true);
                    favoritIcon.setImageResource(R.drawable.ic_favorite_black_24dp);
                }
                Toast.makeText(getContext(), "there is error in connection", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private Call<SimpleResponse> callfavoriteService(int type, int productid, int status) {
        switch (type) {
            case 1:
                return mService.getfavoratePruduct(productid, controller.getprefrancesUserApiToken(), status);
            case 2:
                return mService.favorateaccessory(productid, controller.getprefrancesUserApiToken(), status);
            case 3:
                return mService.favorateOtherProduct(productid, controller.getprefrancesUserApiToken(), status);
        }
        return null;
    }

    private Call<GalleryAnswerRespone> callProductDetailsService(int type, int product_id) {
        SharedPreferences session = getContext().getSharedPreferences("Session", MODE_PRIVATE);
        String lang = LocaleHelper.getLanguage(getContext());;
        if(lang==null){
            lang= LocaleHelper.getLanguage(getContext());
        }
        switch (type) {
            case 1:
                return mService.getProductDetails("/api/getProductDetails", product_id,lang);
            case 2:
                return mService.getAccessoryDetails(product_id,lang);
            case 3:
                return mService.getOtherProductDetails(product_id,lang);
        }
        return null;
    }

    private void findViewById(View view) {
        pdtNameTxt = (TextView) view.findViewById(R.id.pdt_name);
        pdt_Disc = (TextView) view.findViewById(R.id.pdt_Disc);
        pdt_Price = (TextView) view.findViewById(R.id.pdt_Price);
        pdt_new_Price = (TextView) view.findViewById(R.id.pdt_new_Price);
        pdtIdTxt = (TextView) view.findViewById(R.id.pdt_type);


        pdt_id_title = (TextView) view.findViewById(R.id.pdt_id_title);
        pdt_new_Price_title = (TextView) view.findViewById(R.id.pdt_new_Price_title);
        pdt_Price_title = (TextView) view.findViewById(R.id.pdt_Price_title);
        pdt_Disc_title = (TextView) view.findViewById(R.id.pdt_Disc_title);

        pdt_id_title.setText(resources.getString(R.string.product_type));
        pdt_new_Price_title.setText(resources.getString(R.string.productNewPrice));
        pdt_Price_title.setText(resources.getString(R.string.productPrice));
        pdt_Disc_title.setText(resources.getString(R.string.product_Description));
        carticon = (Button) view.findViewById(R.id.carticon);
        favoritIcon = (ImageView) view.findViewById(R.id.favoritsicon);
//        compareIcon = (ImageView) view.findViewById(R.id.compareicon);
//		pdtImg = (ImageView) view.findViewById(R.id.product_detail_img);
    }

    private static class ImageDisplayListener extends
            SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections
                .synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);

                }
            }
        }
    }

    private void setProductItem(Product resultProduct) {
        pdtNameTxt.setText("" + resultProduct.getName());
        pdt_Disc.setText(resultProduct.getDescription() + "");
        pdt_Price.setText(resultProduct.getPrice() +" "+ resources.getString(R.string.unit));
        if (resultProduct.getPrice_after_discount()!=null&&resultProduct.getPrice_after_discount()>0&&resultProduct.getPrice_after_discount() != null) {
            pdt_new_Price.setText(resultProduct.getPrice_after_discount() +" "+ resources.getString(R.string.unit));
        } else
            pdt_new_Price_layout.setVisibility(View.GONE);
        pdtIdTxt.setText("Product Id: " + resultProduct.getId());
        if (resultProduct.getType() == 1) {
            pdtIdTxt.setText("Bicycle");
        } else {
            pdtIdTxt.setText("Accessory");
        }
//		imageLoader.displayImage(resultProduct.getImageUrl(), pdtImg, options,
//				imageListener);
    }
}
