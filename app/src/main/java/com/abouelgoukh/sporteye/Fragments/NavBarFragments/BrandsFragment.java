package com.abouelgoukh.sporteye.Fragments.NavBarFragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.adapter.BrandsAdapter;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.Brand;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.BrandAnswerResponse;

import com.abouelgoukh.sporteye.other.GridSpacingItemDecoration;

import static com.abouelgoukh.sporteye.adapter.BrandsAdapter.main_Product_id;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BrandsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BrandsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BrandsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView brandsRecyclerView;
    private BrandsAdapter brandsAbumAdapter;
    ArrayList<Brand> brandsAlbumList;
    Controller controller;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String Brand_type;
    private int Brand_id;
    ApiServiceInterface brandsApiService;
    ProgressBar main_progress;
    FrameLayout maincontent;
    FrameLayout imageview;
    private OnFragmentInteractionListener mListener;
    View view;

    public BrandsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BrandsFragment.
     */

    // TODO: Rename and change types and number of parameters
    public static BrandsFragment newInstance(String param1, String param2) {
        BrandsFragment fragment = new BrandsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Brand_type = getArguments().getString("Brand_type");
            Brand_id = getArguments().getInt("brandid");

//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_brands, container, false);
            maincontent = (FrameLayout) view.findViewById(R.id.content);
            imageview = (FrameLayout) view.findViewById(R.id.imageview);
            brandsRecyclerView = (RecyclerView) view.findViewById(R.id.brands_recycler_view);
            main_progress = (ProgressBar) view.findViewById(R.id.main_progress);
            controller = (Controller) getContext().getApplicationContext();
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
            brandsRecyclerView.setLayoutManager(mLayoutManager);
            brandsRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
            brandsRecyclerView.setItemAnimator(new DefaultItemAnimator());
            brandsAlbumList = new ArrayList<>();
            brandsAbumAdapter = new BrandsAdapter(getContext(), brandsAlbumList, Brand_type);
            brandsRecyclerView.setAdapter(brandsAbumAdapter);
            brandsApiService = ApiUtils.getRetrofitObject();

//        prepareAlbums();
            if (isNetworkAvailable()) {
                getBicyclesBrands();
                imageview.setVisibility(View.GONE);
                maincontent.setVisibility(View.VISIBLE);
            } else {
                imageview.setVisibility(View.VISIBLE);
                maincontent.setVisibility(View.GONE);
            }
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if( keyCode == KeyEvent.KEYCODE_BACK ) {

                        // leave this blank in order to disable the back press
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            Handler handler=new Handler();
            Runnable runnable=new Runnable() {
                @Override
                public void run() {
                    view.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            if( keyCode == KeyEvent.KEYCODE_BACK ) {

                                // leave this blank in order to disable the back press
                                return false;
                            } else {
                                return false;
                            }
                        }
                    });
                }
            };
            handler.postDelayed(runnable,500);
        }


        return view;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void getBicyclesBrands() {

        Call<BrandAnswerResponse> getBrandsService = null;
        if (Brand_type.equals("Bicycles Brands")) {
            getBrandsService = getBicyclesBrandsService();
        } else if (Brand_type.equals("Accessories Brands")) {
            getBrandsService = getAccessoriesBrandsService();
        } else if (Brand_type.equals("OtherProductsCatogery")) {
            getBrandsService = getOtherProductsService();
        } else if (Brand_type.equals("OtherProductsSubCatogery")) {
            getBrandsService = getOtherSubProductsService(Brand_id);
            main_Product_id = Brand_id;
        }

        getBrandsService.enqueue(new Callback<BrandAnswerResponse>() {
            @Override
            public void onResponse(Call<BrandAnswerResponse> call, Response<BrandAnswerResponse> response) {

                if (response.isSuccessful()) {
//                    adapter.updateAnswers(response.body().getData().getBrands());

                    brandsAlbumList.addAll(response.body().getData().getBrands());
                    brandsAbumAdapter.notifyDataSetChanged();
//                    brandsAbumAdapter.addAll(brandsAlbumList);
                    main_progress.setVisibility(View.INVISIBLE);
                    Log.d("MainActivity", "posts loaded from API");
                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                    main_progress.setVisibility(View.INVISIBLE);
                    showMassage();
                }
            }

            @Override
            public void onFailure(Call<BrandAnswerResponse> call, Throwable t) {
                int statusCode = 11;
                showMassage();

            }


        });
    }

    void showMassage() {
        main_progress.setVisibility(View.VISIBLE);
        new AlertDialog.Builder(getContext())
                .setTitle("We're Sorry")
                .setIcon(R.drawable.ic_error_outline_black_24dp)
                .setMessage("An error has occurred. Please try again")
                .setCancelable(true)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isNetworkAvailable()) {
                            getBicyclesBrands();
                            imageview.setVisibility(View.GONE);
                            maincontent.setVisibility(View.VISIBLE);
                        } else {
                            imageview.setVisibility(View.VISIBLE);
                            maincontent.setVisibility(View.GONE);
                        }
                    }
                }).show();
    }

    Call<BrandAnswerResponse> getBicyclesBrandsService() {
        return brandsApiService.getBicyclesBrands();
    }

    Call<BrandAnswerResponse> getAccessoriesBrandsService() {
        return brandsApiService.getAccessoriesBrands();
    }

    Call<BrandAnswerResponse> getOtherProductsService() {
        return brandsApiService.getOtherProducts(LocaleHelper.getLanguage(getContext()));
    }

    Call<BrandAnswerResponse> getOtherSubProductsService(int brand_id) {
        return brandsApiService.getOtherSubProducts(brand_id, LocaleHelper.getLanguage(getContext()));
    }


    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        view = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
