package com.abouelgoukh.sporteye.Fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MyProfileFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.utils.Validation;

/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordDialogFragment extends DialogFragment {

    ApiServiceInterface apiServiceInterface= ApiUtils.getRetrofitObject();
    private Button btnsavedatapassword;
    private Button btncanceldatapassword;
    EditText txtoldpassword,txtnewpassword,txtconfirmpassword;
    FragmentManager fragmentManager ;

    FragmentTransaction fragmentTransaction;

    String old_password;
    String new_password;
    String confirm_password;
    boolean truevalues=false;

    private MyProfileFragment profileFragment;


    public PasswordDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_password_dialog, container, false);
          fragmentManager = getFragmentManager();

          fragmentTransaction = fragmentManager.beginTransaction();


        profileFragment = new MyProfileFragment();
        btnsavedatapassword = (Button) view.findViewById(R.id.btnsavedatapassword);
        btncanceldatapassword = (Button) view.findViewById(R.id.btncanceldatapassword);
        txtoldpassword = (EditText) view.findViewById(R.id.txtoldpassword);
        txtnewpassword = (EditText) view.findViewById(R.id.txtnewpassword);
        txtconfirmpassword = (EditText) view.findViewById(R.id.txtconfirmpassword);


        btnsavedatapassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();

            }
        });
        return view;


    }

    private void resetPassword() {
         old_password=txtoldpassword.getText().toString();
         new_password=txtnewpassword.getText().toString();
         confirm_password=txtconfirmpassword.getText().toString();
        if(checkValues()){
            callResetPassword(old_password,new_password);
        }


    }

    private void callResetPassword(String old_password, String new_password) {


            callResetPasswordService(old_password,new_password).enqueue(new Callback<SimpleResponse>() {
                @Override
                public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                    if(response.isSuccessful()) {

                        if(response.body().getStatus()==200) {
                            Toast.makeText(getContext(),"password Changed",Toast.LENGTH_SHORT).show();
                            fragmentTransaction.replace(R.id.frame, profileFragment);
                            fragmentTransaction.commit();

                        }else if(response.body().getStatus()==402){
                            //something is required
                            Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                        }


                    }else {
                        int statusCode  = response.code();
                        // handle request errors depending on status code
                    }
                }

                @Override
                public void onFailure(Call<SimpleResponse> call, Throwable t) {


                }

            });

        }

    private Call<SimpleResponse> callResetPasswordService(String old_password, String new_password) {
        SharedPreferences session=getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN",null);
        return apiServiceInterface.resetPassword(apiToken,old_password,new_password);
    }

    private boolean checkValues(){
        if(!Validation.validate_Password(old_password)){
            txtoldpassword.setError("Invalid Password");
            return false;
        }
        if(!Validation.validate_Password(new_password)){
            txtnewpassword.setError("Invalid Password");
            return false;
        }

        if(!confirm_password.equals(new_password)){
            txtconfirmpassword.setError("Invalid Password Confirmation");
           return false ;
        }
        return true;
    }

}
