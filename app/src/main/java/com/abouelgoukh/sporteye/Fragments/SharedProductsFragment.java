package com.abouelgoukh.sporteye.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Interfaces.ExecuteServiceDelegate;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.adapter.SharedProductsAdapter;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.other.GridSpacingItemDecoration;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.ProductAnswerResponse;
import com.abouelgoukh.sporteye.utils.PaginationAdapterCallback;
import com.abouelgoukh.sporteye.utils.PaginationScrollListener;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SharedProductsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SharedProductsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SharedProductsFragment extends Fragment implements ExecuteServiceDelegate, PaginationAdapterCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private SharedProductsAdapter adapter;
    private List<Product> albumList;

    String message;
    boolean iscanceled = false;
    String target = "";
    int brandid;
    String price_from;
    String price_to;
    String size_from;
    String size_to;
    String SearchToken;
    ProgressBar webservicePG;
    TextView searchresult;
    private ApiServiceInterface mService;
    public ExecuteServiceDelegate delegate;
    public static Snackbar snackbar;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES = 5;
    private int currentPage = PAGE_START;
    Controller controller;
    SharedPreferences session;
    private Gson gson;
    FrameLayout maincontent;
    FrameLayout imageview;
    CoordinatorLayout CoordinatorLayout_main_content;
    private OnFragmentInteractionListener mListener;
    View view;
    Call<ProductAnswerResponse> getProduct;

    public SharedProductsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SharedProductsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SharedProductsFragment newInstance(String param1, String param2) {

        SharedProductsFragment fragment = new SharedProductsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            target = getArguments().getString("target");
            brandid = getArguments().getInt("brandid");
            price_from = getArguments().getString("price_from");
            price_to = getArguments().getString("price_to");
            size_from = getArguments().getString("size_from");
            size_to = getArguments().getString("size_to");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_home, container, false);
            maincontent = (FrameLayout) view.findViewById(R.id.Maincontent);
            imageview = (FrameLayout) view.findViewById(R.id.imageview);
            CoordinatorLayout_main_content = (CoordinatorLayout) view.findViewById(R.id.CoordinatorLayout_main_content);

            webservicePG = (ProgressBar) view.findViewById(R.id.main_progress);
            searchresult = (TextView) view.findViewById(R.id.searchresult);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(1), true));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            albumList = new ArrayList<>(0);

            mService = ApiUtils.getRetrofitObject();
            controller = (Controller) getContext().getApplicationContext();
            if (!target.equals("Home") && !target.equals("Favorite") && !target.equals("SearchResult")) {
                recyclerView.addOnScrollListener(new PaginationScrollListener(mLayoutManager) {
                    @Override
                    protected void loadMoreItems() {
                        isLoading = true;
                        currentPage += 1;

                        // mocking network delay for API call

                        if (currentPage == 2) {
                            adapter.addLoadingFooter();
                        }
                        loadNextPage();

                    }

                    @Override
                    public int getTotalPageCount() {
                        return TOTAL_PAGES;
                    }

                    @Override
                    public boolean isLastPage() {
                        return isLastPage;
                    }

                    @Override
                    public boolean isLoading() {
                        return isLoading;
                    }
                });
            }
//        prepareAlbums();
            if (getParentFragment() instanceof ExecuteServiceDelegate) {
                executeSevice();
            }
            snackbar = Snackbar.make(CoordinatorLayout_main_content, "No internet connection!", Snackbar.LENGTH_LONG).
                    setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    });
            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById( com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);

            loadProduct();
        }
        return view;


    }


    public void loadProduct() {
        webservicePG.setVisibility(View.VISIBLE);
        getProduct = null;
        if (target.equals("Home")) {
            session = getContext().getSharedPreferences("Home", MODE_PRIVATE);
            if (controller.isNetworkAvailable()) {
                SharedPreferences.Editor editor = session.edit();
                editor.clear();
                editor.apply();
                getProduct = callHomeService();
            } else {
                loadFavorits();
                manageUi(controller.getFavorite().getFavoritsItems());
                return;
            }

        } else if (target.equals("Bicycles Brands") && brandid > 0) {
            getProduct = getBicycles();
        } else if (target.equals("Bicycles Brands") && brandid == 0) {
            if (controller.isNetworkAvailable()) {
                getProduct = getAllBicycles();
            } else {
                manageUi(null);
                return;
            }
        } else if (target.equals("Accessories Brands") && brandid > 0) {
            getProduct = getAccessories();
        } else if (target.equals("Accessories Brands") && brandid == 0) {
            if (controller.isNetworkAvailable()) {
                getProduct = getAllAccessories();
            } else {
                manageUi(null);
                return;
            }
        } else if (target.equals("OtherProductsSubCatogery") && brandid > 0) {
            if (controller.isNetworkAvailable()) {
                getProduct = OtherProductsSubCatogery();
            } else {
                manageUi(null);
                return;
            }
        }
//        else if (target.equals("OtherProductsSubCatogery")&&brandid==0) {
//            if (controller.isNetworkAvailable()) {
//                getProduct = getAllOtherProductsSubCatogery();
//            } else {
//                manageUi(null);
//                return;
//            }
//        }
        else if (target.equals("Favorite")) {
            session = getContext().getSharedPreferences("Favorits", MODE_PRIVATE);


            if (controller.isNetworkAvailable()) {
                getProduct = getFavoritesService();
            } else {
                loadFavorits();
                manageUi(controller.getFavorite().getFavoritsItems());
                return;
            }
        } else if (target.equals("SearchResult")) {
            if (controller.isNetworkAvailable()) {
                SearchToken = getArguments().getString("searchToken");
                getProduct = getSearchService(SearchToken);
            } else {
                manageUi(null);
                return;

            }
        }
        if (getProduct != null)
            getProduct.enqueue(new Callback<ProductAnswerResponse>() {
                @Override
                public void onResponse(Call<ProductAnswerResponse> call, Response<ProductAnswerResponse> response) {
                    webservicePG.setVisibility(View.GONE);
                    if (response.isSuccessful()) {

                        if (response.body().getStatus() == 200) {

                            webservicePG.setVisibility(View.GONE);
                            albumList = response.body().getData().getProduct();
                            adapter = new SharedProductsAdapter(getContext(), response.body().getData().getProduct(), recyclerView, target, session, true, getActivity());
                            recyclerView.setAdapter(adapter);
//                        adapter.addAll(response.body().getData().getProduct(), session, true);

                        } else if (response.body().getStatus() == 400) {
                            searchresult.setText("Error: "+400);
                        } else if (response.body().getStatus() == 403) {

                            searchresult.setText(getContext().getResources().getString(R.string.nodate));
                        }

                    } else {
                        int statusCode = response.code();
                        String statusCode2 = response.message();
                        if (isAdded() && getActivity() != null) {
                            showMassage();
                        }
                        // handle request errors depending on status code
                    }
                }

                @Override
                public void onFailure(Call<ProductAnswerResponse> call, Throwable t) {
                    String ds = t.getMessage();
                    webservicePG.setVisibility(View.GONE);
                    if (isAdded() && getActivity() != null) {
                        if (!iscanceled) {
                            showMassage();
                        }

                    }
                }

            });
    }

    void showMassage() {
        new AlertDialog.Builder(getContext())
                .setTitle("We're Sorry")
                .setIcon(R.drawable.ic_error_outline_black_24dp)
                .setMessage("An error has occurred. Please try again")
                .setCancelable(true)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadProduct();
                    }
                }).show();
    }

    private void manageUi(List<Product> ListItems) {


        snackbar.show();

        if (ListItems == null) {
            imageview.setVisibility(View.VISIBLE);
            maincontent.setVisibility(View.GONE);
            return;
        } else if (ListItems.size() == 0) {
            imageview.setVisibility(View.VISIBLE);
            maincontent.setVisibility(View.GONE);
            return;
        } else {
            imageview.setVisibility(View.GONE);
            maincontent.setVisibility(View.VISIBLE);
            webservicePG.setVisibility(View.GONE);
            adapter = new SharedProductsAdapter(getContext(), controller.getFavorite().getFavoritsItems(), recyclerView, target, session, false, getActivity());
            recyclerView.setAdapter(adapter);
//            adapter.addAll(controller.getFavorite().getFavoritsItems(), session, false);

            return;
        }
    }


    private void loadFavorits() {


        gson = new Gson();
        controller.clearListData();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            Product product = gson.fromJson(jsonString, Product.class);
            if (product != null) {
                controller.setFavoriteProducts(product.getId(), product.getType());
                controller.getFavorite().setFavoritsProducts(product);
            }
        }
    }


    private void loadNextPage() {
        Call<ProductAnswerResponse> getProduct = null;
        if (target.equals("Bicycles Brands") && brandid > 0) {
            getProduct = getBicycles();
        } else if (target.equals("Bicycles Brands") && brandid == 0) {
            getProduct = getAllBicycles();
        } else if (target.equals("Accessories Brands") && brandid > 0) {
            getProduct = getAccessories();
        } else if (target.equals("Accessories Brands") && brandid == 0) {
            getProduct = getAllAccessories();
        }else if (target.equals("OtherProductsSubCatogery") && brandid > 0) {
            getProduct = OtherProductsSubCatogery();
        }

        if (getProduct != null) {
            getProduct.enqueue(new Callback<ProductAnswerResponse>() {
                @Override
                public void onResponse(Call<ProductAnswerResponse> call, Response<ProductAnswerResponse> response) {


                    if (response.isSuccessful()) {

                        if (response.body().getStatus() == 200) {
                            adapter.removeLoadingFooter();
                            isLoading = false;
                            List<Product> results = fetchResults(response);
                            adapter.addAll(results, session, false);
                            adapter.addLoadingFooter();
                        } else {
                            adapter.removeLoadingFooter();
                            isLastPage = true;
                        }
                    } else {
                        int statusCode = response.code();
                        // handle request errors depending on status code
                    }
                }

                @Override
                public void onFailure(Call<ProductAnswerResponse> call, Throwable t) {
    //                adapter. showRetry(true, fetchErrorMessage(t));

                }

            });
        }
    }


    /**
     * @param response extracts List<{@link Product>} from response
     * @return
     */

    private List<Product> fetchResults(Response<ProductAnswerResponse> response) {
        ProductAnswerResponse topRatedMovies = response.body();
        return topRatedMovies.getData().getProduct();
    }

    private Call<ProductAnswerResponse> callHomeService() {
        SharedPreferences session = getContext().getSharedPreferences("Session", MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());;
        if (lang == null) {
            lang = LocaleHelper.getLanguage(getContext());
        }
        if (apitoken == null) {
            return mService.getHome(lang);
        } else {
            return mService.getHome(apitoken, lang);
        }


    }

    private Call<ProductAnswerResponse> getSearchService(String searchToken) {
        SharedPreferences session = getContext().getSharedPreferences("Session", MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        if (apitoken == null || apitoken.isEmpty()) {
            webservicePG.setVisibility(View.GONE);
            searchresult.setText("you must login");
            Toast.makeText(getContext(),"you must login",Toast.LENGTH_SHORT).show();
            return null;
        }
        return mService.getSearchList(apitoken, searchToken);
    }

    private Call<ProductAnswerResponse> getFavoritesService() {
        SharedPreferences session = getContext().getSharedPreferences("Session", MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        if (apitoken == null || apitoken.isEmpty()) {
            webservicePG.setVisibility(View.GONE);
            searchresult.setText("you must login");
            Toast.makeText(getContext(),"you must login",Toast.LENGTH_SHORT).show();
            return null;
        }
        return mService.getFavoratedList(apitoken);
    }

    private Call<ProductAnswerResponse> getAccessories() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());
        if (lang == null) {
            lang = LocaleHelper.getLanguage(getContext());
        }
        if (apitoken == null) {
            return mService.getAccessories(brandid, currentPage, price_from, price_to, size_from, size_to, lang);
        } else {
            return mService.getAccessories(brandid, currentPage, price_from, price_to, size_from, size_to, apitoken, lang);
        }
    }

    private Call<ProductAnswerResponse> OtherProductsSubCatogery() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());;
        if (lang == null) {
            lang = LocaleHelper.getLanguage(getContext());
        }
        if (apitoken == null) {
            return mService.getOtherProduct(brandid, currentPage, price_from, price_to, size_from, size_to, lang);
        } else {
            return mService.getOtherProduct(brandid, currentPage, price_from, price_to, size_from, size_to, apitoken, lang);
        }
    }

    private Call<ProductAnswerResponse> getAllOtherProductsSubCatogery() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());;
        if (lang == null) {
            lang = LocaleHelper.getLanguage(getContext());
        }
        if (apitoken == null) {
            return mService.getAllOtherProduct(currentPage, price_from, price_to, size_from, size_to, lang);
        } else {
            return mService.getAllOtherProduct(currentPage, price_from, price_to, size_from, size_to, apitoken, lang);
        }
    }

    private Call<ProductAnswerResponse> getBicycles() {

        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());;
        if (lang == null) {
            lang = LocaleHelper.getLanguage(getContext());
        }
        if (apitoken == null) {
            return mService.getBicycles(brandid, currentPage, price_from, price_to, size_from, size_to, lang);
        } else {
            return mService.getBicycles(brandid, currentPage, price_from, price_to, size_from, size_to, apitoken, lang);
        }
    }

    private Call<ProductAnswerResponse> getAllBicycles() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());;
        if (lang == null) {
            lang = LocaleHelper.getLanguage(getContext());
        }
        if (apitoken == null) {

            return mService.getAllBicycles(currentPage, price_from, price_to, size_from, size_to, lang);
        } else {
            return mService.getAllBicycles(apitoken, currentPage, price_from, price_to, size_from, size_to, lang);

        }
    }

    private Call<ProductAnswerResponse> getAllAccessories() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());
        if (lang == null) {
            lang = LocaleHelper.getLanguage(getContext());
        }
        if (apitoken == null) {
            return mService.getAllAccessories(currentPage, price_from, price_to, size_from, size_to, lang);
        } else {
            return mService.getAllAccessories(apitoken, currentPage, price_from, price_to, size_from, size_to, lang);
        }
    }


    @Override
    public void executeSevice() {
        delegate.executeSevice();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        view = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null) {
            adapter.add(null);
        }
    }

    public void onSearchViewShown(String[] lstSource, String newText) {
    }

    @Override
    public void retryPageLoad() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getProduct != null && getProduct.isExecuted()) {
            getProduct.cancel();
            iscanceled = true;
        }
    }
}
