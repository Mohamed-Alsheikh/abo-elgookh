package com.abouelgoukh.sporteye.Fragments;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MyProfileFragment;
import com.abouelgoukh.sporteye.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalInfoFragment extends Fragment {

    TextView txteditprofile;

    MyProfileFragment profileFragment;


    public PersonalInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_personal_info, container, false);

        txteditprofile = (TextView)view.findViewById(R.id.txteditprofile);

        final FragmentManager fragmentManager= getFragmentManager();

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        profileFragment = new MyProfileFragment();

        txteditprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.replace(R.id.frame, profileFragment);

                fragmentTransaction.commit();

            }
        });

        return view;
    }

}
