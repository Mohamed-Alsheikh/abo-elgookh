package com.abouelgoukh.sporteye.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Deleget;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MyProfileFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.AddressAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Government;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model.Address;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model.Profile;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.profile_model.ProfileAnswerReponse;
import com.abouelgoukh.sporteye.utils.Validation;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditeProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditeProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditeProfileFragment extends Fragment implements View.OnClickListener, Deleget {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ApiServiceInterface apiServiceInterface;
    View view;
    EditText txtmobilephone, txtEmail, txtusername;

    FloatingActionButton btnrestprofileData;

    ProgressDialog progressDialog;
    ArrayAdapter<String> address_Adapter;
    List<String> addressListstr;
    List<Address> addressList;
    Profile profiledetails;
    ProgressBar main_progress;


    private DataChanagePasswordFragment dataChanagePasswordFragment;
    public Deleget delegate;

    FrameLayout noInternet_image;

    CoordinatorLayout profileCoordinatorLayout;
    Snackbar snackbar;
    SharedPreferences session;
    private Gson gson;
    Call<SimpleResponse> callLoginService;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public EditeProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditeProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditeProfileFragment newInstance(String param1, String param2) {
        EditeProfileFragment fragment = new EditeProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_edite_profile, container, false);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Reset Profile Data ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (callLoginService != null && callLoginService.isExecuted()) {
                    callLoginService.cancel();
                    Toast.makeText(getContext(), "reset profile data canceled", Toast.LENGTH_SHORT).show();

                }
            }
        });
        noInternet_image = (FrameLayout) view.findViewById(R.id.imageview);
        session = getContext().getSharedPreferences("ProfileData", MODE_PRIVATE);

        profileCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.profileCoordinatorLayout);

        addressListstr = new ArrayList<>();
        addressList = new ArrayList<>();
        apiServiceInterface = ApiUtils.getRetrofitObject();
        main_progress = (ProgressBar) view.findViewById(R.id.main_progress);
        txtmobilephone = (EditText) view.findViewById(R.id.txtmobilephone);
        txtEmail = (EditText) view.findViewById(R.id.txtEmail);
        txtusername = (EditText) view.findViewById(R.id.txtusername);
        txtusername.setText(MyProfileFragment.profiledetails.getUsername());
        txtEmail.setText(MyProfileFragment.profiledetails.getEmail());
        txtmobilephone.setText(MyProfileFragment.profiledetails.getMobile());
        btnrestprofileData = (FloatingActionButton) view.findViewById(R.id.btnrestprofileData);
        dataChanagePasswordFragment = new DataChanagePasswordFragment();

        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        noInternet_image.setVisibility(View.GONE);

        if (MainActivity.controller.isNetworkAvailable()) {

            getUserdata();

        } else {
            loadOfflineProfileData();
            if (profiledetails == null) {
                noInternet_image.setVisibility(View.VISIBLE);
            }

            snackbar = Snackbar.make(profileCoordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG).
                    setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    });
            // Changing message text color
            snackbar.setActionTextColor(Color.RED);

            // Changing action button text color
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        }
        btnrestprofileData.setOnClickListener(this);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void resetprofile() {

        final String str_username = txtusername.getText().toString();
        final String str_email = txtEmail.getText().toString();
        final String str_mobile = txtmobilephone.getText().toString();
        if (checkValues(str_username, str_email, str_mobile)) {

            final AlertDialog.Builder adb = new AlertDialog.Builder(getContext());

            adb.setTitle("Do you want to Reset your values?");

            //adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    callResetprofile(str_username, str_email, str_mobile);
                }
            });

            adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            adb.show();


        }

    }

    private boolean checkValues(String str_username, String str_email, String str_mobile) {

        if (!Validation.validate_name(str_username)) {
            txtusername.setError("Invalid Name ");
            return false;
        }
        if (!Validation.validate_Email(str_email)) {
            txtEmail.setError("Invalid Email ");
            return false;
        }
        if (!Validation.validate_mobile(str_mobile)) {
            txtmobilephone.setError("Invalid Mobile Number ");
            return false;
        }
        return true;
    }

    private void callResetprofile(String str_username, String str_email, String str_mobile) {
        progressDialog.show();
        callLoginService = callResetProfileDataService(str_username, str_email, str_mobile);
        callLoginService.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        Toast.makeText(getContext(), "profile Data Changed", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        Toast.makeText(getContext(), "something is required ", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {

                progressDialog.dismiss();
            }

        });

    }

    private Call<SimpleResponse> callResetProfileDataService(String str_username, String str_email, String str_mobile) {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        return apiServiceInterface.resetProfileData(apiToken, str_username, str_email, str_mobile);
    }

    private void saveOffLineProfileData(Profile profile) {
        gson = new Gson();
        String json = gson.toJson(profile);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(profile.getMobile() + "", json);
        editor.apply();
    }

    private void loadOfflineProfileData() {
        // Loop over all geofence keys in prefs and add to namedGeofences

        gson = new Gson();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            profiledetails = gson.fromJson(jsonString, Profile.class);
        }
        main_progress.setVisibility(View.GONE);
    }

    private void getUserdata() {

        callgetMyOrdersDetailsService().enqueue(new Callback<ProfileAnswerReponse>() {
            @Override
            public void onResponse(Call<ProfileAnswerReponse> call, Response<ProfileAnswerReponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        profiledetails = response.body().getProfile();
                        main_progress.setVisibility(View.GONE);
                        saveOffLineProfileData(profiledetails);
                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        main_progress.setVisibility(View.GONE);

                        Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    int statusCode = response.code();
                    main_progress.setVisibility(View.GONE);

                    Toast.makeText(getContext(), "connection error", Toast.LENGTH_SHORT).show();

                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<ProfileAnswerReponse> call, Throwable t) {
                main_progress.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Connection Failed", Toast.LENGTH_SHORT).show();

            }

        });

    }

    private void calldeleteAddress(int address_id, final int selecteditem) {

        callgetdeleteAddressService(address_id).enqueue(new Callback<AddressAnswerResponse>() {
            @Override
            public void onResponse(Call<AddressAnswerResponse> call, Response<AddressAnswerResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        Toast.makeText(getContext(), "Address Deleted ", Toast.LENGTH_SHORT).show();
                        addressListstr.remove(selecteditem);
                        address_Adapter.notifyDataSetChanged();
                        addressList.remove(selecteditem);

                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<AddressAnswerResponse> call, Throwable t) {


            }

        });

    }

    private Call<ProfileAnswerReponse> callgetMyOrdersDetailsService() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(getContext());
        return apiServiceInterface.getProfileDetails(apiToken, lang);
    }

    private Call<AddressAnswerResponse> callgetdeleteAddressService(int address_id) {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        return apiServiceInterface.deleteAddress(apiToken, address_id);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.btnrestprofileData:
                if (MainActivity.controller.isNetworkAvailable()) {
                    resetprofile();
                } else
                    snackbar.show();

                break;

        }
    }


    @Override
    public void onSaveAddress(Integer Address_id, String addressTxt, Government government, String goven_name) {

    }

    @Override
    public void onDeleteCartitem(Double deleted_item_price, String operation, int amount) {

    }


}
