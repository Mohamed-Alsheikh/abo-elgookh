package com.abouelgoukh.sporteye.Fragments.NavBarFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.adapter.MyOrdersAdapter;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.other.GridSpacingItemDecoration;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model.MyOrdersAnswerRespons;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model.Order;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyOrdersFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyOrdersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyOrdersFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView myorderrecyclerView;
    private ProgressBar main_progress;
    private SharedPreferences session;
    private Gson gson = new Gson();
    private FrameLayout maincontent;
    private FrameLayout imageview;
    private CoordinatorLayout CoordinatorLayout_main_content;
    private View view;
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();
    private List<Order> myOrdersList = new ArrayList<>();

    String message;
    public static Snackbar snackbar;
    private OnFragmentInteractionListener mListener;
    private Order unregistered_order;

    public MyOrdersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyOrdersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyOrdersFragment newInstance(String param1, String param2) {
        MyOrdersFragment fragment = new MyOrdersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_my_orders, container, false);
            main_progress = (ProgressBar) view.findViewById(R.id.main_progress);
            maincontent = (FrameLayout) view.findViewById(R.id.myOrdersMainContent);
            imageview = (FrameLayout) view.findViewById(R.id.imageview);
            Controller controller = (Controller) getContext().getApplicationContext();
            CoordinatorLayout_main_content = (CoordinatorLayout) view.findViewById(R.id.CoordinatorLayout_main_content);
            session = getContext().getSharedPreferences("MyOrders", MODE_PRIVATE);
            myorderrecyclerView = (RecyclerView) view.findViewById(R.id.orders_recycler_view);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 1);
            myorderrecyclerView.setLayoutManager(mLayoutManager);
            myorderrecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
            myorderrecyclerView.setItemAnimator(new DefaultItemAnimator());
            if (controller.isNetworkAvailable()) {
                SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
                if (session.contains("order_code")) {
                    try {
                        try {

                            JSONObject order = new JSONObject(session.getString("order_code", ""));
                            ArrayList<Product> items = new ArrayList<>();
                            if (order.getJSONArray("items") != null) {
                                for (int i = 0; i < order.getJSONArray("items").length(); i++) {
                                    items.add(gson.fromJson(order.getJSONArray("items").get(i).toString(), Product.class));
                                }
                            }
                            submitOrder(order.getString("apiToken"), order.getString("order_code"), order.getInt("address_id"), order.getInt("payment_method"), order.getString("mobile_first"), items);
                            SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy");
                            Date order_date = null;

                            order_date = sdf.parse(order.getString("date"));

                            SimpleDateFormat print = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String date = print.format(order_date);
                            Toast.makeText(getContext(), date, Toast.LENGTH_SHORT).show();

                            order_date = print.parse(date);
                            unregistered_order = new Order(1, 3, order.getString("order_code"), date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                getMyOrders();
            } else {
                loadMyorders();
            }
        }
        return view;
    }

    private void loadMyorders() {
        // Loop over all geofence keys in prefs and add to namedGeofences
        gson = new Gson();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            Order order = gson.fromJson(jsonString, Order.class);
            if (order != null) {
                myOrdersList.add(order);
            }
        }
        manageUi(myOrdersList);
    }

    private void saveMyOrder(Order product) {

        gson = new Gson();
        String json = gson.toJson(product);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(product.getId() + "", json);
        editor.apply();
    }

    private void manageUi(List<Order> ListItems) {

        snackbar = Snackbar.make(CoordinatorLayout_main_content, "No internet connection!", Snackbar.LENGTH_LONG).
                setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();

        if (ListItems == null || ListItems.size() == 0) {
            imageview.setVisibility(View.VISIBLE);
            maincontent.setVisibility(View.GONE);
            return;
        } else {
            imageview.setVisibility(View.GONE);
            maincontent.setVisibility(View.VISIBLE);
            main_progress.setVisibility(View.GONE);
            prepareMyOrders(ListItems);
            return;
        }
    }

    private void getMyOrders() {
        callgetMyOrdersService().enqueue(new Callback<MyOrdersAnswerRespons>() {
            @Override
            public void onResponse(Call<MyOrdersAnswerRespons> call, Response<MyOrdersAnswerRespons> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        if (unregistered_order != null) {
                            myOrdersList.add(unregistered_order);
                        }
                        myOrdersList.addAll(response.body().getOrders());
                        prepareMyOrders(myOrdersList);
                        main_progress.setVisibility(View.GONE);

                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                        main_progress.setVisibility(View.GONE);
                    } else if (response.body().getStatus() == 300) {
                        //something is required
                        Toast.makeText(getContext(), "there is no data", Toast.LENGTH_SHORT).show();
                        main_progress.setVisibility(View.GONE);
                    }else {
                        Toast.makeText(getContext(), "Error: "+response.body().getStatus(), Toast.LENGTH_SHORT).show();
                    }


                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                    Toast.makeText(getContext(), "try again", Toast.LENGTH_SHORT).show();
                    main_progress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<MyOrdersAnswerRespons> call, Throwable t) {
                main_progress.setVisibility(View.GONE);
                Toast.makeText(getContext(), "try again", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void prepareMyOrders(List<Order> myOrdersList) {
        MyOrdersAdapter myorderadapter = new MyOrdersAdapter(getContext(), myOrdersList);
        myorderrecyclerView.setAdapter(myorderadapter);
        for (int x = 0; x < myOrdersList.size(); x++) {
            saveMyOrder(myOrdersList.get(x));
        }
    }

    private Call<MyOrdersAnswerRespons> callgetMyOrdersService() {
        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        return mService.getMyOrderts(apiToken);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void submitOrder(String apiToken, String order_code, int address_id, final int pyment_methodint, String mobile_number_str, final ArrayList<Product> items) {
        final Call<SimpleResponse> getAdresses = makeOrder(apiToken, order_code, address_id, pyment_methodint, mobile_number_str, items);

        getAdresses.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
//                Log.d("ccccccccccc", response.body().toString());

                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), "response" + response.body().getStatus(), Toast.LENGTH_SHORT).show();

                    if (response.body().getStatus() == 200) {
                        Toast.makeText(getContext(), "the data is rendered", Toast.LENGTH_SHORT).show();
                        Controller controller = (Controller) getContext().getApplicationContext();
                        SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = session.edit();
                        editor.remove("order_code");
                        editor.commit();

                    } else {
                        Toast.makeText(getContext(), "your order not registered please try again", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(getContext(), "your order not registered please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                Toast.makeText(getContext(), "your order not registered please try again", Toast.LENGTH_SHORT).show();

            }

        });
    }

    private Call<SimpleResponse> makeOrder(String apiToken, String order_code, int address_id, int pyment_methodint, String mobile_number_str, ArrayList<Product> items) {


        Gson gson = new Gson();
        JsonObject main = new JsonObject();

        main.addProperty("apiToken", apiToken);
        main.addProperty("order_code", order_code);
        main.addProperty("address_id", address_id);
        main.addProperty("payment_method", pyment_methodint);
        main.addProperty("mobile_first", mobile_number_str);
        main.addProperty("date", "" + new Date());
        main.add("items", gson.toJsonTree(items));
        return mService.makeOrder(main);
    }
}
