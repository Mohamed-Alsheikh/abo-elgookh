package com.abouelgoukh.sporteye.Fragments.NavBarFragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Activities.SplashScreen;
import com.abouelgoukh.sporteye.Fragments.ProductSlideShowFragment;
import com.abouelgoukh.sporteye.Fragments.SharedProductsFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.ads_model.Ad;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.ads_model.AdsAnswerResponse;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentTransaction transaction;
    ProgressBar main_progress;
    public View view;
    SharedPreferences session;
    private Gson gson;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();
    FragmentActivity f;
    List<Ad> adsList = new ArrayList<>();

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_main, container, false);
            main_progress = (ProgressBar) view.findViewById(R.id.main_progress);
            session = getContext().getSharedPreferences("Adds", MODE_PRIVATE);
            LoadHomeSharedProduct();
            adsList = SplashScreen.adsList;
        }

        if (isNetworkAvailable()) {
            if (adsList.size() > 0) {
                prepareAdds();
            } else {
                getAds();
            }
        } else
            prepareAdds();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        view = null;

        f = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onAttach(Activity activity) {

        f = (FragmentActivity) activity;

        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        view = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void getAds() {
        callAdsService().enqueue(new Callback<AdsAnswerResponse>() {
            @Override
            public void onResponse(Call<AdsAnswerResponse> call, Response<AdsAnswerResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {

                        adsList = response.body().getAds();
                        prepareAdds();

                        main_progress.setVisibility(View.GONE);

                    } else if (response.body().getStatus() == 300) {
                        //the pruduct id is wrong
                        Toast.makeText(getContext(), "the pruduct id is wrong", Toast.LENGTH_SHORT).show();
                        main_progress.setVisibility(View.GONE);
                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        main_progress.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();

                    }


                } else {
                    int statusCode = response.code();

                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<AdsAnswerResponse> call, Throwable t) {
                if (getContext() != null) {
                    Toast.makeText(getContext(), "connection fails in load adds", Toast.LENGTH_SHORT).show();
                    main_progress.setVisibility(View.GONE);
                }
            }

        });
    }

    private Call<AdsAnswerResponse> callAdsService() {

        return mService.getAds();
    }


    private void replaceFragment(Fragment fragment) {

    }

    private void saveGeofence(Ad add) {

        gson = new Gson();
        String json = gson.toJson(add);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(add.getId() + "", json);
        editor.apply();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void loadFavorits() {
        adsList.clear();
        gson = new Gson();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            Ad adds = gson.fromJson(jsonString, Ad.class);
            if (adds != null) {
                adsList.add(adds);
            }
        }
    }

    private void LoadHomeSharedProduct() {

        if (getChildFragmentManager().findFragmentByTag("SharedProductsFragment") != null) {
            Fragment fragment = getChildFragmentManager().findFragmentByTag("SharedProductsFragment");
            fragment.onDetach();
            transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.child_fragment, fragment, "SharedProductsFragment").commit();
        } else {
            Fragment sharedProductsFragmentFragment = new SharedProductsFragment();
            Bundle arguments = new Bundle();
            arguments.putString("target", "Home");
            sharedProductsFragmentFragment.setArguments(arguments);
            transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.child_fragment, sharedProductsFragmentFragment, "SharedProductsFragment").commit();
        }
        main_progress.setVisibility(View.GONE);

    }

    private void prepareAdds() {
        if (!isNetworkAvailable()) {
            loadFavorits();
        }
//
        ArrayList<String> productImages = new ArrayList<>();
        for (int x = 0; x < adsList.size(); x++) {
            productImages.add(adsList.get(x).getPath());
            if (isNetworkAvailable()) {
                saveGeofence(adsList.get(x));
            }
        }


        if (getActivity() == null)
            return;

        if (getActivity() instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) getActivity();
            Bundle slideimagearguments = new Bundle();
            slideimagearguments.putStringArrayList("singleProduct", productImages);
            Fragment slideimage = new ProductSlideShowFragment();
            slideimage.setArguments(slideimagearguments);
            mainActivity.switchContentAddSlideShow(slideimage, productImages.size());
        }

//        if (getChildFragmentManager().findFragmentByTag("slideimagearguments") != null) {
//
//            Fragment fragment = getChildFragmentManager().findFragmentByTag("slideimagearguments");
//            transaction = getChildFragmentManager().beginTransaction();
//            transaction.replace(R.id.slodeshow_child_fragment, fragment, "slideimagearguments");
//            transaction.commitAllowingStateLoss();
//            Toast.makeText(getContext(), "slid again", Toast.LENGTH_SHORT).show();
//
//
//        } else {
//            Bundle slideimagearguments = new Bundle();
//            slideimagearguments.putStringArrayList("singleProduct", productImages);
//            Fragment slideimage = new ProductSlideShowFragment();
//            slideimage.setArguments(slideimagearguments);
//            Toast.makeText(getContext(), "slid first", Toast.LENGTH_SHORT).show();
//
//            transaction.replace(R.id.slodeshow_child_fragment, slideimage, "slideimagearguments");
//            transaction.commitAllowingStateLoss();
//
//        }

    }

}
