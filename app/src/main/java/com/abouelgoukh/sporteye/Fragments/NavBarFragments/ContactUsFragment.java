package com.abouelgoukh.sporteye.Fragments.NavBarFragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ShareCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.model.locationcompany;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.contact_us.Contact;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.contact_us.ContactAnswerResponse;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContactUsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContactUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactUsFragment extends Fragment implements OnMapReadyCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    //    private Button btnlocation;
    GoogleMap mMap;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    MapView mapView;
    GoogleMap map;
    View view;
    Handler handler;
    ImageView call, sendEmail;
    Runnable r;
    ApiServiceInterface apiServiceInterface = ApiUtils.getRetrofitObject();
    TextView txtdisplayaddress;
    TextView txtdisplaymobile;
    TextView txtdisplayemail;
    SupportMapFragment mapFragment;
    FrameLayout noInternet_image;
    LinearLayout contactusData;
    CoordinatorLayout contactusCoordinatorLayout;
    Snackbar snackbar;
    SharedPreferences session;
    ProgressBar main_progress;
    private Gson gson;
    private OnFragmentInteractionListener mListener;
    String Address = null;
    LatLng aboAlgookh;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactUsFragment.
     */

    // TODO: Rename and change types and number of parameters
    public static ContactUsFragment newInstance(String param1, String param2) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view==null) {

            view = inflater.inflate(R.layout.fragment_contact_us, container, false);
            sendEmail =  view.findViewById(R.id.sendEmail);
            call =  view.findViewById(R.id.imageView2);
            main_progress = (ProgressBar) view.findViewById(R.id.main_progress);
            noInternet_image = (FrameLayout) view.findViewById(R.id.imageview);
            session = getContext().getSharedPreferences("ContactyUsData", MODE_PRIVATE);
            contactusData = (LinearLayout) view.findViewById(R.id.contactusData);
            contactusCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.contactusCoordinatorLayout);
            txtdisplayaddress = (TextView) view.findViewById(R.id.contactustxtdisplayaddress);
            txtdisplaymobile = (TextView) view.findViewById(R.id.contactustxtdisplaymobile);
            txtdisplayemail = (TextView) view.findViewById(R.id.contactustxtdisplayemail);
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!txtdisplaymobile.getText().toString().isEmpty()) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+ txtdisplaymobile.getText().toString()));
                        startActivity(intent);
                    }else {
                        Toast.makeText(getContext(),getResources().getString(R.string.noPhoneNumber),Toast.LENGTH_SHORT).show();
                    }
                }
            });
            sendEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:"));
                    intent.putExtra(Intent.EXTRA_EMAIL  , new String[] { txtdisplayemail.getText().toString() });
                    intent.putExtra(Intent.EXTRA_SUBJECT, "My subject");

                    startActivity(Intent.createChooser(intent, "Send feedback"));
                }
            });
//        btnlocation = (Button) view.findViewById(R.id.btnlocation);
            noInternet_image.setVisibility(View.GONE);
            contactusData.setVisibility(View.VISIBLE);
            if (MainActivity.controller.isNetworkAvailable()) {
                callCompanyINFO();
                handler = new Handler();
                r = new Runnable() {
                    public void run() {
                        createMap();
                    }
                };
                handler.postDelayed(r, 1000);

            } else {
                Contact contact = loadOffLineContactUsData();
                if (contact == null) {
                    noInternet_image.setVisibility(View.VISIBLE);
                    contactusData.setVisibility(View.GONE);
                }
                snackbar = Snackbar.make(contactusCoordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG).
                        setAction("RETRY", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        });
                // Changing message text color
                snackbar.setActionTextColor(Color.RED);

                // Changing action button text color
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();

            }


            FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (MainActivity.controller.isNetworkAvailable()) {
                        MassageFragment massageFragment = new MassageFragment();
                        if (getContext() == null)
                            return;
                        if (getContext() instanceof MainActivity) {
                            MainActivity mainActivity = (MainActivity) getContext();
                            mainActivity.switchContent(massageFragment, 13);
                        }
                    } else {
                        snackbar.show();
                    }

                }
            });
        }
        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        view=null;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void saveContactUsdata(Contact contact) {

        gson = new Gson();
        String json = gson.toJson(contact);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(contact.getMobile().toString() + "", json);
        editor.apply();
    }

    private Contact loadOffLineContactUsData() {
        // Loop over all geofence keys in prefs and add to namedGeofences

        gson = new Gson();
        Contact contact = null;
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            contact = gson.fromJson(jsonString, Contact.class);
            if (contact != null) {
                txtdisplayaddress.setText(contact.getAddress());
                txtdisplaymobile.setText(contact.getMobile().toString());
                txtdisplayemail.setText("aboelgokhapp@gmail.com");
            }
        }
        main_progress.setVisibility(View.GONE);
        return contact;
    }

    private void callCompanyINFO() {
        callContactUsService().enqueue(new Callback<ContactAnswerResponse>() {
            @Override
            public void onResponse(Call<ContactAnswerResponse> call, Response<ContactAnswerResponse> response) {

                if (response.isSuccessful()) {
                    main_progress.setVisibility(View.GONE);
                    if (response.body().getStatus() == 200) {
                        saveContactUsdata(response.body().getContacts());
                        Address = response.body().getContacts().getAddress();
                        txtdisplayaddress.setText(response.body().getContacts().getAddress());
                        txtdisplaymobile.setText(response.body().getContacts().getMobile().toString());
                        txtdisplayemail.setText("aboelgokhapp@gmail.com");
                        if (mMap != null) {
                            mMap.addMarker(new MarkerOptions().position(aboAlgookh).title(Address)).showInfoWindow();
                        }
                    } else if (response.body().getStatus() == 402) {
                    }


                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<ContactAnswerResponse> call, Throwable t) {
                main_progress.setVisibility(View.GONE);
                if( getContext()!=null) {
                    Toast.makeText(getContext(), "connection fail", Toast.LENGTH_SHORT).show();
                }

            }

        });

    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);

    }

    private void createMap() {
        final locationcompany locationcompany = new locationcompany();
        locationcompany.setLatitude("30.035264");
        locationcompany.setLangtitude("31.279304");
        LinearLayout location = (LinearLayout) view.findViewById(R.id.locationlayout);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String urlAddress = "http://maps.google.com/maps?q=" + locationcompany.getLatitude() + "," + locationcompany.getLangtitude() + "(" + "AbuAlgokh" + ")";
                Intent maps = new Intent(Intent.ACTION_VIEW, Uri.parse(urlAddress));
                startActivity(maps);

            }
        });
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(ContactUsFragment.this);
            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.map, mapFragment)
                    .commitAllowingStateLoss();
        }
    }

    private Call<ContactAnswerResponse> callContactUsService() {
        return apiServiceInterface.ContactUsService();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        aboAlgookh = new LatLng(30.035264, 31.279304);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);


        // Add a marker in Sydney and move the camera


        // Set a preference for minimum and maximum zoom.
//        mMap.setMinZoomPreference(20.0f);
//        mMap.setMaxZoomPreference(5.0f);

        //        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        CameraUpdate center =
                CameraUpdateFactory.newLatLng(aboAlgookh);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        if (Address != null) {
            mMap.addMarker(new MarkerOptions().position(aboAlgookh).title(Address)).showInfoWindow();
        }
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
