package com.abouelgoukh.sporteye.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.adapter.ProductSlideShowAdapter;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.utils.CirclePageIndicator;
import com.abouelgoukh.sporteye.utils.PageIndicator;

public class ProductSlideShowFragment extends Fragment {

    public static final String ARG_ITEM_ID = "home_fragment";

    private static final long ANIM_VIEWPAGER_DELAY = 5000;
    private static final long ANIM_VIEWPAGER_DELAY_USER_VIEW = 5000;

    // UI References
    ArrayList<String>imageList;
    private ViewPager mViewPager;
//    TextView imgNameTxt;
    PageIndicator mIndicator;

    AlertDialog alertDialog;
    ProgressBar progressBar ;
    List<Product> products;
//    RequestImgTask task;
    boolean stopSliding = false;
    String message;

    private Runnable animateViewPager;
    private Handler handler = new Handler();


    FragmentActivity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_slide_show, container, false);
        findViewById(view);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            imageList = bundle.getStringArrayList("singleProduct");
        }
        mIndicator.setOnPageChangeListener(new PageChangeListener());
        mViewPager.setOnPageChangeListener(new PageChangeListener());
        mViewPager.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction()) {

                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        // calls when touch release on ViewPager
                        if (imageList != null && imageList.size() != 0) {

                            stopSliding = false;
                            runnable(imageList.size());
                            handler.postDelayed(animateViewPager,
                                    ANIM_VIEWPAGER_DELAY_USER_VIEW);
                        }
                        break;

                    case MotionEvent.ACTION_MOVE:
                        // calls when ViewPager touch
                        if (handler != null && stopSliding == false) {
                            stopSliding = true;
                            handler.removeCallbacks(animateViewPager);
                        }
                        break;
                }
                return false;
            }
        });

        return view;
    }

    private void findViewById(View view) {
        mViewPager = (ViewPager) view.findViewById(R.id.product_view_pager);
        mIndicator = (CirclePageIndicator) view.findViewById(R.id.product_indicator);
        progressBar = (ProgressBar) view. findViewById(R.id.progress);
//        imgNameTxt = (TextView) view.findViewById(R.id.product_img_name);
    }

    public void runnable(final int size) {

        animateViewPager = new Runnable() {
            public void run() {
                if (!stopSliding) {
                    if (mViewPager.getCurrentItem() == size - 1) {
                        mViewPager.setCurrentItem(0);
                    } else {
                        mViewPager.setCurrentItem(
                                mViewPager.getCurrentItem() + 1, true);
                    }
                    handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
                }
            }
        };
    }
boolean created = false;
    @Override
    public void onResume() {
        if (imageList == null) {
//            Toast.makeText(getContext(),"fff",Toast.LENGTH_SHORT).show();
        } else {
            if (created) {
               return;
            }
            mViewPager.setAdapter(new ProductSlideShowAdapter( activity, imageList, ProductSlideShowFragment.this));

            mIndicator.setViewPager(mViewPager);
//            imgNameTxt.setText("Test Image" );
            runnable(imageList.size());
            //Re-run callback
            handler.removeCallbacks(animateViewPager);
            handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
        }
        super.onResume();
    }


    @Override
    public void onPause() {
//        if (task != null)
//            task.cancel(true);
//        if (handler != null) {
//            //Remove callback
//            handler.removeCallbacks(animateViewPager);
//        }
        super.onPause();
    }

//    private void sendRequest() {
//        if (CheckNetworkConnection.isConnectionAvailable(activity)) {
//            task = new RequestImgTask(activity);
//            task.execute(url);
//        } else {
//            message = getResources().getString(R.string.no_internet_connection);
//            showAlertDialog(message, true);
//        }
//    }

    public void showAlertDialog(String message, final boolean finish) {
        alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (finish)
                            activity.finish();
                    }
                });
        alertDialog.show();
    }

    private class PageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                if (products != null) {
//                    imgNameTxt.setText("" + ((Product) products.get(mViewPager.getCurrentItem())).getName());
                }
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int arg0) {
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
