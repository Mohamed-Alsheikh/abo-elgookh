package com.abouelgoukh.sporteye.Fragments.NavBarFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.utils.Validation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.abouelgoukh.sporteye.Activities.MainActivity.swipeRefreshLayout;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MassageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MassageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MassageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText txtyourname, txtyourmobile, txtyourmessaage, txtyourfeedback;
    //    Button send;
    ApiServiceInterface apiServiceInterface = ApiUtils.getRetrofitObject();
    private OnFragmentInteractionListener mListener;
    ProgressDialog progressDialog;
    public MassageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MassageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MassageFragment newInstance(String param1, String param2) {
        MassageFragment fragment = new MassageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_massage, container, false);
        txtyourname = (EditText) view.findViewById(R.id.txtyourname);
        txtyourmobile = (EditText) view.findViewById(R.id.txtyourmobile);
        txtyourmessaage = (EditText) view.findViewById(R.id.txtyourmessaage);
        txtyourfeedback = (EditText) view.findViewById(R.id.txtyourfeedback);
//                send = (Button) view.findViewById(R.id.send);
        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stryourname = txtyourname.getText().toString();
                if (!Validation.validate_name(stryourname)) {
                    txtyourname.setError("enter valid name");
                    return;
                }
                String stryourmobile = txtyourmobile.getText().toString();
                if (!Validation.validate_mobile(stryourmobile)) {
                    txtyourmobile.setError("enter valid mobile");
                    return;
                }
                String stryourmessaagetile = txtyourmessaage.getText().toString();
                if (!Validation.validate_name(stryourmessaagetile)) {
                    txtyourmessaage.setError("enter valid product_name");
                    return;
                }
                String stryourfeedback = txtyourfeedback.getText().toString();
                if (stryourfeedback.isEmpty()) {
                    txtyourfeedback.setError("enter valid massage");
                    return;
                }
                final AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
                adb.setTitle("Send Maintainance Request?");
                //adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(fab.getWindowToken(), 0);

                        progressDialog.show();
                        callFeedBack(stryourname, stryourmobile, stryourmessaagetile, stryourfeedback);

                    }
                });

                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                adb.show();


            }
        });
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getResources().getString(R.string.sendMessagePleaseWait));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                sendMessage.cancel();
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private Call<SimpleResponse> sendMessage;
    private void callFeedBack(String stryourname, String stryourmobile, String stryourmessaagetile, String stryourfeedback) {

         sendMessage= callFeedBackService(stryourname, stryourmobile, stryourmessaagetile, stryourfeedback);
        sendMessage.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        Toast.makeText(getContext(), getResources().getString(R.string.yourFeedBackSent), Toast.LENGTH_SHORT).show();


                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        Toast.makeText(getContext(), "something is required for FeedBack", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                    Toast.makeText(getContext(), "not success", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                Toast.makeText(getContext(), getResources().getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

        });

    }


    private Call<SimpleResponse> callFeedBackService(String stryourname, String stryourmobile, String stryourmessaagetile, String stryourfeedback) {
        return apiServiceInterface.FeedBackService(stryourname, stryourmobile, stryourmessaagetile, stryourfeedback);
    }

    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        swipeRefreshLayout.setEnabled(true);
    }
}
