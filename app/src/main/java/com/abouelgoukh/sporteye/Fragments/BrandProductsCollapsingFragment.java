package com.abouelgoukh.sporteye.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.readystatesoftware.viewbadger.BadgeView;

import java.util.ArrayList;
import java.util.List;

import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Interfaces.ExecuteServiceDelegate;
import com.abouelgoukh.sporteye.ProductBrandDelegate;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.Brand;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link BrandProductsCollapsingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BrandProductsCollapsingFragment extends Fragment implements AdapterView.OnItemSelectedListener, ProductBrandDelegate {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    ProductBrandDelegate productBrandDelegate;
    ImageView pricesfrompinnericon;
    ImageView pricestopinnericon;
    ImageView sizefrompinnericon;
    ImageView sizetopinnericon;
    Spinner pricefromspinner;
    Spinner pricetospinner;
    Spinner sizefromspinner;
    Spinner sizetospinner;

    ArrayList<String> price_from_list = new ArrayList<>();
    ArrayList<String> price_to_list = new ArrayList<>();
    ArrayList<String> size_from_list = new ArrayList<>();
    ArrayList<String> size_to_list = new ArrayList<>();
    ArrayAdapter<String> price_from_Adapter;
    ArrayAdapter<String> price_to_Adapter;
    ArrayAdapter<String> size_from_Adapter;
    ArrayAdapter<String> size_to_Adapter;

    ImageView productbrandimage;
    private int brandid, Brand_Items;
    private String  image_Path, brand_name;
    private String mParam2;
    int position;
    int All_Content;
    SharedProductsFragment sharedProductsFragmentFragment;
    boolean flag_fire_from = false;
    boolean flag_fire_to = false;
    boolean flag_fire_size_from = false;
    boolean flag_fire_size_to = false;
    boolean flag_price_rang_ok = true;
    boolean flag_size_rang_ok = true;
    String Brand_type;
    ArrayList<String> brands_names;
    ArrayList<String> brands_images;
    ArrayList<String> brands_Item_Count;
    List<Brand> brandsList;
    private BrandProductsFragment.OnFragmentInteractionListener mListener;
    ExecuteServiceDelegate this_delegate;
    Spinner spinner;
    LinearLayout layoutfiltersize;
    ImageView spinnericon;
    ArrayAdapter<String> brandAdapter;
    public static BadgeView messageCenterBadge;

    public BrandProductsCollapsingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BrandProductsCollapsingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BrandProductsCollapsingFragment newInstance(String param1, String param2) {
        BrandProductsCollapsingFragment fragment = new BrandProductsCollapsingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_brand_products_collapsing, container, false);
        Bundle bundle = this.getArguments();

        layoutfiltersize=(LinearLayout)view.findViewById(R.id.layoutfiltersize);
        if (bundle != null) {
            brands_names = bundle.getStringArrayList("brands_names");
            brands_images = bundle.getStringArrayList("brands_images");
            brands_Item_Count = bundle.getStringArrayList("brands_Item_Count");
            position = bundle.getInt("position");
            All_Content = bundle.getInt("All_Content");
            Brand_type = bundle.getString("Brand_type");
        }

        productbrandimage = (ImageView) view.findViewById(R.id.productbrandimage);
        spinner = (Spinner) view.findViewById(R.id.spinner);
        spinnericon = (ImageView) view.findViewById(R.id.spinnericon);

        pricefromspinner = (Spinner) view.findViewById(R.id.pricefromspinner);
        pricetospinner = (Spinner) view.findViewById(R.id.pricetopinner);
        sizefromspinner = (Spinner) view.findViewById(R.id.sizefromspinner);
        sizetospinner = (Spinner) view.findViewById(R.id.sizetopinner);

        pricesfrompinnericon = (ImageView) view.findViewById(R.id.pricesfrompinnericon);
        pricestopinnericon = (ImageView) view.findViewById(R.id.pricestopinnericon);
        sizefrompinnericon = (ImageView) view.findViewById(R.id.sizefrompinnericon);
        sizetopinnericon = (ImageView) view.findViewById(R.id.sizetopinnericon);



        price_from_list.add("From");
        price_from_list.add("500");
        price_from_list.add("1000");
        price_from_list.add("2000");
        price_from_list.add("3000");
        price_from_list.add("4000");
        price_from_list.add("5000");
        price_from_list.add("6000");
        price_from_list.add("7000");
        price_from_list.add("8000");
        price_from_list.add("9000");
        price_from_list.add("10000");
        price_from_list.add("20000");
        price_from_list.add("30000");
        price_from_list.add("40000");
        price_from_list.add("50000");
        price_from_list.add("60000");
        price_from_list.add("70000");
        price_from_list.add("80000");
        price_from_list.add("90000");
        price_from_list.add("100000");
        price_from_list.add("120000");
        price_from_list.add("130000");
        price_from_list.add("140000");
        price_from_list.add("150000");


        price_to_list.add("to");
        price_to_list.add("500");
        price_to_list.add("1000");
        price_to_list.add("2000");
        price_to_list.add("3000");
        price_to_list.add("4000");
        price_to_list.add("5000");
        price_to_list.add("6000");
        price_to_list.add("7000");
        price_to_list.add("8000");
        price_to_list.add("9000");
        price_to_list.add("10000");
        price_to_list.add("20000");
        price_to_list.add("30000");
        price_to_list.add("40000");
        price_to_list.add("50000");
        price_to_list.add("60000");
        price_to_list.add("70000");
        price_to_list.add("80000");
        price_to_list.add("90000");
        price_to_list.add("100000");
        price_to_list.add("120000");
        price_to_list.add("130000");
        price_to_list.add("140000");
        price_to_list.add("150000");

        size_from_list.add("From");
        size_from_list.add("9");
        size_from_list.add("12");
        size_from_list.add("16");
        size_from_list.add("18");
        size_from_list.add("21");
        size_from_list.add("24");
        size_from_list.add("26");
        size_from_list.add("28");


        size_to_list.add("To");
        size_to_list.add("9");
        size_to_list.add("12");
        size_to_list.add("16");
        size_to_list.add("18");
        size_to_list.add("21");
        size_to_list.add("24");
        size_to_list.add("26");
        size_to_list.add("28");


        price_from_Adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, price_from_list);
        spinnerInit(pricefromspinner, price_from_Adapter);

        price_to_Adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, price_to_list);
        spinnerInit(pricetospinner, price_to_Adapter);


        size_from_Adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, size_from_list);
        spinnerInit(sizefromspinner, size_from_Adapter);

        size_to_Adapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, size_to_list);
        spinnerInit(sizetospinner, size_to_Adapter);


        if(Brand_type.equals("OtherProductsSubCatogery")){
            layoutfiltersize.setVisibility(View.GONE);
        }
        messageCenterBadge = new BadgeView(getContext(), productbrandimage);
        messageCenterBadge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        messageCenterBadge.setBadgeMargin(0);
        messageCenterBadge.setTextSize(10);
        MainActivity.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBrandOrButtonClicked(spinner.getSelectedItemPosition());
            }
        });
        actionHandeling();
        SpinnerBrandsinit();
        return view;

    }

    private void SpinnerBrandsinit() {
        if (getActivity() != null) {
            brandAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner_item, brands_names);
            brandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(brandAdapter);
        }
        if (position >= 0) {
            spinner.setSelection(position);
        } else {
            position = brands_names.size() - 1;
            spinner.setSelection(position);
        }


        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

    }

    private void spinnerInit(Spinner spinner, ArrayAdapter<String> Adapter) {
        // Drop down layout style - list view with radio button
        Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(Adapter);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
    }

    private void actionHandeling() {
        spinnericon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDown("spinnericon");
            }
        });
        pricesfrompinnericon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDown("pricesfrompinnericon");
            }
        });
        pricestopinnericon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDown("pricestopinnericon");
            }
        });
        sizefrompinnericon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDown("sizefrompinnericon");
            }
        });
        sizetopinnericon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDropDown("sizetopinnericon");
            }
        });
    }

    public void showDropDown(String source) {
        if (source.equals("spinnericon")) {
            spinner.performClick();
        } else if (source.equals("pricesfrompinnericon")) {
            pricefromspinner.performClick();
        } else if (source.equals("pricestopinnericon")) {
            pricetospinner.performClick();
        } else if (source.equals("sizefrompinnericon")) {
            sizefromspinner.performClick();
        } else if (source.equals("sizetopinnericon")) {
            sizetospinner.performClick();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.pricefromspinner:
                if (flag_fire_from) {
                    onPriceFromSelected();
                }
                flag_fire_from = true;
                break;


            case R.id.pricetopinner:
                if (flag_fire_to) {
                    onPriceToSelected();
                }
                flag_fire_to = true;
                break;

            case R.id.sizefromspinner:
                if (flag_fire_size_from) {
                    onSizeFromSelected();
                }
                flag_fire_size_from = true;
                break;


            case R.id.sizetopinner:
                if (flag_fire_size_to) {
                    onSizeToSelected();
                }
                flag_fire_size_to = true;
                break;


            case R.id.spinner:
                onBrandOrButtonClicked(position);
                break;
        }
    }

   public void onBrandOrButtonClicked(int position){

        String price_from = null;
        String price_to = null;
        String size_from = null;
        String size_to = null;
        if (flag_price_rang_ok) {
            if (pricefromspinner.getSelectedItemPosition() != 0) {
                price_from = price_from_list.get(pricefromspinner.getSelectedItemPosition());
            }
            if (pricetospinner.getSelectedItemPosition() != 0) {
                price_to = price_to_list.get(pricetospinner.getSelectedItemPosition());
            }
        } else {
            Toast.makeText(getContext(), "Price Rang invalid", Toast.LENGTH_SHORT).show();
            return;
        }
        if (flag_size_rang_ok) {
            if (sizefromspinner.getSelectedItemPosition() != 0) {
                size_from = size_from_list.get(sizefromspinner.getSelectedItemPosition());
            }
            if (sizetospinner.getSelectedItemPosition() != 0) {
                size_to = size_to_list.get(sizetospinner.getSelectedItemPosition());

            }
        } else {
            Toast.makeText(getContext(), "Price Rang invalid", Toast.LENGTH_SHORT).show();
            return;
        }
        if (MainActivity.controller.isNetworkAvailable()) {
            onChangBrand(position, price_from, price_to, size_from, size_to);
        }
    }

    private void onPriceToSelected() {

        if (pricefromspinner.getSelectedItemPosition() != 0 && pricetospinner.getSelectedItemPosition() != 0) {

            int price_to = Integer.parseInt(price_to_list.get(pricetospinner.getSelectedItemPosition()));
            int price_from = Integer.parseInt(price_from_list.get(pricefromspinner.getSelectedItemPosition()));
            if (price_to < price_from) {
                flag_price_rang_ok = false;
            } else
                flag_price_rang_ok = true;

        } else
            flag_price_rang_ok = true;

    }

    private void onSizeFromSelected() {

        if (sizefromspinner.getSelectedItemPosition() != 0 && sizetospinner.getSelectedItemPosition() != 0) {
            int price_to = Integer.parseInt(size_to_list.get(sizetospinner.getSelectedItemPosition()));
            int price_from = Integer.parseInt(size_from_list.get(sizefromspinner.getSelectedItemPosition()));
            if (price_to < price_from) {
                flag_size_rang_ok = false;
            } else
                flag_size_rang_ok = true;
            Toast.makeText(getContext(), "flag_size_rang_ok " + flag_size_rang_ok, Toast.LENGTH_SHORT).show();
        } else
            flag_size_rang_ok = true;

    }

    private void onSizeToSelected() {

        if (sizefromspinner.getSelectedItemPosition() != 0 && sizetospinner.getSelectedItemPosition() != 0) {

            int price_to = Integer.parseInt(size_to_list.get(sizetospinner.getSelectedItemPosition()));
            int price_from = Integer.parseInt(size_from_list.get(sizefromspinner.getSelectedItemPosition()));
            if (price_to < price_from) {
                flag_size_rang_ok = false;
            } else
                flag_size_rang_ok = true;
            Toast.makeText(getContext(), "flag_size_rang_ok " + flag_size_rang_ok, Toast.LENGTH_SHORT).show();
        } else
            flag_size_rang_ok = true;

    }

    private void onPriceFromSelected() {

        if (pricefromspinner.getSelectedItemPosition() != 0 && pricetospinner.getSelectedItemPosition() != 0) {
            int price_to = Integer.parseInt(price_to_list.get(pricetospinner.getSelectedItemPosition()));
            int price_from = Integer.parseInt(price_from_list.get(pricefromspinner.getSelectedItemPosition()));
            if (price_to < price_from) {
                flag_price_rang_ok = false;
            } else
                flag_price_rang_ok = true;
            Toast.makeText(getContext(), "flag_price_rang_ok " + flag_price_rang_ok, Toast.LENGTH_SHORT).show();
        } else
            flag_price_rang_ok = true;

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onChangBrand(int addressTxt, String price_from, String price_to, String size_from, String size_to) {


        if (addressTxt == brands_names.size() - 1) {

            Glide.with(getContext()).
                    load(R.drawable.bicycle)
                    .apply(RequestOptions.circleCropTransform()).into(productbrandimage);


            messageCenterBadge.setText(String.valueOf(All_Content));
            messageCenterBadge.show();

        } else {
            Glide.with(getContext()).
                    load(brands_images.get(addressTxt))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                    .apply(RequestOptions.circleCropTransform())
                    .into(productbrandimage);

            messageCenterBadge.setText(String.valueOf(brands_Item_Count.get(addressTxt)));
            messageCenterBadge.show();
        }

        productBrandDelegate.onChangBrand(addressTxt, price_from, price_to, size_from, size_to);
    }
}
