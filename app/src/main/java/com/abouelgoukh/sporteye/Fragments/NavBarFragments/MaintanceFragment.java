package com.abouelgoukh.sporteye.Fragments.NavBarFragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.other.ProgressRequestBody;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.utils.Validation;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class MaintanceFragment extends Fragment implements EasyPermissions.PermissionCallbacks, ProgressRequestBody.UploadCallbacks {

    //views
    private FloatingActionButton btnOpenCamera, btnBrowse, send;

    private ImageView imageView;
    EditText txtprodutcname, txtprodutcdescription;
    ApiServiceInterface apiServiceInterface = ApiUtils.getRetrofitObject();
    ProgressDialog progressDialog;
    private int TAKE_PICTURE_CODE_THIS_FRAGMENT = 1;
    private static final int REQUEST_GALLERY_CODE = 200;
    private static final int READ_REQUEST_CODE = 300;
    String apiToken;
    private int PICK_IMAGE_FROM_YOUR_GALLERY = 2;
    Bitmap myBitmap;
    String image_Bath = "";
    Uri uri;
    CoordinatorLayout maintainanceCoordinatorLayout;
    Call<SimpleResponse> callmaintainanceService;

    public MaintanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_maintance, container, false);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( keyCode == KeyEvent.KEYCODE_BACK ) {

                    // leave this blank in order to disable the back press
                    return true;
                } else {
                    return false;
                }
            }
        });

        Handler handler=new Handler();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                view.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if( keyCode == KeyEvent.KEYCODE_BACK ) {

                            // leave this blank in order to disable the back press
                            return false;
                        } else {
                            return false;
                        }
                    }
                });
            }
        };
        handler.postDelayed(runnable,300);
        btnOpenCamera = (FloatingActionButton) view.findViewById(R.id.btnopencamera);
        imageView = (ImageView) view.findViewById(R.id.imageview);
        txtprodutcname = (EditText) view.findViewById(R.id.txtprodutcname);
        txtprodutcdescription = (EditText) view.findViewById(R.id.txtprodutcdescription);
        maintainanceCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.maintainanceCoordinatorLayout);
        FloatingActionButton send = (FloatingActionButton) view.findViewById(R.id.fab);
        assert send != null;

        btnOpenCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                if (EasyPermissions.hasPermissions(getContext(),perms)) {
                    OpenCamera();
                } else {
                    EasyPermissions.requestPermissions(MaintanceFragment.this, getString(R.string.read_file), READ_REQUEST_CODE, perms);
                }

            }
        });
        btnBrowse = (FloatingActionButton) view.findViewById(R.id.btnbrowse);
//        send = (Button) view.findViewById(R.id.send);

        btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (EasyPermissions.hasPermissions(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                    openGalleryIntent.setType("image/*");
                    startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
                } else {
                    EasyPermissions.requestPermissions(MaintanceFragment.this, getString(R.string.read_file), REQUEST_GALLERY_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            }
        });

//        send.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                maintainance();
//            }
//        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.controller.isNetworkAvailable()) {
                    SharedPreferences session = getContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
                    apiToken = session.getString("APITOKEN", null);
                    if (apiToken != null) {
                        maintainance();
                    } else
                        Toast.makeText(getContext(), "You must login to send request", Toast.LENGTH_SHORT).show();
                } else {
                    Snackbar snackbar = Snackbar.make(maintainanceCoordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG).
                            setAction("RETRY", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                }
                            });
                    // Changing message text color
                    snackbar.setActionTextColor(Color.RED);

                    // Changing action button text color
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }
            }
        });
        return view;
    }

    private void OpenCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, TAKE_PICTURE_CODE_THIS_FRAGMENT);

    }

    private void processCameraImage(Intent intent) {
        if (intent != null) {
            myBitmap = (Bitmap) intent.getExtras().get("data");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.PNG, 70, stream);
            Glide.with(getContext())
                    .asBitmap()
                    .load(stream.toByteArray())
                    .into(imageView);
//            imageView.setImageBitmap(myBitmap);
            if (intent.getData() != null) {
                uri = intent.getData();
            } else {
                String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), myBitmap, "Title", null);
                uri = Uri.parse(path);
            }
            image_Bath = initImageUri(uri);
        }
    }

    //----------------------------------------
//where we have to pass the contentUri on Image Capture Action
    private String initImageUri(Uri contentUri) {
        String imagePath;
        try {

            Cursor cursor = getContext().getContentResolver().query(contentUri, null, null,
                    null, null);
            int index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            imagePath = cursor.getString(index);
            cursor.close();
            // Log.i("imagePath", imagePath);
            return imagePath;
        } catch (Exception e) {
            Log.e("MYAPP", "exception", e);
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (TAKE_PICTURE_CODE_THIS_FRAGMENT == requestCode) {
            //set data to processCameraImage()
            processCameraImage(data);
        } else if (requestCode == REQUEST_GALLERY_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Pick_Image_From_Gallery_Deivce(data);
        }
    }

    private void Pick_Image_From_Gallery_Deivce(Intent data) {

        uri = data.getData();
        if (EasyPermissions.hasPermissions(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
//            image_Bath = getRealPathFromURIPath(uri, getActivity());
            image_Bath = initImageUri(uri);
            Toast.makeText(getContext(), "paht" + image_Bath, Toast.LENGTH_SHORT).show();

            try {
                myBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                imageView.setImageBitmap(myBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_file), REQUEST_GALLERY_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults,this);
        switch (requestCode) {
            case REQUEST_GALLERY_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                    openGalleryIntent.setType("image/*");
                    startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
                }
            case READ_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    OpenCamera();
                }
        }


    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void maintainance() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(getContext());

        adb.setTitle("Send Maintainance Request?");
        //adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String str_produtcdescription = txtprodutcdescription.getText().toString();
                String str_produtcname = txtprodutcname.getText().toString();
//                callmaintainance(str_produtcname, str_produtcdescription);

                if (checkValues(str_produtcname, str_produtcdescription)) {

                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage("Uploading...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            if (callmaintainanceService != null) {
                                callmaintainanceService.cancel();
                                Toast.makeText(getContext(), "progressDialog canceled", Toast.LENGTH_SHORT).show();

                            }
                        }
                    });

                    callmaintainance(str_produtcname, str_produtcdescription, image_Bath);
                }
            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();

    }

    private boolean checkValues(String str_produtcname, String str_produtcdescription) {
        if (!Validation.validate_product_name(str_produtcname)) {
            txtprodutcname.setError("Invalid product name ");
            return false;
        }
        if (!Validation.validate_description(str_produtcdescription)) {
            txtprodutcdescription.setError("too big problem Discription ");
            return false;
        }
        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (callmaintainanceService != null && callmaintainanceService.isExecuted()) {
            callmaintainanceService.cancel();
        }
    }

    private void callmaintainance(String str_produtcname, String str_produtcdescription, String image_Bath) {

        callmaintainanceService = callmaintainanceService(str_produtcname, str_produtcdescription, this.image_Bath);
        if (callmaintainanceService != null) {
            callmaintainanceService.request().url().toString();
            progressDialog.show();
            callmaintainanceService.enqueue(new Callback<SimpleResponse>() {
                @Override
                public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() == 200) {
                            Toast.makeText(getContext(), getResources().getString(R.string.maintainanceRequestSuccess), Toast.LENGTH_SHORT).show();

                        } else if (response.body().getStatus() == 402) {
                            //something is required
                            Toast.makeText(getContext(), "something is required for Product details", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        int statusCode = response.code();
                        // handle request errors depending on status code
                        Toast.makeText(getContext(), "Threr is error in server side", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SimpleResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "connection fail", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Log.d("cccccccccccc", t.getMessage().toString());
                }
            });
        } else {
            Toast.makeText(getContext(), "You must login to send request", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();
        }
    }

    public Call<SimpleResponse> callmaintainanceService(String str_produtcname, String str_produtcdescription, String image_Bath) {

        Toast.makeText(getContext(), "path" + this.image_Bath, Toast.LENGTH_SHORT).show();
        if (apiToken != null) {
//pass it like this
            RequestBody apitoken;
            RequestBody problem;
            RequestBody product_name;
            File file = new File(this.image_Bath);
            Bitmap bmp = BitmapFactory.decodeFile(this.image_Bath);
            product_name =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), str_produtcname);
            // add another part within the multipart request
            problem =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), str_produtcdescription);
            // add another part within the multipart request

            apitoken =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"), apiToken);
            if (!this.image_Bath.equals("") && bmp != null) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 70, bos);

//        RequestBody requestFile =
//                RequestBody.create(MediaType.parse("multipart/form-data"), file);
                ProgressRequestBody requestFile = new ProgressRequestBody(file, this);

//            RequestBody requestFile =
//                    RequestBody.create(
//                            MediaType.parse("image/*"),
//                            bos.toByteArray()
//                    );
// MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part imagebody =
                        MultipartBody.Part.createFormData("image", file.getName(), requestFile);
// add another part within the multipart request


                return apiServiceInterface.maintainance(apitoken, product_name, problem, imagebody);

            } else {
                return apiServiceInterface.maintainanced(apitoken, product_name, problem);
            }
        } else {
            return null;
        }


    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onProgressUpdate(int percentage) {
        // set current progress

        progressDialog.setProgress(percentage);
    }

    @Override
    public void onError() {
        // do something on error
    }

    @Override
    public void onFinish() {
        // do something on upload finished
        // for example start next uploading at queue

        progressDialog.setProgress(100);
    }
}
