package com.abouelgoukh.sporteye.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class GetJSONObject {
//	public static String urlvalue="HTTP://192.168.43.87:80/products.json";
	public static String urlvalue="HTTP://192.168.1.117:80/products.json";
//	public static String urlvalue="HTTP://192.168.1.23:80/products.json";

	public static JSONObject getJSONObject(String url) throws IOException,
			JSONException {
		JSONParser jsonParser = new JSONParser();
		// Use HttpURLConnection
		JSONObject jsonObject = jsonParser.getJSONHttpURLConnection(url);

		return jsonObject;
	}
}
