package com.abouelgoukh.sporteye.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.utils.TagName;

public class JsonReader {

	public static List<Product> getHome(JSONObject jsonObject) throws JSONException {
		List<Product> products = new ArrayList<>();

		JSONArray jsonArray = jsonObject.getJSONArray(TagName.TAG_PRODUCTS);
		Product product;
		for (int i = 0; i < jsonArray.length(); i++) {
			product = new Product();
			JSONObject productObj = jsonArray.getJSONObject(i);
			product.setId(productObj.getInt(TagName.KEY_ID));
			product.setName(productObj.getString(TagName.KEY_NAME));
			product.setImage(productObj.getString(TagName.KEY_IMAGE_URL));
			product.setMount(1);
			products.add(product);
		}
		return products;
	}
}
