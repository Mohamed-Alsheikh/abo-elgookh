package com.abouelgoukh.sporteye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.abouelgoukh.sporteye.Activities.LoginActivity;
import com.abouelgoukh.sporteye.Deleget;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Address;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Government;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;

/**
 * Created by Mohamed.Alsheikh on 6/21/2017.
 */

public class AddAddress extends Dialog implements View.OnClickListener, Deleget {


    public Activity context;
    public Button closeDialog, saveAddress;
    Spinner government;
    ImageView spinnericon;
    ImageView refresh;
    EditText address;
    String governmentTxt, addressTxt;
    int governmentId;
    ApiServiceInterface apiServiceInterface;
    ProgressBar progressbar;
    ProgressDialog progressDialog;
    List<Government> governList = new ArrayList<>();
    List<String> governListstr = new ArrayList<>();
    String target;
    public Deleget deleget;

    public AddAddress(Activity checkOutActivity, int dialogTheme) {
        super(checkOutActivity, dialogTheme);
        context = checkOutActivity;
    }

    public AddAddress(Activity a, String target) {
        super(a);
        // TODO Auto-generated constructor stub
        this.context = a;
        this.target = target;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_address_dialog);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressbar =  findViewById(R.id.progressDialog);
        saveAddress =  findViewById(R.id.save_address);
        refresh =  findViewById(R.id.refresh);
        closeDialog =  findViewById(R.id.close_dialog);
        government =  findViewById(R.id.addgovernspinner);
        spinnericon =  findViewById(R.id.AddAddressSpinnericon2);
        spinnericon.setOnClickListener(this);
        address =  findViewById(R.id.addaddresstxt);
        apiServiceInterface = ApiUtils.getRetrofitObject();
        callGovernment();
        saveAddress.setOnClickListener(this);
        closeDialog.setOnClickListener(this);
        refresh.setOnClickListener(this);


    }

    public void showDropDown() {
        government.performClick();
    }

    private void callGovernment() {
        progressDialog.show();
        Call<Address> getAdresses = callGovernmentService();

        getAdresses.enqueue(new Callback<Address>() {
            @Override
            public void onResponse(Call<Address> call, Response<Address> response) {

                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        Toast.makeText(getContext(), "the Government is rendered", Toast.LENGTH_SHORT).show();
                        governList = response.body().getGovernments();
                        prepareGovern(governList);
                    } else if (response.body().getStatus() == 403) {

                        Toast.makeText(getContext(), "there is no Government", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "failed to load Government try again", Toast.LENGTH_SHORT).show();

                }
                progressbar.setVisibility(View.GONE);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Address> call, Throwable t) {
                String message = t.getMessage();
                Toast.makeText(getContext(), "there is an error \n check internet connection", Toast.LENGTH_SHORT).show();
                progressbar.setVisibility(View.GONE);
                progressDialog.dismiss();
                progressbar.isEnabled();

            }
        });
    }

    private void prepareGovern(List<Government> governList) {
        governListstr.clear();
        for (int x = 0; x < governList.size(); x++) {
            governListstr.add(governList.get(x).getName());
        }
        setRequiredValues(governListstr);
    }

    private void setRequiredValues(List<String> spinnerList) {
        ArrayAdapter<String> Adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, spinnerList);

        // Drop down layout style - list view with radio button
        Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        government.setAdapter(Adapter);

        // Spinner click listener
//        government.setOnItemSelectedListener(this);
    }

    private Call<Address> callGovernmentService() {

        SharedPreferences session = context.getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(context);
        return apiServiceInterface.getAllGovernments(apitoken, lang);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_address:
                if (government.getSelectedItem() != null) {
                    governmentTxt = government.getSelectedItem().toString();
                    if (!TextUtils.isEmpty(governmentTxt)) {
                        progressDialog.show();
                        addressTxt = address.getText().toString();
                        int governmentposition = government.getSelectedItemPosition();
                        governList.get(governmentposition).getId();
                        String goven_name = governList.get(governmentposition).getName();
                        callSaveAddressService(addressTxt, governList.get(governmentposition), goven_name);
                    }
                } else {
                    Toast.makeText(getContext(), "there is no Government\n please refresh the government", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.close_dialog:
                dismiss();
                break;
            case R.id.AddAddressSpinnericon2:
                showDropDown();
                break;
            case R.id.refresh:
                callGovernment();
                break;
            default:
                break;
        }

    }

    private void callSaveAddressService(final String addressTxt, final Government government, final String goven_name) {
        Call<SimpleResponse> getAdresses = addAddress(addressTxt, government.getId());

        getAdresses.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
//                        Toast.makeText(getContext(), "the address is added  " + response.body().getId(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        dismiss();

                        onSaveAddress(response.body().getId(), addressTxt, government, goven_name);

                    } else if (response.body().getStatus() == 403) {
                        Toast.makeText(getContext(), "there is no Government", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getStatus() == 402) {
                        Toast.makeText(getContext(), "there is some thing is requird", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getStatus() == 406) {
                        Toast.makeText(getContext(), "You Are not Active User", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), "failed try again", Toast.LENGTH_SHORT).show();

                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                String message = t.getMessage();
                Log.d("failure", message);
                progressDialog.dismiss();
            }
        });
    }

    private Call<SimpleResponse> addAddress(String addressTxt, int governmentId) {
        Controller controller = (Controller) context.getApplicationContext();
        String apiToken = controller.getprefrancesUserApiToken();
        return apiServiceInterface.addAddress(apiToken, addressTxt, governmentId);

    }

    @Override
    public void onSaveAddress(Integer Address_id, String addressTxt, Government government, String goven_name) {
        deleget.onSaveAddress(Address_id, addressTxt, government, goven_name);
    }

    @Override
    public void onDeleteCartitem(Double deleted_item_price, String operation, int amount) {

    }
}
