package com.abouelgoukh.sporteye.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import com.abouelgoukh.sporteye.R;

/**
 * Created by User on 8/1/2017.
 */

public class ImageDialog extends Dialog implements View.OnClickListener {

    /**
     * Created by Mohamed.Alsheikh on 6/21/2017.
     */

    public Activity context;
    public Button closeDialog, saveAddress;

    ImageView imageview;

    String imagePath;

    public ImageDialog(Activity checkOutActivity, int dialogTheme,String imagePath) {
        super(checkOutActivity, dialogTheme);
        context = checkOutActivity;
    }

    public ImageDialog(Activity a, String target,String imagePath) {
        super(a);
        this.imagePath=imagePath;
        // TODO Auto-generated constructor stub
        this.context = a;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.imagedialog);

        imageview = (ImageView) findViewById(R.id.imageview);

        Glide.with(context).load(imagePath).into(imageview);



    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.fabimage:
//
//
//                break;
            default:
                break;
        }
        dismiss();
    }

}
