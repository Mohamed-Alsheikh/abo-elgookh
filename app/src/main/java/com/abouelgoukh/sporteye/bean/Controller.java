package com.abouelgoukh.sporteye.bean;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import androidx.multidex.MultiDex;

import java.util.ArrayList;
import java.util.Locale;

import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.model.ModelCart;
import com.abouelgoukh.sporteye.model.ModelCompare;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;

/**
 * Created by User on 6/5/2017.
 */

public class Controller extends Application {


    public ModelCart myCart = new ModelCart();
    public ArrayList<Integer> cartOfProductsID = new ArrayList<>();
    public ArrayList<Integer> cartOfProductsTypes = new ArrayList<>();




    public ArrayList<Integer> getCartOfProductsID() {
        return cartOfProductsID;
    }

    public String getprefrancesUserApiToken() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        return prefs.getString("APITOKEN", null);
    }

    public String getprefrancesUser_email() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Session", Context.MODE_PRIVATE);
        return prefs.getString("EMAIL", null);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    public void setCartOfProducts(int product, int type) {
        cartOfProductsID.add(product);
        cartOfProductsTypes.add(type);
    }

    public boolean CheckProductInCart(int product, int type) {
        for (int i = 0; i < cartOfProductsID.size(); i++) {
            if (cartOfProductsID.get(i) == product && cartOfProductsTypes.get(i) == type) {
                return true;
            }
        }
        return false;
    }

    public int deleteCartProducts(int product, int type, Product album) {
        int indexof_deleted_element = -1;
        for (int i = 0; i < cartOfProductsID.size(); i++) {
            if (cartOfProductsID.get(i) == product && cartOfProductsTypes.get(i) == type) {
                getCart().getCartItems().remove(i);
                cartOfProductsID.remove(i);
                cartOfProductsTypes.remove(i);
                indexof_deleted_element = i;
                break;
            }
        }
        return indexof_deleted_element;
    }

    public int getcartProductsArraylistsize() {
        return cartOfProductsID.size();
    }

    public ModelCart getCart() {
        return myCart;
    }


    /****
     * Set Favorite Products
     * ****/
    private ModelCart myFavorite = new ModelCart();
    private ArrayList<Integer> my_FavoriteProducts = new ArrayList<>();
    private ArrayList<Integer> my_FavoriteProductstype = new ArrayList<>();

    public void clearListData() {
        my_FavoriteProducts.clear();
        my_FavoriteProductstype.clear();
        myFavorite.getFavoritsItems().clear();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public int getFavoriteProducts(int pPosition) {
        return my_FavoriteProducts.get(pPosition);
    }

    public void setFavoriteProducts(int products, int type) {
        my_FavoriteProducts.add(products);
        my_FavoriteProductstype.add(type);
    }

    public boolean CheckProductInFavorites(int product, int type) {
        for (int i = 0; i < my_FavoriteProducts.size(); i++) {
            if (my_FavoriteProducts.get(i) == product && my_FavoriteProductstype.get(i) == type) {
                return true;
            }
        }
        return false;

    }

    public void deletefavoriteProducts(int product, int type) {

        for (int i = 0; i < cartOfProductsID.size(); i++) {
            if (my_FavoriteProducts.get(i) == product && my_FavoriteProductstype.get(i) == type) {
                my_FavoriteProducts.remove(my_FavoriteProducts.indexOf(product));
                my_FavoriteProductstype.remove(my_FavoriteProductstype.indexOf(type));
            }
        }
    }

    public int getFavoriteProductsArraylistsize() {
        return my_FavoriteProducts.size();
    }

    public ModelCart getFavorite() {
        return myFavorite;
    }

    /****
     *
     * ***/

    public ModelCompare  modelCompare = new ModelCompare();
    public ArrayList<Integer> compareOfProductsID = new ArrayList<>();
    public ArrayList<Integer> compareOfProductsTypes = new ArrayList<>();

    public void setCompareOfProducts(int product, int type) {
        compareOfProductsID.add(product);
        compareOfProductsTypes.add(type);
    }

    public boolean CheckProductInCompare(int product, int type) {
        for (int i = 0; i < compareOfProductsID.size(); i++) {
            if (compareOfProductsID.get(i) == product && compareOfProductsTypes.get(i) == type) {
                return true;
            }
        }
        return false;
    }

    public int deleteCompareProducts(int product, int type, Product album) {
        int indexof_deleted_element = -1;
        for (int i = 0; i < compareOfProductsID.size(); i++) {
            if (compareOfProductsID.get(i) == product && compareOfProductsTypes.get(i) == type) {
                getCompare().getCompareItems().remove(i);
                compareOfProductsID.remove(i);
                compareOfProductsTypes.remove(i);
                indexof_deleted_element = i;
                break;
            }
        }
        return indexof_deleted_element;
    }

    public int getcompareProductsArraylistsize() {
        return compareOfProductsID.size();
    }

    public ModelCompare getCompare() {
        return modelCompare;
    }


}