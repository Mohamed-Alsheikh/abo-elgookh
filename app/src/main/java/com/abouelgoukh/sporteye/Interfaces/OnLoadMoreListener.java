package com.abouelgoukh.sporteye.Interfaces;

/**
 * Created by Mohamed.Alsheikh on 6/11/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
