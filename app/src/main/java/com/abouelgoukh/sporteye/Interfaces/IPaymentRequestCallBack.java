package com.abouelgoukh.sporteye.Interfaces;

import com.abouelgoukh.sporteye.helper.PayFortData;

/**
 * Created by User on 8/15/2017.
 */

public interface IPaymentRequestCallBack {
    void onPaymentRequestResponse(int responseType, PayFortData responseData);
}