

package com.abouelgoukh.sporteye.utils;

public class Validation {
 public static boolean validate_number(String string) {
        if (string.matches("\\d{1,100}")) {
            return true;
        } else {
            return false;
        }
    }   
 public static boolean validate_small_number(String string) {
        if (string.matches("\\d{1,3}")) {
            return true;
        } else {
            return false;
        }
    }   
 
     ////////////////validate name/////////////////
    public static boolean validate_product_name(String string) {
     return string.matches("([a-zA-Z0-9أ-ي]+\\s*?){2,4}");
    }
    public static boolean validate_name(String string) {
     return string.matches("([a-zA-Zأ-ي]+\\s*?){1,4}");
    }
    public static boolean validate_description(String string) {
     return string.matches("([a-zA-Zأ-ي]+\\s*?){1,30}");
    }
    
    public static boolean validate_Email(String string) {
        if (string.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")) {
            return true;
        } else {
            return false;
        }

    }

       public static boolean validate_mobile(String string) {
        if (string.matches("^(0106|0100|0109|0101|0102|0122|0127|0128|0120|0110|0111|0114|0115|0112)[0-9]{7}")) {
            return true;
        } else {
            return false;
        }
       }
       public static boolean validate_Password(String string) {
//        if (string.matches("^[a-zA-Z0-9]{9,20}")) {
        if (string.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$")) {
            return true;
        } else {
            return false;
        }
    }
       
           public boolean validate_date(String string) {
        if (string.matches("^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$")) {
            return true;
        } else {
            return false;
        }
    }

}
