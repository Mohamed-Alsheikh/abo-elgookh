package com.abouelgoukh.sporteye.utils;

/**
 * Created by M. AlSheikh on 3/19/2018.
 */

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.util.ArrayList;

import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.ProductBrandDelegate;
import com.abouelgoukh.sporteye.R;


/**
 * Created by M. AlSheikh on 3/13/2018.
 */

public class ViewDialog implements AdapterView.OnItemSelectedListener, ProductBrandDelegate {
    public MainActivity mainActivity;
    RangeSeekBar seekbar_placeholder_size;
    RangeSeekBar seekbar_placeholder_price;
    Spinner spinner;
    Dialog dialog;
    ImageView spinnericon;
    Activity activity;
    ArrayAdapter<String> brandAdapter;
    int position;
    int all_Content;
    String brand_type;
    ArrayList<String> brands_names;
    ArrayList<String> brands_images;
    ArrayList<String> brands_Item_Count;
    int price_from = 0;
    int price_to = 0;
    int size_from = 0;
    int size_to = 0;
    String brand;
    public ProductBrandDelegate productBrandDelegate;

    public void showDialog(Activity activity, ArrayList<String> brands_names, ArrayList<String> brands_images, ArrayList<String> brands_Item_Count, int position, int all_Content, String brand_type) {
        if (dialog == null) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(R.layout.custom_dialogbox_otp);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(dialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setAttributes(lp);
            this.activity = activity;
            this.position = position;
            this.all_Content = all_Content;
            this.brand_type = brand_type;
            this.brands_names = brands_names;
            this.brands_images = brands_images;
            this.brands_Item_Count = brands_Item_Count;


            spinner = (Spinner) dialog.findViewById(R.id.spinner);
            spinnericon = (ImageView) dialog.findViewById(R.id.spinnericon);


            spinnericon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    spinner.performClick();
                }
            });

            SpinnerBrandsinit();

            // Seek bar for which we will set text color in code
            seekbar_placeholder_price = (RangeSeekBar) dialog.findViewById(R.id.seekbar_placeholder_price);
            seekbar_placeholder_price.setTextAboveThumbsColorResource(R.color.colorAccent);

            seekbar_placeholder_size = (RangeSeekBar) dialog.findViewById(R.id.seekbar_placeholder_size);
            seekbar_placeholder_size.setTextAboveThumbsColorResource(R.color.colorAccent);
        }
        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
//
        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                onApplyClicked();
                dialog.dismiss();
            }
        });
        price_from = Integer.parseInt(""+ seekbar_placeholder_price.getSelectedMinValue());
        price_to = Integer.parseInt(""+ seekbar_placeholder_price.getSelectedMaxValue());

        size_from = Integer.parseInt(""+seekbar_placeholder_size.getSelectedMinValue());
        size_to =Integer.parseInt(""+ seekbar_placeholder_size.getSelectedMaxValue());
        brand = spinner.getSelectedItem()+"";
        dialog.show();
    }

    boolean parameterValuesChanged() {
        if (price_from == Integer.parseInt(""+ seekbar_placeholder_price.getSelectedMinValue())
                && price_to == Integer.parseInt(""+ seekbar_placeholder_price.getSelectedMaxValue())
                && size_from == Integer.parseInt(""+ seekbar_placeholder_size.getSelectedMinValue())
                && size_to == Integer.parseInt(""+ seekbar_placeholder_size.getSelectedMaxValue())
                && brand.equals(spinner.getSelectedItem().toString())) {
            return false;
        } else
            return true;
    }

    private void SpinnerBrandsinit() {
        if (activity != null) {

            brandAdapter= new ArrayAdapter<String>(activity, R.layout.spinner_item, brands_names) {

                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent)
                {
                    View v = null;
                    v = super.getDropDownView(position, null, parent);
                    // If this is the selected item position
                    if (position == spinner.getSelectedItemPosition()) {
                        v.setBackgroundColor(Color.GRAY);
                    }
                    else {
                        // for other views
                        v.setBackgroundColor(Color.WHITE);

                    }
                    return v;
                }
            };
            brandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(brandAdapter);
        }
        if (position >= 0) {
            spinner.setSelection(position);
        } else {
            position = brands_names.size() - 1;
            spinner.setSelection(position);
        }


        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onApplyClicked() {


        if (MainActivity.controller.isNetworkAvailable()) {
            if (parameterValuesChanged()) {
                String price_from;
                String price_to;
                String size_from;
                String size_to;
                if (Integer.parseInt(""+ seekbar_placeholder_price.getSelectedMinValue()) == 0) {
                    price_from = null;
                } else {
                    price_from = seekbar_placeholder_price.getSelectedMinValue() + "";
                }

                if (Integer.parseInt(""+ seekbar_placeholder_price.getSelectedMaxValue()) == 100000) {
                    price_to = null;
                } else {
                    price_to = seekbar_placeholder_price.getSelectedMaxValue()+ "";
                }

                if (Integer.parseInt(""+ seekbar_placeholder_size.getSelectedMinValue()) == 6) {
                    size_from = null;
                } else {
                    size_from = seekbar_placeholder_size.getSelectedMinValue() + "";
                }

                if (Integer.parseInt(""+ seekbar_placeholder_size.getSelectedMaxValue()) == 36) {
                    size_to = null;
                } else {
                    size_to = seekbar_placeholder_size.getSelectedMaxValue() + "";
                }

                onChangBrand(
                        spinner.getSelectedItemPosition()
                        , price_from
                        , price_to
                        , size_from
                        , size_to);
            }
        } else {

        }
    }

    @Override
    public void onChangBrand(int addressTxt, String price_from, String price_to, String size_from, String size_to) {
        productBrandDelegate.onChangBrand(addressTxt, price_from, price_to, size_from, size_to);
    }
}