package com.abouelgoukh.sporteye.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Activities.ProductDetailsActivity;
import com.abouelgoukh.sporteye.Activities.TransitionHelper;
import com.abouelgoukh.sporteye.Fragments.BrandProductsFragment;
import com.abouelgoukh.sporteye.Fragments.SharedProductsFragment;
import com.abouelgoukh.sporteye.Interfaces.OnLoadMoreListener;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mohamed.Alsheikh on 5/23/2017.
 */
public class SharedProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Product> albumList;
    Controller controller;

    private boolean isLoadingAdded = false;

    private OnLoadMoreListener mOnLoadMoreListener;

    String Brand_type;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    RecyclerView recyclerView;
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private static final int FOOTER = 2;
    String target;
    Runnable onFavoritsClicked;
    SharedPreferences session;
    private Gson gson;
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();


    Activity activity;

    public SharedProductsAdapter(Context mContext, List<Product> albumList, RecyclerView recyclerView, String target, SharedPreferences session, boolean update, Activity activity) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.target = target;
        this.session = session;
        this.recyclerView = recyclerView;
        this.activity = activity;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
        for (Product result : albumList) {
            if (session != null && update) {
                saveGeofence(result);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        controller = (Controller) mContext.getApplicationContext();
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case ITEM:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_album_card, parent, false);
                viewHolder = new MyViewHolder(view);
                break;
            case LOADING:
//                Toast.makeText(mContext, "LOADING notify", Toast.LENGTH_SHORT).show();
                View v2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
            case FOOTER:
//                Toast.makeText(mContext, "LOADING notify", Toast.LENGTH_SHORT).show();
                View v3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.all_brand_cart, parent, false);
                viewHolder = new FooterViewHolder(v3);
                break;
        }
        return viewHolder;

    }

    private void saveGeofence(Product product) {

        gson = new Gson();
        String json = gson.toJson(product);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(product.getId().toString() + product.getType().toString() + "", json);
        editor.apply();
    }

    private void removeSavedGeofences(Product product) {

        SharedPreferences.Editor editor = session.edit();
        editor.remove(product.getId().toString() + product.getType().toString() + "");
        editor.apply();
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder1, final int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                final MyViewHolder holder = (MyViewHolder) holder1;
                final Product album = albumList.get(position);
                holder.product_name.setText(album.getName());
                holder.price_before.setVisibility(View.VISIBLE);
                holder.price_before_view.setVisibility(View.VISIBLE);
                holder.discount.setVisibility(View.VISIBLE);
                holder.price_before.setText(mContext.getResources().getString(R.string.price) + " " + album.getPrice() + " " + mContext.getResources().getString(R.string.unit));
                if (album.getPrice_after_discount() != null && album.getPrice_after_discount() > 0) {

                    double discount_value = ((album.getPrice() - album.getPrice_after_discount()) / album.getPrice()) * 100;
                    double discount_percent = Math.floor(discount_value + 0.5d);
                    holder.discount.setText(discount_percent + " %");
                    holder.Price_after.setText(mContext.getResources().getString(R.string.price) + " " + album.getPrice_after_discount() + " " + mContext.getResources().getString(R.string.unit));
                } else {
                    holder.price_before.setVisibility(View.INVISIBLE);
                    holder.price_before_view.setVisibility(View.INVISIBLE);
                    holder.discount.setVisibility(View.INVISIBLE);
                    holder.Price_after.setText(mContext.getResources().getString(R.string.price) + " " + album.getPrice() + " " + mContext.getResources().getString(R.string.unit));
                }
                if (album.getIsFavorated() == 1) {
                    album.setFavoritImageChanged(true);
                }


                if (controller.CheckProductInCart(album.getId(), album.getType())) {
                    holder.cart.setBackground(mContext.getResources().getDrawable(R.drawable.btn_cart_clicked_bg));
//                    holder.cart.setBackground(AppCompatDrawableManager.get().getDrawable(mContext, R.drawable.btn_cart_clicked_bg));
                    holder.cart.setText(mContext.getResources().getString(R.string.removeCart));
                    album.setCartImageChanged(true);
                } else {
//                    holder.cart.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
                    holder.cart.setBackground(mContext.getResources().getDrawable(R.drawable.btn_cart_bg));
                    holder.cart.setText(mContext.getResources().getString(R.string.AddToCart));

                }

                if (controller.CheckProductInCompare(album.getId(), album.getType())) {
                    holder.compare.setImageResource(R.drawable.ic_compare_red_24dp);
                    album.setCartImageChanged(true);
                } else {
                    holder.compare.setImageResource(R.drawable.ic_compare_black_24dp);

                }

//
//                if (controller.checkCompareInProducts(album.getId())) {
//                    holder.compare.setImageResource(R.drawable.ic_compare);
//                }


                if (album.isfavoritImageChanged()) {
                    holder.favorites.setImageResource(R.drawable.ic_favorite);
                } else {
                    holder.favorites.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                }


                // loading album cover using Glide library

//                Glide.with(mContext).load(album.getImage()).into(holder.thumbnail);
                Glide.with(mContext).
                        load(album.getImage())
                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))

                        .apply(RequestOptions.placeholderOf(R.drawable.brands))
                        .into(holder.thumbnail);

//                Glide.with(mContext)
//                        .load(album.getImage())
//                        .listener(new RequestListener<Drawable>() {
//                            @Override
//                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                                holder.progressBar.setVisibility(View.GONE);
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                                holder.progressBar.setVisibility(View.GONE);
//                                return false;
//                            }
//                        })
//                        .into(holder.thumbnail);

                holder.cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        session = mContext.getSharedPreferences("Cart", MODE_PRIVATE);
                        if (!controller.CheckProductInCart(album.getId(), album.getType())) {
                            int indexof_added_element = controller.getcartProductsArraylistsize();

                            controller.setCartOfProducts(album.getId(), album.getType());
                            (controller.getCart()).setProducts(album);
                            album.setCartImageChanged(true);
//                            holder.cart.setImageResource(R.drawable.ic_shopping_cart2);
                            holder.cart.setBackground(mContext.getResources().getDrawable(R.drawable.btn_cart_clicked_bg));
                            holder.cart.setText(mContext.getResources().getString(R.string.removeCart));
//                            Toast.makeText(mContext, "Cart list size =" + controller.getcartProductsArraylistsize(), Toast.LENGTH_SHORT).show();

                            saveGeofence(album);
                        } else {
                            album.setCartImageChanged(false);
                            int indexof_deleted_element = controller.deleteCartProducts(album.getId(), album.getType(), album);
//                            holder.cart.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
                            holder.cart.setBackground(mContext.getResources().getDrawable(R.drawable.btn_cart_bg));
                            holder.cart.setText(mContext.getResources().getString(R.string.AddToCart));
//                            Toast.makeText(mContext, "Cart list size =" + controller.getcartProductsArraylistsize(), Toast.LENGTH_SHORT).show();
                            removeSavedGeofences(album);
                        }
                        MainActivity.setupMessagesBadge(controller.getcartProductsArraylistsize());

                    }
                });

                holder.compare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        session = mContext.getSharedPreferences("COMPARE", MODE_PRIVATE);
                        if (!controller.CheckProductInCompare(album.getId(), album.getType())) {
                            int indexof_added_element = controller.getcompareProductsArraylistsize();

                            controller.setCompareOfProducts(album.getId(), album.getType());
                            (controller.getCompare()).setProducts(album);
                            album.setCompareImageChanged(true);
                            holder.compare.setImageResource(R.drawable.ic_compare_red_24dp);

                            saveGeofence(album);
                        } else {
                            album.setCompareImageChanged(false);
                            int indexof_deleted_element = controller.deleteCompareProducts(album.getId(), album.getType(), album);
                            holder.compare.setImageResource(R.drawable.ic_compare_black_24dp);
                            removeSavedGeofences(album);
                        }
                        MainActivity.setupMessagesBadgeCompare(controller.getcompareProductsArraylistsize());

                    }
                });
//                holder.compare.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        if (!controller.checkCompareInProducts(album.getId())) {
//                            controller.setCompareProducts(album.getId());
//                            (controller.getCompareList()).setProducts(album);
//                            holder.compare.setImageResource(R.drawable.ic_compare);
////                            Toast.makeText(mContext, "CompareList list size =" + controller.getcartProductsArraylistsize(), Toast.LENGTH_SHORT).show();
//
//                        } else {
//                            (controller.getCompareList()).deleteProducts(album);
//                            controller.deleteCompareProducts(album.getId());
//                            holder.compare.setImageResource(R.drawable.ic_compare_black_24dp);
////                            Toast.makeText(mContext, "Compare list size =" + controller.getCompareList().getcomparelistsize(), Toast.LENGTH_SHORT).show();
//
//                        }
//                    }
//                });


                holder.favorites.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (controller.isNetworkAvailable()) {
                            if (album.isfavoritImageChanged()) {
                                holder.favorites.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                                album.setFavoritImageChanged(false);
                            } else {
                                holder.favorites.setImageResource(R.drawable.ic_favorite);
                                album.setFavoritImageChanged(true);
                            }
                            holder.favoritshandler.removeCallbacks(onFavoritsClicked);
                            onFavoritsClicked = new Runnable() {
                                @Override
                                public void run() {
                                    if (album.isfavoritImageChanged()) {
                                        setfavorits(album.getType(), album.getId(), 1, holder, album, position);
                                    } else {
                                        setfavorits(album.getType(), album.getId(), 0, holder, album, position);
                                    }
                                }
                            };
                            holder.favoritshandler.postDelayed(onFavoritsClicked, 1000);
                        } else {
                            SharedProductsFragment.snackbar.show();
                            MainActivity.appBarLayout.setExpanded(false);
                        }

                    }
                });
                holder.thumbnail.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(mContext, "card num = " + holder.Price.getText().toString(), Toast.LENGTH_SHORT).show();
//                        switchContent(position, album);
//                        Intent intent =new Intent(mContext, ProductDetailsActivity.class);
//                        Bundle bundlanimation =
//                        ActivityOptions.makeCustomAnimation(mContext,
//                                R.anim.login_animation, R.anim.login_animation2).toBundle();
//                        mContext.startActivity(intent, bundlanimation);

                        transitionToActivity(ProductDetailsActivity.class, holder, album);
//
//
//                        Intent intent = new Intent(activity, ProductDetailsActivity.class);
//// Pass data object in the bundle and populate details activity.
////                        intent.putExtra("Product", album);
//                        Bundle arguments = new Bundle();
//                        arguments.putParcelable("Product", album);
//                        intent.putExtras(arguments);
//                        ActivityOptionsCompat options = ActivityOptionsCompat.
//                                makeSceneTransitionAnimation(activity, (View) holder.product_name, "profile");
//                        activity.startActivity(intent, options.toBundle());
                    }
                });
            case LOADING:
//                Do nothing
                break;
            case FOOTER:
//
                final FooterViewHolder holder2 = (FooterViewHolder) holder1;
                holder2.layout_showAllbicycles.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mContext, "LOADING layout_showAllbicycles", Toast.LENGTH_SHORT).show();
                        Brand_type = "Bicycles Brands";
                        showall(0, "Bicycles Brands");
                    }
                });
                holder2.layout_showAllaccessories.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Brand_type = "Accessories Brands";
                        showall(0, "Accessories Brands");
                    }
                });
                break;
        }
    }

    private void transitionToActivity(Class target, MyViewHolder viewHolder, Product sample) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(activity, false,
                new Pair<ImageView, String>(viewHolder.thumbnail, "product_image")
                , new Pair<TextView, String>(viewHolder.product_name, "product_name"));

        startActivity(target, pairs, sample);
    }

    private void startActivity(Class target, Pair<View, String>[] pairs, Product sample) {
        Intent i = new Intent(activity, target);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, pairs);
        i.putExtra("Product", sample);
        activity.startActivity(i, transitionActivityOptions.toBundle());
    }

    public void showall(int brandid, String brand_name) {
        if (mContext == null)
            return;
        if (mContext instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) mContext;
            Fragment fragment = new BrandProductsFragment();
            mainActivity.switchContent(true, null, brandid, brand_name, Brand_type, fragment);

        }
    }

    public void onRunFavoritsClicked(Product album, final MyViewHolder holder, int status) {


    }

    public void switchContent(int position, Product album) {
        if (mContext == null)
            return;
        if (mContext instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) mContext;
            mainActivity.switchContent(position, album);
        }
    }

    private void setfavorits(int type, int prduct_id, int status, final MyViewHolder holder, final Product album, final int position) {
        session = mContext.getSharedPreferences("Favorits", MODE_PRIVATE);
        if (callfavoriteService(type, prduct_id, status, album, holder) != null) {
            callfavoriteService(type, prduct_id, status, album, holder).enqueue(new Callback<SimpleResponse>() {
                @Override
                public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                    if (response.isSuccessful()) {

                        if (response.body().getStatus() == 200) {
                            album.setFavoritImageChanged(true);
                            holder.favorites.setImageResource(R.drawable.ic_favorite);
                            saveGeofence(album);
                            Toast.makeText(mContext, "Added To Favorite List", Toast.LENGTH_SHORT).show();

                        } else if (response.body().getStatus() == 300) {
                            album.setFavoritImageChanged(false);
                            holder.favorites.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            removeSavedGeofences(album);
                            if (target.equals("Favorite")) {
                                removeAt(position, holder.getAdapterPosition(), album);
                            }

                            Toast.makeText(mContext, "Removed From Favorite List", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "there is an error", Toast.LENGTH_SHORT).show();
                            if (album.isfavoritImageChanged()) {
                                album.setFavoritImageChanged(false);
                                holder.favorites.setImageResource(R.drawable.ic_favorite);
                            } else {
                                album.setFavoritImageChanged(true);
                                holder.favorites.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            }

                        }

                    } else {
                        int statusCode = response.code();
                        if (album.isfavoritImageChanged()) {
                            album.setFavoritImageChanged(false);
                            holder.favorites.setImageResource(R.drawable.ic_favorite);
                        } else {
                            album.setFavoritImageChanged(true);
                            holder.favorites.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        }
                        Toast.makeText(mContext, "there is error in connection", Toast.LENGTH_SHORT).show();
                        // handle request errors depending on status code
                    }


                }

                @Override
                public void onFailure(Call<SimpleResponse> call, Throwable t) {
                    if (album.isfavoritImageChanged()) {
                        album.setFavoritImageChanged(false);
                        holder.favorites.setImageResource(R.drawable.ic_favorite);
                    } else {
                        album.setFavoritImageChanged(true);
                        holder.favorites.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    }
                    Toast.makeText(mContext, "there is error in connection", Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    private Call<SimpleResponse> callfavoriteService(int type, int productid, int status, Product album, MyViewHolder holder) {
        if (controller.getprefrancesUserApiToken() != null) {
            switch (type) {
                case 1:
                    return mService.getfavoratePruduct(productid, controller.getprefrancesUserApiToken(), status);
                case 2:
                    return mService.favorateaccessory(productid, controller.getprefrancesUserApiToken(), status);
                case 3:
                    return mService.favorateOtherProduct(productid, controller.getprefrancesUserApiToken(), status);
            }
        } else {
            Toast.makeText(mContext, "please login to add favorite Products", Toast.LENGTH_SHORT).show();
            if (!album.isfavoritImageChanged()) {
                album.setFavoritImageChanged(false);
                holder.favorites.setImageResource(R.drawable.ic_favorite);
            } else {
                album.setFavoritImageChanged(true);
                holder.favorites.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            }

        }
        return null;
    }

    /*
     * Showing popup menu when tapping on 3 dots
     */
    public void removeAt(int position, int adapterPosition, Product product) {

        for (int i = 0; i < controller.getCart().getCartItems().size(); i++) {
            if (controller.getCart().getCartItems().get(i).getId() == product.getId() && controller.getCart().getCartItems().get(i).getType() == product.getType()) {
                controller.getCart().getCartItems().get(i).setFavoritImageChanged(false);
                session = mContext.getSharedPreferences("Cart", MODE_PRIVATE);
                saveGeofence(controller.getCart().getCartItems().get(i));
                break;
            }
        }
        albumList.remove(position);
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, albumList.size());
    }

    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    public void updateAnswers(List<Product> products) {
        albumList = products;
        notifyDataSetChanged();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        if (target.equals("Home")) {
            return albumList == null ? 0 : albumList.size() + 1;
        } else {
            return albumList == null ? 0 : albumList.size();

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (target.equals("Home")) {
            return (position == albumList.size()) ? FOOTER : ITEM;
        } else {
            return (position == albumList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;

        }
    }
 


    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(Product r) {
        if (r != null) {
            albumList.add(r);
        }
        notifyDataSetChanged();
    }

    public void addAll(List<Product> moveResults, SharedPreferences session, boolean update) {

        this.session = session;
        for (Product result : moveResults) {
            add(result);
            if (session != null && update) {
                saveGeofence(result);
            }
        }
    }

    public void remove(Product r) {
        int position = albumList.indexOf(r);
        if (position > -1) {
            albumList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Product());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = albumList.size() - 1;
        Product result = getItem(position);

        if (result != null) {
            albumList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Product getItem(int position) {

        return albumList.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView product_name, Price_after, price_before, discount;
        View price_before_view;
        //        ProgressBar mProgress ;
        public ImageView thumbnail, favorites, compare;
        Button cart;
        Handler favoritshandler;
        Runnable onFavoritsClicked;
        ProgressBar progressBar;


        MyViewHolder(View view) {
            super(view);
//            mProgress =  itemView.findViewById(R.id.progress);
            product_name = view.findViewById(R.id.title);
            Price_after = view.findViewById(R.id.price_after);
            price_before = view.findViewById(R.id.price_before);
            price_before_view = view.findViewById(R.id.price_before_view);
            discount = view.findViewById(R.id.discount);

            thumbnail = view.findViewById(R.id.thumbnailv);
            cart = view.findViewById(R.id.cart);
//            cart = ) view.findViewById(R.id.cart);
            favorites = view.findViewById(R.id.favorits);
            compare = view.findViewById(R.id.compare);
            favoritshandler = new Handler();
            progressBar = view.findViewById(R.id.progress);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }
    // Define a view holder for Footer view

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        ImageView showAll;
        LinearLayout layout_showAllbicycles;
        LinearLayout layout_showAllaccessories;

        public FooterViewHolder(View itemView) {
            super(itemView);
//            showAll=(ImageView) itemView.findViewById(R.id.showAll) ;
            layout_showAllbicycles = (LinearLayout) itemView.findViewById(R.id.layout_showAllbicycles);
            layout_showAllaccessories = (LinearLayout) itemView.findViewById(R.id.layout_showAllaccessories);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Do whatever you want on clicking the item
                }
            });
        }
    }


}
