package com.abouelgoukh.sporteye.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import com.abouelgoukh.sporteye.Activities.CartActivity;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MyOrdersFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model.Order;

/**
 * Created by Mohamed.Alsheikh on 6/6/2017.
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.MyodersViewHolder> {
    Context context;
    List<Order> myorderList;
    Controller controller;

    public MyOrdersAdapter(Context context, List<Order> myorderList) {
        this.context = context;
        this.myorderList = myorderList;
        controller = (Controller) context.getApplicationContext();
    }

    @Override
    public MyodersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myorders_card, parent, false);
        return new MyodersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyodersViewHolder holder, int position) {
        final Order order = myorderList.get(position);
        if (order.getStatus() == 0) {
            holder.myorder_Status.setText(context.getResources().getString(R.string.status_Pending));
            holder.myorder_Status.setTextColor(context.getResources().getColor(R.color.textedittextcolorblack));
        } else if (order.getStatus() == 1) {
            holder.myorder_Status.setText(context.getResources().getString(R.string.status_under_processing));
            holder.myorder_Status.setTextColor(context.getResources().getColor(R.color.textedittextcolorblack));
        } else if (order.getStatus() == 2) {
            holder.myorder_Status.setText(context.getResources().getString(R.string.status_completed));
            holder.myorder_Status.setTextColor(context.getResources().getColor(R.color.textedittextcolorblack));
        }
        holder.myorder_TotalPrice.setText(context.getResources().getString(R.string.code)+ " " + order.getCode());
        holder.myorder_Date.setText(context.getResources().getString(R.string.date)+ " " + order.getDate());
        Glide.with(context).load(R.drawable.order_cover).into(holder.orderImage);
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (controller.isNetworkAvailable()) {
                    if (order.getStatus() != 3) {
                        Intent intent = new Intent(context, CartActivity.class);
                        intent.putExtra("Target", "Order");
                        intent.putExtra("OrderId", order.getId());
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, "not registered yet please wait", Toast.LENGTH_SHORT).show();
                    }
                } else
                    MyOrdersFragment.snackbar.show();

            }
        });
    }


    @Override
    public int getItemCount() {
        return myorderList.size();
    }

    public class MyodersViewHolder extends RecyclerView.ViewHolder {


        TextView myorder_TotalPrice;
        TextView myorder_Date;
        ProgressBar notsubmetedOrderItem;
        TextView myorder_Status;
        ImageView orderImage;
        View view;

        public MyodersViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            myorder_TotalPrice = itemView.findViewById(R.id.myoderTotalPrice);
            notsubmetedOrderItem = itemView.findViewById(R.id.notsubmetedOrderItem);
            myorder_Date = itemView.findViewById(R.id.myoderDate);
            myorder_Status = itemView.findViewById(R.id.myoderStatus);
            orderImage = itemView.findViewById(R.id.orderimage);
        }
    }


}
