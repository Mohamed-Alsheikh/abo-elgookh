package com.abouelgoukh.sporteye.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.model.AddressModel;


public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolderAddress>{

    List<AddressModel>addresses;
    Context context;

    int counter = 0;
    public AddressAdapter(Context context, List<AddressModel>addresses){
        this.addresses =  addresses;
        this.context = context;
    }
    @Override
    public ViewHolderAddress onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_items,null);

        return new ViewHolderAddress(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolderAddress holder, final int position) {


         int posittion = holder.getAdapterPosition();



        holder.btnrmove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater vi = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View viewlinear = vi.inflate(R.layout.row_items,null);
                holder.linearLayout.removeView(viewlinear);


            }
        });


    }


    @Override
    public int getItemCount() {
        return  5;
    }



    class  ViewHolderAddress extends RecyclerView.ViewHolder{

        EditText txtaddress;
        Button btnadd;
        Button btnrmove;
        Button btnedit;
        LinearLayout linearLayout;

        Context context;
        public ViewHolderAddress(final View itemView) {
            super(itemView);
            context = itemView.getContext();
            txtaddress = (EditText)itemView.findViewById(R.id.txtaddressuser);
            btnadd = (Button) itemView.findViewById(R.id.btnadd);
            btnrmove = (Button) itemView.findViewById(R.id.btnremove);

            linearLayout = (LinearLayout)itemView.findViewById(R.id.row_item);

            btnrmove.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        addresses.remove(getAdapterPosition());
        notifyItemRemoved(getAdapterPosition());
    }
});
        }
    }
}
