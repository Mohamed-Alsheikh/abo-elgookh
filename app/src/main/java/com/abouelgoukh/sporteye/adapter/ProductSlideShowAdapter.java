package com.abouelgoukh.sporteye.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.abouelgoukh.sporteye.Fragments.ProductSlideShowFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.utils.ImageDialoge;

public class ProductSlideShowAdapter extends PagerAdapter {
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private ImageLoadingListener imageListener;
	FragmentActivity activity;
	ArrayList<String> products;
	ProductSlideShowFragment productSlideShowFragment;

	public ProductSlideShowAdapter(FragmentActivity activity, ArrayList<String> products, ProductSlideShowFragment productSlideShowFragment) {
		this.activity = activity;
		this.productSlideShowFragment = productSlideShowFragment;
		this.products = products;
		options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.ic_directions_bike_green_24dp)
				.showImageOnFail(R.drawable.ic_error)
				.cacheInMemory(false)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.build();
//		options = new DisplayImageOptions.Builder()
//				.showImageOnFail(R.drawable.ic_error)
//				.showStubImage(R.drawable.ic_launcher)
//				.showImageForEmptyUri(R.drawable.ic_empty).cacheInMemory()
//				.cacheOnDisc().build();

		imageListener = (ImageLoadingListener) new ImageDisplayListener();
	}


    @Override
	public int getCount() {
		return products.size();
	}

	@Override
	public View instantiateItem(ViewGroup container, final int position) {
		LayoutInflater inflater = (LayoutInflater) activity
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.vp_image, container, false);

		ImageView mImageView = (ImageView) view
				.findViewById(R.id.image_display);
		mImageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				openAddressDialog(products.get(position));
			}
		});
//		imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
//		imageLoader.displayImage(
//				( products.get(position)), mImageView,
//				options, imageListener);
		Glide.with(activity).
				load( products.get(position))
				.apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))

				.apply(RequestOptions.placeholderOf(R.drawable.brands))
				.into(mImageView);


		container.addView(view);
		return view;
	}
	private void openAddressDialog(String imagepath) {
		int width;
		int height;
		ImageDialoge policyTermsDialog = new ImageDialoge(activity, imagepath);
		policyTermsDialog.show();
		policyTermsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		WindowManager manager = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
		if (Build.VERSION.SDK_INT > 8) {
			width = manager.getDefaultDisplay().getWidth();
			height = manager.getDefaultDisplay().getHeight() - 60;
		} else {
			Point point = new Point();
			manager.getDefaultDisplay().getSize(point);
			width = point.x;
			height = point.y - 60;
		}
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(policyTermsDialog.getWindow().getAttributes());
		lp.width = width;
		lp.height = height;
		policyTermsDialog.getWindow().setAttributes(lp);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	private static class ImageDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
}