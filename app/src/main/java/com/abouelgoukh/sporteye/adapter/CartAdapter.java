package com.abouelgoukh.sporteye.adapter;

import android.content.Context;
import android.content.SharedPreferences;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Deleget;
import com.abouelgoukh.sporteye.Fragments.SharedProductsFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Government;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by User on 6/5/2017.
 */


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> implements Deleget {
    Context context;
    List<Product> cartList;
    Controller controller;
    String target;
    int total;
    public Deleget deleget;
    SharedPreferences session;

    Runnable onFavoritsClicked;
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();

    public CartAdapter(Context context, List<Product> cartList, String target) {
        this.context = context;
        this.cartList = cartList;
        this.target = target;
        session = context.getSharedPreferences("Cart", MODE_PRIVATE);



    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LocaleHelper.updateResources(context, LocaleHelper.getLanguage(context));
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_card, parent, false);
        controller = (Controller) context.getApplicationContext();
        return new CartViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CartViewHolder holder, final int position) {
        final Product product = cartList.get(position);
        if (product.getMount() == 0) {
            product.setMount(1);
        }
        if (target.equals("COMPARE")) {
            holder.counter.setVisibility(View.GONE);
        }
        if (product.isfavoritImageChanged()) {
            Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite);
            holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.add_towishlist.setEnabled(false);
        } else {
            Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp);
            holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.add_towishlist.setEnabled(true);
        }

        Glide.with(context).load(product.getImage()).into(holder.productimage);
        holder.productname.setText(product.getName());
        holder.productInCartCounter.setText(product.getMount() + "");
        holder.productpriceBefore.setVisibility(View.VISIBLE);
        holder.oldprice_over_View.setVisibility(View.VISIBLE);
        holder.productpriceDiscount.setVisibility(View.VISIBLE);
        holder.productpriceBefore.setText(context.getResources().getString(R.string.price) + " " + product.getPrice() + " " + context.getResources().getString(R.string.unit));
        if (product.getPrice_after_discount() != null && product.getPrice_after_discount() > 0) {

            double discount_value = ((product.getPrice() - product.getPrice_after_discount()) / product.getPrice()) * 100;
            double discount_percent = Math.floor(discount_value + 0.5d);
            holder.productpriceDiscount.setText(discount_percent + " %");
            holder.productpriceAfter.setText(context.getResources().getString(R.string.price) + " " + product.getPrice_after_discount() + " " + context.getResources().getString(R.string.unit));
        } else {
            holder.productpriceBefore.setVisibility(View.GONE);
            holder.oldprice_over_View.setVisibility(View.GONE);
            holder.productpriceDiscount.setVisibility(View.GONE);
            holder.productpriceAfter.setText(context.getResources().getString(R.string.price) + " " + product.getPrice() + " " + context.getResources().getString(R.string.unit));
        }


        if (target.equals("Order")) {
            holder.productInCartCounter.setText(product.getMount() + "");
            holder.plus.setEnabled(false);
            holder.minus.setEnabled(false);
            holder.delete.setEnabled(false);
            holder.add_towishlist.setEnabled(false);

        } else if (target.equals("Cart")) {
//
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "pos" + position, Toast.LENGTH_SHORT).show();
                removeAt(position, holder.getAdapterPosition(), product, Integer.parseInt(holder.productInCartCounter.getText().toString()));
//                cartList.remove(position);
                if (target.equals("Cart")) {
                    session = context.getSharedPreferences("Cart", MODE_PRIVATE);
                } else if (target.equals("COMPARE")) {
                    session = context.getSharedPreferences("COMPARE", MODE_PRIVATE);

                }

                removeSavedGeofences(product);

            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = context.getSharedPreferences("Cart", MODE_PRIVATE);
                int x = Integer.parseInt(holder.productInCartCounter.getText().toString());
                x = x + 1;
                holder.productInCartCounter.setText(x + "");
                product.setMount(x);
                if (product.getPrice_after_discount() != null && product.getPrice_after_discount() > 0) {
                    onDeleteCartitem(product.getPrice_after_discount(), "plus", 0);

                } else {
                    onDeleteCartitem(product.getPrice(), "plus", 0);
                }
                saveGeofence(product);
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = context.getSharedPreferences("Cart", MODE_PRIVATE);
                int x = Integer.parseInt(holder.productInCartCounter.getText().toString());
                if (x > 1) {
                    x = x - 1;
                    holder.productInCartCounter.setText(x + "");
                    product.setMount(x);
                    if (product.getPrice_after_discount() != null && product.getPrice_after_discount() > 0) {
                        onDeleteCartitem(product.getPrice_after_discount(), "minus", 0);
                    } else {
                        onDeleteCartitem(product.getPrice(), "minus", 0);
                    }
                    saveGeofence(product);
                }

            }
        });


        holder.add_towishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (controller.isNetworkAvailable()) {

                    Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite);
                    holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    product.setFavoritImageChanged(true);

                    holder.favoritshandler.removeCallbacks(onFavoritsClicked);
                    onFavoritsClicked = new Runnable() {
                        @Override
                        public void run() {
                            if (product.isfavoritImageChanged()) {
                                setfavorits(product.getType(), product.getId(), 1, holder, product, position);
                            } else {
                                setfavorits(product.getType(), product.getId(), 0, holder, product, position);
                            }
                        }
                    };
                    holder.favoritshandler.postDelayed(onFavoritsClicked, 1000);
                } else {
                    SharedProductsFragment.snackbar.show();
                    MainActivity.appBarLayout.setExpanded(false);
                }


            }
        });

    }

    private void setfavorits(int type, int prduct_id, int status, final CartViewHolder holder, final Product album, final int position) {
        session = context.getSharedPreferences("Favorits", MODE_PRIVATE);
        if (callfavoriteService(type, prduct_id, status, album, holder) != null) {
            callfavoriteService(type, prduct_id, status, album, holder).enqueue(new Callback<SimpleResponse>() {
                @Override
                public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                    if (response.isSuccessful()) {

                        if (response.body().getStatus() == 200) {
                            album.setFavoritImageChanged(true);
                            Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite);
                            holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            saveGeofence(album);
                            Toast.makeText(context, "Added To Favorite List", Toast.LENGTH_SHORT).show();
                            removeAt(position, holder.getAdapterPosition(), album, Integer.parseInt(holder.productInCartCounter.getText().toString()));

                            removeSavedGeofences(album);
                        } else if (response.body().getStatus() == 300) {
                            album.setFavoritImageChanged(false);
                            Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp);
                            holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            removeSavedGeofences(album);

                        } else {
                            Toast.makeText(context, "there is an error", Toast.LENGTH_SHORT).show();
                            if (album.isfavoritImageChanged()) {
                                album.setFavoritImageChanged(false);
                                Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite);
                                holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            } else {
                                album.setFavoritImageChanged(true);
                                Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp);
                                holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                            }

                        }

                    } else {
                        int statusCode = response.code();
                        if (album.isfavoritImageChanged()) {
                            album.setFavoritImageChanged(false);
                            Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite);
                            holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                        } else {
                            album.setFavoritImageChanged(true);
                            Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp);
                            holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                        }
                        Toast.makeText(context, "there is error in connection", Toast.LENGTH_SHORT).show();
                        // handle request errors depending on status code
                    }


                }

                @Override
                public void onFailure(Call<SimpleResponse> call, Throwable t) {
                    if (album.isfavoritImageChanged()) {
                        album.setFavoritImageChanged(false);
                        Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite);
                        holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    } else {
                        album.setFavoritImageChanged(true);
                        Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp);
                        holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
                    }
                    Toast.makeText(context, "there is error in connection", Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    private Call<SimpleResponse> callfavoriteService(int type, int productid, int status, Product album, CartViewHolder holder) {
        if (controller.getprefrancesUserApiToken() != null) {
            switch (type) {
                case 1:
                    return mService.getfavoratePruduct(productid, controller.getprefrancesUserApiToken(), status);
                case 2:
                    return mService.favorateaccessory(productid, controller.getprefrancesUserApiToken(), status);
                case 3:
                    return mService.favorateOtherProduct(productid, controller.getprefrancesUserApiToken(), status);
            }
        } else {
            Toast.makeText(context, "please login to add favorite Products", Toast.LENGTH_SHORT).show();
            if (!album.isfavoritImageChanged()) {
                album.setFavoritImageChanged(false);
                Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite);
                holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            } else {
                album.setFavoritImageChanged(true);
                Drawable img = context.getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp);
                holder.add_towishlist.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            }

        }
        return null;
    }

    private void saveGeofence(Product product) {

        Gson gson = new Gson();
        String json = gson.toJson(product);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(product.getId().toString() + product.getType().toString() + "", json);
        editor.apply();
    }

    private void removeSavedGeofences(Product product) {
        SharedPreferences.Editor editor = session.edit();
        editor.remove(product.getId().toString() + product.getType().toString() + "");
        editor.apply();
    }

    public void removeAt(int position, int adapterPosition, Product product, int amount) {
        if (target.equals("Cart")) {
            controller.deleteCartProducts(product.getId(), product.getType(), product);
            MainActivity.setupMessagesBadge(controller.getcartProductsArraylistsize());
        } else if (target.equals("COMPARE")) {
            controller.deleteCompareProducts(product.getId(), product.getType(), product);
            MainActivity.setupMessagesBadgeCompare(controller.getcompareProductsArraylistsize());
        }
        notifyItemRemoved(adapterPosition);
        notifyItemRangeChanged(adapterPosition, cartList.size());

        if (product.getPrice_after_discount() != null && product.getPrice_after_discount() > 0) {
            onDeleteCartitem(product.getPrice_after_discount(), "delete", amount);
        } else {
            onDeleteCartitem(product.getPrice(), "delete", amount);
        }


    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    @Override
    public void onSaveAddress(Integer Address_id, String addressTxt, Government government, String goven_name) {

    }

    @Override
    public void onDeleteCartitem(Double deleted_item_price, String operation, int amount) {
        deleget.onDeleteCartitem(deleted_item_price, operation, amount);
    }

    public class CartViewHolder extends RecyclerView.ViewHolder {
        ImageView productimage;
        TextView productname;
        TextView productpriceBefore;
        View oldprice_over_View;
        TextView productpriceAfter;
        TextView productpriceDiscount;
        TextView productInCartCounter;
        ImageView plus;
        ImageView minus;
        Button delete, add_towishlist;
        Handler favoritshandler;
        LinearLayout counter;

        public CartViewHolder(View itemView) {
            super(itemView);
            counter = itemView.findViewById(R.id.counter);
            productimage = (ImageView) itemView.findViewById(R.id.cartproductimage);
            productname = (TextView) itemView.findViewById(R.id.productname);
            productpriceBefore = (TextView) itemView.findViewById(R.id.cartproductprice_before);
            oldprice_over_View = (View) itemView.findViewById(R.id.oldprice_over_View);
            productpriceAfter = (TextView) itemView.findViewById(R.id.cartproductprice_after);
            productpriceDiscount = (TextView) itemView.findViewById(R.id.cartproductprice_discount);

            productInCartCounter = (TextView) itemView.findViewById(R.id.productincartcounter);
            plus = (ImageView) itemView.findViewById(R.id.plus);
            minus = (ImageView) itemView.findViewById(R.id.minuse);
            delete = (Button) itemView.findViewById(R.id.delete);
            add_towishlist = (Button) itemView.findViewById(R.id.add_towishlist);
            favoritshandler = new Handler();

        }
    }
}
