package com.abouelgoukh.sporteye.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Fragments.BrandProductsFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.BrandsFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.Brand;


/**
 * Created by User on 5/29/2017.
 */


public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.BrandsViewHolder> {
    Context context;
     ArrayList<Brand> brandsalbumlist;
    String Brand_type;
    public static int main_Product_id;
    Bundle Brands_arguments = new Bundle();

    public BrandsAdapter(Context context, ArrayList<Brand> brandsalbumlist, String Brand_type) {
        this.context = context;
        this.brandsalbumlist = brandsalbumlist;
        this.Brand_type = Brand_type;
    }

    @Override
    public BrandsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.brands_album_card, parent, false);
        return new BrandsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BrandsViewHolder holder, final int position) {
        final Brand brandsAlbum = brandsalbumlist.get(position);
        Glide.with(context).
                load(brandsAlbum.getLogo())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .apply(RequestOptions.circleCropTransform())
                .apply(RequestOptions.placeholderOf(R.drawable.brands))
                .into(holder.brandimage);

//        Picasso.with(context)
//                .load(brandsAlbum.getLogo())
//                .placeholder(R.drawable.brands)
//                .transform(new CircleTransform())
//                .into(holder.brandimage);

        holder.brandname.setText(brandsAlbum.getName());
        holder.Stockcount.setText(brandsAlbum.getItems() + " Items");

        holder.brandimage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switchContent(brandsAlbum.getBrandId(), brandsAlbum.getName());
            }
        });

    }


    public void switchContent(int brandid, String name) {
        if (context == null)
            return;
        if (context instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) context;
            Fragment fragment = null;
            if (Brand_type.equals("OtherProductsCatogery")) {
                fragment = new BrandsFragment();
                mainActivity.switchContent(false, brandsalbumlist,brandid, name, "OtherProductsSubCatogery", fragment);
            } else {

                    fragment = new BrandProductsFragment();
                    mainActivity.switchContent(true, brandsalbumlist,brandid, name, Brand_type, fragment);


            }

        }
    }

    @Override
    public int getItemCount() {
        return brandsalbumlist.size();
    }

    public void addAll(ArrayList<Brand> brands) {
        for (Brand result : brands) {
            add(result);
        }
    }

    public void add(Brand brand) {
        brandsalbumlist.add(brand);
        notifyItemInserted(brandsalbumlist.size() - 1);
    }


    public class BrandsViewHolder extends RecyclerView.ViewHolder {
        TextView brandname;
        TextView Stockcount;
        ImageView brandimage;

        public BrandsViewHolder(View itemView) {
            super(itemView);
            brandname = (TextView) itemView.findViewById(R.id.brandname);
            brandimage = (ImageView) itemView.findViewById(R.id.brandbmage);
            Stockcount = (TextView) itemView.findViewById(R.id.Stockcount);


        }
    }
}
