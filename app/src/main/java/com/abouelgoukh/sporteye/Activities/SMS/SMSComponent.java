package com.abouelgoukh.sporteye.Activities.SMS;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = SMSModule.class)
public interface SMSComponent {
    SMSPresenter providePresenter();
    SMSModel provideModel();

}
