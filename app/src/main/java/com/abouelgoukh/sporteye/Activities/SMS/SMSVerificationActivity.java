package com.abouelgoukh.sporteye.Activities.SMS;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.IntentFilter;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.Activities.SMS.sms_reciever.SMSReceiver;
import com.abouelgoukh.sporteye.Activities.SMS.sms_reciever.SmsListener;
import com.abouelgoukh.sporteye.AppSignatureHelper;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.databinding.ActivitySmsverificationBinding;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

public class SMSVerificationActivity extends AppCompatActivity implements SMSContract.View, View.OnClickListener, SmsListener {
    ActivitySmsverificationBinding binding;
    @Inject
    SMSPresenter presenter;
    public static final String OTP_REGEX = "[0-9]{4}";
    String otp;
    ProgressDialog progressDialog;
String userName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SmsRetrieverClient client = SmsRetriever.getClient(SMSVerificationActivity.this);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Android will provide message once receive. Start your broadcast receiver.
                IntentFilter filter = new IntentFilter();
                filter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
                registerReceiver(new SMSReceiver(), filter);
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
            }
        });
        userName=getIntent().getStringExtra("UserName");
        binding = DataBindingUtil.setContentView(SMSVerificationActivity.this, R.layout.activity_smsverification);
        SMSComponent smsComponent = DaggerSMSComponent.builder().sMSModule(new SMSModule(this, SMSVerificationActivity.this)).build();
        presenter = smsComponent.providePresenter();
        presenter.model = smsComponent.provideModel();
        SMSReceiver.bindListener(this);
        presenter.startDownCounter();
        binding.sendSMS.setEnabled(false);
        binding.sendSMS.setOnClickListener(this);
        binding.btnSignup.setOnClickListener(this);
//        binding.constraint_main_content.setOnClickListener(this);

        m1Actioin();
        m2Actioin();
        m3Actioin();
        m4Actioin();
        progressDialog = new ProgressDialog(SMSVerificationActivity.this);
        progressDialog.setMessage("Activate Your Acount ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
//                if (callLoginService != null && callLoginService.isExecuted()) {
//                    callLoginService.cancel();
//                    Toast.makeText(LoginActivity.this, "Login canceled", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void m1Actioin() {
        binding.m1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    binding.m2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void m2Actioin() {
        binding.m2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    binding.m3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void m3Actioin() {
        binding.m3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    binding.m4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void m4Actioin() {
        binding.m4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                hideSoftKeyboard(SMSVerificationActivity.this);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    @Override
    public void changeWaitingCounterText(String counterValue) {
        binding.smsCounter.setText(counterValue);
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(SMSVerificationActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void changeArrowNavVisibility(boolean visible) {
        if (visible) {
            binding.imageViewUpArrow.setVisibility(View.VISIBLE);
            binding.sendSMS.setEnabled(true);
        } else {
            binding.imageViewUpArrow.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgressDialode(boolean show) {
        if (show) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void userVerifiedSuccessfully() {
        Intent intent=new Intent(this, MainActivity.class);
      startActivity(intent);
    }

    @Override
    public void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendSMS:
                presenter.sendSMS(userName);
                binding.sendSMS.setEnabled(false);
                break;
            case R.id.btn_signup:
                showProgressDialode(true);
                presenter.ValidateVerificationCode((binding.m1.getText() + "") + (binding.m2.getText()) + (binding.m3.getText()) + (binding.m4.getText()) + "");
                break;
            case R.id.constraint_main_content:
                hideSoftKeyboard(SMSVerificationActivity.this);
                break;
        }
    }

    @Override
    public void messageReceived(String messageText) {
//From the received text string you may do string operations to get the required OTP
        //It depends on your SMS format
        presenter.doWhenFinish_OR_Error();
//        Log.e("Message", messageText);
//        showToast("Message: " + messageText);

        // If your OTP is six digits number, you may use the below code

        Pattern pattern = Pattern.compile(OTP_REGEX);
        Matcher matcher = pattern.matcher(messageText);

        while (matcher.find()) {
            otp = matcher.group();
        }
        binding.m1.setText(otp.charAt(0) + "");
        binding.m2.setText(otp.charAt(1) + "");
        binding.m3.setText(otp.charAt(2) + "");
        binding.m4.setText(otp.charAt(3) + "");

    }

}
