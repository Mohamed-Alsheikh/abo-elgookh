package com.abouelgoukh.sporteye.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.ads_model.Ad;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.ads_model.AdsAnswerResponse;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    int SPLASH_TIME_OUT = 2000;
    ImageView zoom;
    Animation zoomAnimation;
    private RotateLoading rotateLoading;
    public static List<Ad> adsList = new ArrayList<>();
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);
        rotateLoading = (RotateLoading) findViewById(R.id.rotateloading);

        rotateLoading.start();
        zoom = (ImageView) findViewById(R.id.imgLogo);
        zoomAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.zoom);
        if (isNetworkAvailable()) {
            getAds();
        } else {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    zoom.startAnimation(zoomAnimation);
                    zoom.animate()
                            .alpha(0.0f)
                            .setDuration(800)
//                        .translationY(zoom.getHeight())
//                        .translationX(zoom.getWidth())
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    if (rotateLoading.isStart()) {
                                        rotateLoading.stop();
                                    }
                                }
                            });
                }
            }, SPLASH_TIME_OUT);
        }
        zoomAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SplashScreen.this);
                finish();
                if (preferences.getBoolean("IS.THIS.IS.FIRST.TIME", true)) {
                    Intent i = new Intent(SplashScreen.this, SetLangActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void getAds() {
        callAdsService().enqueue(new Callback<AdsAnswerResponse>() {
            @Override
            public void onResponse(Call<AdsAnswerResponse> call, Response<AdsAnswerResponse> response) {
                zoom.startAnimation(zoomAnimation);
                zoom.animate()
                        .alpha(0.0f)
                        .setDuration(800)
//                        .translationY(zoom.getHeight())
//                        .translationX(zoom.getWidth())
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                if (rotateLoading.isStart()) {
                                    rotateLoading.stop();
                                }
                            }
                        });
                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {

                        adsList = response.body().getAds();


                    } else if (response.body().getStatus() == 300) {
                        //the pruduct id is wrong

                    } else if (response.body().getStatus() == 402) {

                    }


                } else {
                    int statusCode = response.code();

                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call<AdsAnswerResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "there is error in connection", Toast.LENGTH_SHORT).show();
                zoom.startAnimation(zoomAnimation);
                zoom.animate()
                        .alpha(0.0f)
                        .setDuration(800)
//                        .translationY(zoom.getHeight())
//                        .translationX(zoom.getWidth())
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                if (rotateLoading.isStart()) {
                                    rotateLoading.stop();
                                }
                            }
                        });
            }

        });
    }

    private Call<AdsAnswerResponse> callAdsService() {

        return mService.getAds();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}







