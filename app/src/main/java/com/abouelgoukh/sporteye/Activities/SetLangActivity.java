package com.abouelgoukh.sporteye.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.helper.LocaleHelper;

public class SetLangActivity extends AppCompatActivity {
Button next;
RadioGroup langRadioGroup;
RadioButton english;
RadioButton arabic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(SetLangActivity.this));

        setContentView(R.layout.activity_set_lang);
        next = findViewById(R.id.next);
        langRadioGroup = findViewById(R.id.LangRadioGroup);
        english = findViewById(R.id.english);
        arabic = findViewById(R.id.arabic);
        if (LocaleHelper.getLanguage(this).equals("en")) {
            english.setChecked(true);
        } else if (LocaleHelper.getLanguage(this).equals("ar")) {
            arabic.setChecked(true);
        }
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SetLangActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("IS.THIS.IS.FIRST.TIME", false);
                editor.apply();
                Intent i = new Intent(SetLangActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        langRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.english:
                        LocaleHelper.updateResources(SetLangActivity.this, "en");
                        finish();
                        startActivity(getIntent());
                        break;
                    case R.id.arabic:
                        LocaleHelper.updateResources(SetLangActivity.this, "ar");
                        finish();
                        startActivity(getIntent());
                        break;

                }
            }
        });

    }
}
