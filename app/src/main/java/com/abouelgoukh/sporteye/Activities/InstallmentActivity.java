package com.abouelgoukh.sporteye.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.helper.PayFortPayment;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class InstallmentActivity extends AppCompatActivity {
//        private final static String MERCHANT_IDENTIFIER = "e54638eb";
    private final static String MERCHANT_IDENTIFIER = "ZpUKVupk";
//        private final static String ACCESS_CODE = "hRRVGXrIpHSYoH19Ebwt";
    private final static String ACCESS_CODE = "iGFy1jxX6LUWv6Z6cY24";
    private final static String SHA_TYPE = "SHA-256";
    private final static String SHA_REQUEST_PHRASE = "sportEyeByAbouElgoukh";
    public final static String SHA_RESPONSE_PHRASE = "sportEyeByAbouElgoukh";
    private final static String TEST_TOKEN_URL = "https://sbcheckout.payfort.com/FortAPI/paymentPage";
    private final static String LIVE_TOKEN_URL = "https://checkout.payfort.com/FortAPI/paymentPage";
    //WS params
    private final static String KEY_MERCHANT_IDENTIFIER = "merchant_identifier";
    private final static String KEY_LANGUAGE = "language";
    private final static String KEY_ACCESS_CODE = "access_code";
    private final static String KEY_SIGNATURE = "signature";
    boolean BACKEFLAG = false;
    Handler handler;
    Runnable r;
    String KEY_amount = "amount";
    String command = "PURCHASE";
    String KEY_command = "command";
    String currency = "EGP";
    String KEY_currency = "currency";
    String KEY_customer_email = "customer_email";

    String KEY_merchant_reference = "merchant_reference";
    String installments = "STANDALONE";
    String KEY_installments = "installments";
    String KEY_return_url = "return_url";
    String return_url = "http://abualgokh.itqanserver.com/api/orderStatus";
    WebView webView;
    ProgressDialog progressDialog;
    private boolean paymentCanceledToShowMerchantPage;
    private int CANCEL_BEFORE_PAYMENT = 111;
    private int CANCEL_AFTER_PAYMENT = 222;
    private int CANCEL_AFTER_THEIR_PAYMENT_CONFIRMATION = 333;
    private int CANCEL_AFTER_OUR_PAYMENT_CONFIRMATION = 444;
    private int Cancel_RESULT;
    String merchantReference;
boolean paymentDone;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(InstallmentActivity.this));
        setContentView(R.layout.activity_installment);
        Cancel_RESULT = CANCEL_BEFORE_PAYMENT;
        progressDialog = new ProgressDialog(InstallmentActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (Cancel_RESULT == CANCEL_BEFORE_PAYMENT) {
                    onBackPressed();
                }
            }
        });
        progressDialog.show();
        webView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.setScrollbarFadingEnabled(false);
        webView.setWebChromeClient(new WebChromeClient()); //same as above
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(InstallmentActivity.this);
                builder.setMessage(R.string.notification_error_ssl_cert_invalid);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {

                if (url.contains("paymentPage") && url.contains("token")) {
                    Cancel_RESULT = CANCEL_BEFORE_PAYMENT;
//                    Toast.makeText(InstallmentActivity.this, "Cancel_RESULT " + Cancel_RESULT, Toast.LENGTH_LONG).show();
                } else if (url.contains("paymentPage") && !url.contains("token")) {
                    Cancel_RESULT = CANCEL_BEFORE_PAYMENT;
//                    Toast.makeText(InstallmentActivity.this, "Cancel_RESULT " + Cancel_RESULT, Toast.LENGTH_LONG).show();

                } else if (url.contains("cancelOperation")) {
                    paymentCanceledToShowMerchantPage = true;
//                    Toast.makeText(InstallmentActivity.this, "Cancel_RESULT " + Cancel_RESULT, Toast.LENGTH_LONG).show();
                    Cancel_RESULT = CANCEL_BEFORE_PAYMENT;
                } else if (!paymentCanceledToShowMerchantPage && url.contains("http://abualgokh.itqanserver.com/api/orderStatus")) {// condition to intercept webview's request
                    Cancel_RESULT = CANCEL_AFTER_OUR_PAYMENT_CONFIRMATION;
                    String urwl = url;
//                    Toast.makeText(InstallmentActivity.this, "Cancel_RESULT " + Cancel_RESULT, Toast.LENGTH_LONG).show();
                }
                if (url.contains("redirectionResponse/return3DsNotEnrolledStatus?fort_id=")) {// condition to intercept webview's request
//                    Toast.makeText(InstallmentActivity.this, "Cancel_RESULT " + Cancel_RESULT, Toast.LENGTH_LONG).show();
                    paymentDone = true;
                    Cancel_RESULT = CANCEL_AFTER_OUR_PAYMENT_CONFIRMATION;
                } else if ( url.contains("backToMerchant")) {
                    progressDialog.setMessage("open Merchant Page");
                    Cancel_RESULT = CANCEL_AFTER_OUR_PAYMENT_CONFIRMATION;
                    handler = new Handler();
                    r = new Runnable() {
                        public void run() {
                            paymentDone = true;
                            onBackPressed();
//                            handler.postDelayed(this, 1000);
                        }
                    };
                    handler.postDelayed(r, 15000);
                } else{
                    progressDialog.setMessage("payment process");
                }
                progressDialog.dismiss();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                Toast.makeText(InstallmentActivity.this, "url \n" + url, Toast.LENGTH_LONG).show();
                if (url.contains("paymentPage") && url.contains("token")) {
                    progressDialog.setMessage("Loading Installment Page");
                    progressDialog.setCancelable(true);
                } else if (url.contains("paymentPage") && !url.contains("token")) {
                    progressDialog.setCancelable(true);
                } else {
                    progressDialog.setCancelable(false);
                }
//                if (url.contains("secure3d")|url.contains("https://sbcheckout.payfort.com/FortAPI/redirectionResponse/")) {// condition to intercept webview's request
//                    progressDialog.setCancelable(false);
//                }else   if(url.contains("https://sbcheckout.payfort.com/FortAPI/general/backToMerchant")){
//                    progressDialog.setCancelable(false);
//                    Cancel_RESULT = CANCEL_AFTER_OUR_PAYMENT_CONFIRMATION;
//                }
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
            }

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                progressDialog.dismiss();
                super.onPageCommitVisible(view, url);
            }

            @Override
            public void onFormResubmission(WebView view, Message dontResend, Message resend) {
                super.onFormResubmission(view, dontResend, resend);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                super.shouldOverrideUrlLoading(view, url);
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:

                view.loadUrl(url);
                return false; // then it is not handled by default action
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
            }
        });

        Controller controller = (Controller) getApplicationContext();
        merchantReference = String.valueOf(System.currentTimeMillis());
        String amount = getIntent().getStringExtra("amount");
        String language = Locale.getDefault().getLanguage();
        String email = controller.getprefrancesUser_email();
        String signature = createSignature(amount, email, language, merchantReference);
        callWebPaymentService(amount, email, language, merchantReference, signature);

    }

    private void callWebPaymentService(String amount, String email, String language, String merchantReference, String signature) {

        String query = "command=" + PayFortPayment.PURCHASE
                + "&access_code=" + ACCESS_CODE
                + "&merchant_identifier=" + MERCHANT_IDENTIFIER
                + "&merchant_reference=" + merchantReference
                + "&amount=" + amount
                + "&currency=" + PayFortPayment.CURRENCY_TYPE
                + "&installments=" + "STANDALONE"
                + "&language=" + language
                + "&customer_email=" + email
                + "&return_url=" + "http://abualgokh.itqanserver.com/api/orderStatus"
                + "&signature=" + signature;
        webView.postUrl(LIVE_TOKEN_URL, query.getBytes());
    }

    private String createSignature(String amount, String email, String language, String merchantReference) {
        String concatenatedString = SHA_REQUEST_PHRASE +
                KEY_ACCESS_CODE + "=" + ACCESS_CODE +
                KEY_amount + "=" + amount +
                KEY_command + "=" + PayFortPayment.PURCHASE +
                KEY_currency + "=" + currency +
                KEY_customer_email + "=" + email +
                KEY_installments + "=" + installments +
                KEY_LANGUAGE + "=" + language +
                KEY_MERCHANT_IDENTIFIER + "=" + MERCHANT_IDENTIFIER +
                KEY_merchant_reference + "=" + merchantReference +
                KEY_return_url + "=" + return_url +
                SHA_REQUEST_PHRASE;

        return getSignatureSHA256(concatenatedString);
    }

    private static String getSignatureSHA256(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();
            return String.format("%0" + (messageDigest.length * 2) + 'x', new BigInteger(1, messageDigest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (Cancel_RESULT==CANCEL_AFTER_OUR_PAYMENT_CONFIRMATION) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("merchantReference", merchantReference);
            setResult(200, returnIntent);
//            Toast.makeText(InstallmentActivity.this, "code " + Cancel_RESULT, Toast.LENGTH_LONG).show();
            webView.clearHistory();
            webView.destroy();
            webView = null;
            finish();
        } else if (Cancel_RESULT == CANCEL_BEFORE_PAYMENT) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("merchantReference", merchantReference);
            setResult(111, returnIntent);
            webView.clearHistory();
            webView.destroy();
            webView = null;
            finish();
//            Toast.makeText(InstallmentActivity.this, "code " + Cancel_RESULT, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
