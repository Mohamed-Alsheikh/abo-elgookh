package com.abouelgoukh.sporteye.Activities.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    private static final int COUNT = 25;



    static {
        // Add 3 sample items.
        addItem(new DummyItem("1", "eBookFrenzy"));
        addItem(new DummyItem("2", "Google"));
        addItem(new DummyItem("3", "Android"));
        addItem(new DummyItem("4", "Android"));
        addItem(new DummyItem("5", "Android"));
        addItem(new DummyItem("6", "Android"));
        addItem(new DummyItem("7", "Android"));
        addItem(new DummyItem("8", "Android"));
        addItem(new DummyItem("8", "Android"));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }



    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public String id;
        public String website_name;


        public DummyItem(String id, String website_name)
        {
            this.id = id;
            this.website_name = website_name;

        }

        @Override
        public String toString() {
            return website_name;
        }
    }
}
