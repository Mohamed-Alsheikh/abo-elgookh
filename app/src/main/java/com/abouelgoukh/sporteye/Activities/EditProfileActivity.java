package com.abouelgoukh.sporteye.Activities;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import com.abouelgoukh.sporteye.Fragments.DataChanagePasswordFragment;
import com.abouelgoukh.sporteye.Fragments.EditeProfileFragment;
import com.abouelgoukh.sporteye.R;

public class EditProfileActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener,EditeProfileFragment.OnFragmentInteractionListener {
    TabLayout tabLayout;
    private int[] tabIcons = new int[]{R.drawable.ic_account_circle_black_24dp, R.drawable.ic_lock_black_24dp};
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(EditProfileActivity.this));
        setContentView(R.layout.activity_edit_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Edit Profile");
        toolbar.setTitleTextColor(getResources().getColor(R.color.textbuttoncolorwhite));
        toolbar.setNavigationIcon(com.google.android.material.R.drawable.abc_ic_ab_back_material);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);
        setupTabIcons();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList();
        private final List<String> mFragmentTitleList = new ArrayList();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        public Fragment getItem(int position) {
            return (Fragment) this.mFragmentList.get(position);
        }

        public int getCount() {
            return this.mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            this.mFragmentList.add(fragment);
            this.mFragmentTitleList.add(title);
        }

        public CharSequence getPageTitle(int position) {
            return (CharSequence) this.mFragmentTitleList.get(position);
        }
    }
    private void setupTabIcons() {
        this.tabLayout.getTabAt(0).setIcon(this.tabIcons[0]);
        this.tabLayout.getTabAt(1).setIcon(this.tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {
        DataChanagePasswordFragment dataChanagePasswordFragment = new DataChanagePasswordFragment();
        EditeProfileFragment editeProfileFragment = new EditeProfileFragment();
        ViewPagerAdapter ViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        ViewPagerAdapter.addFragment(editeProfileFragment, "Edit Profile");
        ViewPagerAdapter.addFragment(dataChanagePasswordFragment, " Change Password");
        viewPager.setAdapter(ViewPagerAdapter);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
