package com.abouelgoukh.sporteye.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Gallery;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.GalleryAnswerRespone;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.utils.ImageDialoge;

public class ProductDetailsActivity extends SwipeBackActivity implements View.OnClickListener {
    CollapsingToolbarLayout collapsingToolbarLayout;
    Product sample;
    ImageView product_image;
    TextView pdtIdTxt;
    TextView pdtNameTxt;
    TextView pdt_Disc;
    TextView pdt_Price;
    TextView pdt_new_Price;
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();
    List<Gallery> galleryList = new ArrayList<>();
    boolean flag;

    SharedPreferences session;
    private Gson gson;
    Controller controller;
    Resources resources;
    Snackbar snackbar;
    private SwipeBackLayout mSwipeBackLayout;
    Runnable onFavoritsClicked;
    Handler favoritshandler = new Handler();
    TextView pdt_id_title;
    TextView pdt_new_Price_title;
    TextView pdt_Price_title;
    TextView pdt_Disc_title;
    RelativeLayout pdt_new_Price_layout;
    CoordinatorLayout pd_main_content;
    FloatingActionButton carticon;
    FloatingActionButton favoritIcon;
    FloatingActionButton compareicon;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(ProductDetailsActivity.this));
        setContentView(R.layout.activity_product_details);
        mSwipeBackLayout = getSwipeBackLayout();
        setDragEdge(SwipeBackLayout.DragEdge.TOP);
        mSwipeBackLayout.setEnabled(false);

        sample = (Product) getIntent().getExtras().getParcelable("Product");
        flag = sample.isfavoritImageChanged();

        setupWindowAnimations();

        findViewById();
        setProductItem(sample);
        dynamicToolbarColor();
        toolbarTextAppernce();

        collapsingToolbarLayout.setTitle(sample.getName());


        snackbar = Snackbar.make(pd_main_content, "No internet connection!", Snackbar.LENGTH_LONG).
                setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        controller = (Controller) ProductDetailsActivity.this.getApplicationContext();
        manageCart();
//        if(MainActivity.controller.isNetworkAvailable()){
//
//        }else{
//            prepareProductDetails();
//            snackbar.show();
//        }
    }

    private void findViewById() {
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(com.google.android.material.R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        product_image = (ImageView) findViewById(R.id.header);

        pdtNameTxt = (TextView) findViewById(R.id.pdt_name);
        pdt_Disc = (TextView) findViewById(R.id.pdt_Disc);
        pdt_Price = (TextView) findViewById(R.id.pdt_Price);
        pdt_new_Price = (TextView) findViewById(R.id.pdt_new_Price);
        pdtIdTxt = (TextView) findViewById(R.id.pdt_type);
        pdt_new_Price_layout = (RelativeLayout) findViewById(R.id.pdt_new_Price_layout);
        pd_main_content = (CoordinatorLayout) findViewById(R.id.pd_main_content);
        carticon = (FloatingActionButton) findViewById(R.id.carticon);
        favoritIcon = (FloatingActionButton) findViewById(R.id.favoritsicon);
        compareicon = (FloatingActionButton) findViewById(R.id.compareicon);
        pdt_new_Price_layout = (RelativeLayout) findViewById(R.id.pdt_new_Price_layout);
        pdt_id_title = (TextView) findViewById(R.id.pdt_id_title);
        pdt_new_Price_title = (TextView) findViewById(R.id.pdt_new_Price_title);
        pdt_Price_title = (TextView) findViewById(R.id.pdt_Price_title);
        pdt_Disc_title = (TextView) findViewById(R.id.pdt_Disc_title);

        pdt_id_title.setText(getResources().getString(R.string.product_type));
        pdt_new_Price_title.setText(getResources().getString(R.string.productNewPrice));
        pdt_Price_title.setText(getResources().getString(R.string.productPrice));
        pdt_Disc_title.setText(getResources().getString(R.string.product_Description));

//        compareIcon = (ImageView)findViewById(R.id.compareicon);
//		pdtImg = (ImageView)findViewById(R.id.product_detail_img);
    }

    private void setProductItem(Product resultProduct) {
        Glide.with(ProductDetailsActivity.this).
                load(resultProduct.getImage())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                .apply(RequestOptions.placeholderOf(R.drawable.brands))
                .into(product_image);
        product_image.setOnClickListener(this);
        pdtNameTxt.setText("" + resultProduct.getName());
        pdt_Disc.setText(resultProduct.getDescription() + "");
        pdt_Price.setText(resultProduct.getPrice() + " " + getResources().getString(R.string.unit));
        if (resultProduct.getPrice_after_discount() != null && resultProduct.getPrice_after_discount() > 0 && resultProduct.getPrice_after_discount() != null) {
            pdt_new_Price.setText(resultProduct.getPrice_after_discount() + " " + getResources().getString(R.string.unit));
        } else
            pdt_new_Price_layout.setVisibility(View.GONE);
        pdtIdTxt.setText("Product Id: " + resultProduct.getId());
        if (resultProduct.getType() == 1) {
            pdtIdTxt.setText("Bicycle");
        } else {
            pdtIdTxt.setText("Accessory");
        }
//		imageLoader.displayImage(resultProduct.getImageUrl(), pdtImg, options,
//				imageListener);
    }

    private void setupWindowAnimations() {
        // We are not interested in defining a new Enter Transition. Instead we change default transition duration
        getWindow().getEnterTransition().setDuration(getResources().getInteger(R.integer.anim_duration_long));
    }

    private void dynamicToolbarColor() {

        collapsingToolbarLayout.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
        collapsingToolbarLayout.setStatusBarScrimColor(getResources().getColor(R.color.colorPrimary));

    }

    private void toolbarTextAppernce() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }

    private void manageCart() {

        if (sample.isCartImageChanged()) {
//            carticon.setBackground(ContextCompat.getDrawable(ProductDetailsActivity.this, R.drawable.btn_cart_clicked_bg));
            carticon.setImageResource(R.drawable.ic_shopping_cartwhite);
        } else {
            carticon.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
        }
        if (sample.isCompareImageChanged) {
//            carticon.setBackground(ContextCompat.getDrawable(ProductDetailsActivity.this, R.drawable.btn_cart_clicked_bg));
            compareicon.setImageResource(R.drawable.ic_compare_red_24dp);
        } else {
            compareicon.setImageResource(R.drawable.ic_compare_black_24dp);
        }
//        if (controller.checkCompareInProducts(sample.getId())) {
//            compareIcon.setImageResource(R.drawable.ic_compare);
//        }
        if (sample.isfavoritImageChanged()) {
            favoritIcon.setImageResource(R.drawable.ic_favorite);
        } else {
            favoritIcon.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }

        carticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = getSharedPreferences("Cart", MODE_PRIVATE);
                if (!controller.CheckProductInCart(sample.getId(), sample.getType())) {
                    int indexof_added_element = controller.getcartProductsArraylistsize();
                    controller.setCartOfProducts(sample.getId(), sample.getType());
                    (controller.getCart()).setProducts(sample);
                    sample.setCartImageChanged(true);
                    carticon.setImageResource(R.drawable.ic_shopping_cartwhite);
                    Toast.makeText(ProductDetailsActivity.this, "added to Cart", Toast.LENGTH_SHORT).show();

                    saveGeofence(sample);
                } else {
                    int indexof_deleted_element = controller.deleteCartProducts(sample.getId(), sample.getType(), sample);
                    sample.setCartImageChanged(false);
                    carticon.setBackground(ContextCompat.getDrawable(ProductDetailsActivity.this, R.drawable.btn_cart_bg));
                    carticon.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
                    removeSavedGeofences(sample);
                    Toast.makeText(ProductDetailsActivity.this, "removed from Cart", Toast.LENGTH_SHORT).show();
                }
                MainActivity.setupMessagesBadge(controller.getcartProductsArraylistsize());

            }
        });  compareicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = getSharedPreferences("COMPARE", MODE_PRIVATE);
                if (!controller.CheckProductInCompare(sample.getId(), sample.getType())) {
                    int indexof_added_element = controller.getcompareProductsArraylistsize();
                    controller.setCompareOfProducts(sample.getId(), sample.getType());
                    (controller.getCompare()).setProducts(sample);
                    sample.setCompareImageChanged(true);
                    compareicon.setImageResource(R.drawable.ic_compare_red_24dp);
                    Toast.makeText(ProductDetailsActivity.this, "added to Compare", Toast.LENGTH_SHORT).show();

                    saveGeofence(sample);
                } else {
                    int indexof_deleted_element = controller.deleteCompareProducts(sample.getId(), sample.getType(), sample);
                    sample.setCompareImageChanged(false);
//                    compareicon.setBackground(ContextCompat.getDrawable(ProductDetailsActivity.this, R.drawable.btn_cart_bg));
                    compareicon.setImageResource(R.drawable.ic_compare_black_24dp);
                    removeSavedGeofences(sample);
                    Toast.makeText(ProductDetailsActivity.this, "removed from Compare", Toast.LENGTH_SHORT).show();
                }
                MainActivity.setupMessagesBadgeCompare(controller.getcompareProductsArraylistsize());

            }
        });
//        compareIcon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!controller.checkCompareInProducts(sample.getId())) {
//                    controller.setCompareProducts(sample.getId());
//                    (controller.getCompareList()).setProducts(sample);
//                    compareIcon.setImageResource(R.drawable.ic_compare);
//
//                } else {
//                    (controller.getCompareList()).deleteProducts(sample);
//                    controller.deleteCompareProducts(sample.getId());
//                    compareIcon.setImageResource(R.drawable.ic_compare_black_24dp);
//                }
//            }
//
//        });

        favoritIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.controller.isNetworkAvailable()) {
                    if (flag) {
                        favoritIcon.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        flag = false;
//					sample.setFavoritImageChanged(false);
                    } else {
                        favoritIcon.setImageResource(R.drawable.ic_favorite);
                        flag = true;
//					sample.setFavoritImageChanged(true);
                    }
                    favoritshandler.removeCallbacks(onFavoritsClicked);
                    onFavoritsClicked = new Runnable() {
                        @Override
                        public void run() {
                            if (flag) {
                                onRunFavoritsClicked(sample, 1);
                            } else {
                                onRunFavoritsClicked(sample, 0);
                            }
                        }
                    };

                    favoritshandler.postDelayed(onFavoritsClicked, 2000);
                } else {

                    snackbar.show();
                }

            }


        });
    }

    private void saveGeofence(Product sample) {

        gson = new Gson();
        String json = gson.toJson(sample);
        SharedPreferences.Editor editor = session.edit();
        editor.putString(sample.getId().toString() + sample.getType().toString() + "", json);
        editor.apply();
    }

    private void removeSavedGeofences(Product sample) {

        SharedPreferences.Editor editor = session.edit();
        editor.remove(sample.getId().toString() + sample.getType().toString() + "");
        editor.apply();
    }

    public void onRunFavoritsClicked(Product sample, int status) {
        if (controller.getprefrancesUserApiToken() == (null)) {

            if (!controller.CheckProductInFavorites(sample.getId(), sample.getType())) {
                controller.setFavoriteProducts(sample.getId(), sample.getType());
            } else {
                controller.deletefavoriteProducts(sample.getId(), sample.getType());

            }
        } else {

            setfavorits(sample.getType(), sample.getId(), status, sample);

        }
    }

    private void setfavorits(int type, int prduct_id, int status, final Product sample) {

        callfavoriteService(type, prduct_id, status).enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {

                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        sample.setFavoritImageChanged(true);
                        if (!ProductDetailsActivity.this.isDestroyed()) {
                            Toast.makeText(ProductDetailsActivity.this, "Added To Favorite List", Toast.LENGTH_SHORT).show();
                        }
                    } else if (response.body().getStatus() == 300) {
                        sample.setFavoritImageChanged(false);
                        if (!ProductDetailsActivity.this.isDestroyed()) {
                            Toast.makeText(ProductDetailsActivity.this, "Removed From Favorite List", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (sample.isfavoritImageChanged()) {
//							sample.setFavoritImageChanged(false);
                            favoritIcon.setImageResource(R.drawable.ic_favorite);
                        } else {
//							sample.setFavoritImageChanged(true);
                            favoritIcon.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        }
                        if (!ProductDetailsActivity.this.isDestroyed()) {
                            Toast.makeText(ProductDetailsActivity.this, "another Status", Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {
                    int statusCode = response.code();
                    if (sample.isfavoritImageChanged()) {
//						sample.setFavoritImageChanged(false);
                        favoritIcon.setImageResource(R.drawable.ic_favorite);
                    } else {
//						sample.setFavoritImageChanged(true);
                        favoritIcon.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    }
                    if (!ProductDetailsActivity.this.isDestroyed()) {
                        Toast.makeText(ProductDetailsActivity.this, "there is error in connection", Toast.LENGTH_SHORT).show();
                    }
                    // handle request errors depending on status code
                }


            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                if (sample.isfavoritImageChanged()) {
//					sample.setFavoritImageChanged(false);
                    favoritIcon.setImageResource(R.drawable.ic_favorite);
                } else {
//					sample.setFavoritImageChanged(true);
                    favoritIcon.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                }
                Toast.makeText(ProductDetailsActivity.this, "there is error in connection", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private Call<SimpleResponse> callfavoriteService(int type, int productid, int status) {
        switch (type) {
            case 1:
                return mService.getfavoratePruduct(productid, controller.getprefrancesUserApiToken(), status);
            case 2:
                return mService.favorateaccessory(productid, controller.getprefrancesUserApiToken(), status);
            case 3:
                return mService.favorateOtherProduct(productid, controller.getprefrancesUserApiToken(), status);
        }
        return null;
    }

    private Call<GalleryAnswerRespone> callProductDetailsService(int type, int product_id) {
        SharedPreferences session = ProductDetailsActivity.this.getSharedPreferences("Session", MODE_PRIVATE);
        String lang =LocaleHelper.getLanguage(ProductDetailsActivity.this);
        if (lang == null) {
            lang = LocaleHelper.getLanguage(ProductDetailsActivity.this);
        }
        switch (type) {
            case 1:
                return mService.getProductDetails("/api/getProductDetails", product_id, lang);
            case 2:
                return mService.getAccessoryDetails(product_id, lang);
            case 3:
                return mService.getOtherProductDetails(product_id, lang);
        }
        return null;
    }

    private void openAddressDialog(String imagepath) {
        int width;
        int height;
        ImageDialoge policyTermsDialog = new ImageDialoge(ProductDetailsActivity.this, imagepath);
        policyTermsDialog.show();
        policyTermsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (Build.VERSION.SDK_INT > 8) {
            width = manager.getDefaultDisplay().getWidth();
            height = manager.getDefaultDisplay().getHeight() - 60;
        } else {
            Point point = new Point();
            manager.getDefaultDisplay().getSize(point);
            width = point.x;
            height = point.y - 60;
        }
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(policyTermsDialog.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;
        policyTermsDialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header:
                openAddressDialog(sample.getImage());
                break;
        }
    }
}
