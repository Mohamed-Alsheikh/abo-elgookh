package com.abouelgoukh.sporteye.Activities.SMS;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.TextUtils;

import javax.inject.Inject;

import com.abouelgoukh.sporteye.R;

public class SMSPresenter implements SMSContract.Presenter {
    SMSContract.View view;
    Context context;
    SMSModel model;
    CountDownTimer countDownTimer;
    @Inject
    public SMSPresenter(SMSContract.View view, Context context) {
        this.view = view;
        this.context = context;

    }

    @Override
    public void startDownCounter() {
         countDownTimer= new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {
                view.changeWaitingCounterText(millisUntilFinished / 1000 + context.getResources().getString(R.string.Waiting_for_activation_code));
            }

            public void onFinish() {
                doWhenFinish_OR_Error();
            }
        }.start();
    }

    @Override
    public void doWhenFinish_OR_Error() {
        countDownTimer.cancel();
        view.changeWaitingCounterText("click for SMS Verification");
        view.changeArrowNavVisibility(true);

    }

    @Override
    public void sendSMS(String userName) {

        view.changeArrowNavVisibility(false);
        model.sendSMS(userName);

    }

    @Override
    public void ValidateVerificationCode(String verificationCode) {
        if (isVerificationCodeValide(verificationCode)) {
            model.activateAcount(verificationCode);
        } else {
            view.showToast("Invalid Verification Code");

        }
    }

    @Override
    public boolean isVerificationCodeValide(String verificationCode) {
        if (verificationCode.trim().length() == 0 || verificationCode == null || TextUtils.equals(verificationCode, "null") || TextUtils.isEmpty(verificationCode)) {
            return false;
        } else {
            return true;
        }
    }
}
