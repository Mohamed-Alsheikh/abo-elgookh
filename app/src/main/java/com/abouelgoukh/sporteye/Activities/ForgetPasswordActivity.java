package com.abouelgoukh.sporteye.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.abouelgoukh.sporteye.Activities.SMS.SMSVerificationActivity;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.login_registration.Register;
import com.abouelgoukh.sporteye.utils.Validation;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity {
    EditText txt_email;
    Button resetPassword;
    ProgressDialog progressDialog;
    ApiServiceInterface serviceInterface;
    Call<SimpleResponse> RegisterService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(ForgetPasswordActivity.this));
        setContentView(R.layout.activity_forget_password);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setDisplayShowHomeEnabled(true);

            if (LocaleHelper.getLanguage(ForgetPasswordActivity.this).equals("en")) {//TODO change it to get from sitting
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            } else if (LocaleHelper.getLanguage(ForgetPasswordActivity.this).equals("ar")) {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_arrow_forward_24);

            }
            getSupportActionBar().setTitle(R.string.forgetPassword);
        }

        txt_email =  findViewById(R.id.txt_email);
        resetPassword =  findViewById(R.id.resetPassword);

        serviceInterface = ApiUtils.getRetrofitObject();
        progressDialog = new ProgressDialog(ForgetPasswordActivity.this);
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (RegisterService != null && RegisterService.isExecuted()) {
                    RegisterService.cancel();
                    Toast.makeText(ForgetPasswordActivity.this, "Reset password cancelled", Toast.LENGTH_SHORT).show();

                }
            }
        });

        resetPassword.setOnClickListener(v -> {
            String var_email = txt_email.getText().toString();
            if (!Validation.validate_Email(var_email)) {
                txt_email.setError("Invalid Email ");
                return;
            }
            resetPassword(var_email);
        });
    }

    private void resetPassword(String var_email) {
        progressDialog.show();
        RegisterService = callResetPasswordService(var_email);
        RegisterService.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    switch (response.body().getStatus()) {
                        case 200:
                            Toast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.reset_link_sent_to_your_Email), Toast.LENGTH_LONG).show();
                          finish();
                            break;
                        case 402:
                            //email is missing
                            Toast.makeText(ForgetPasswordActivity.this, " email is required  ", Toast.LENGTH_SHORT).show();
                            break;
                        case 405:
                            //email / user not exist
                            Toast.makeText(ForgetPasswordActivity.this, "the email is not exists sign in please", Toast.LENGTH_SHORT).show();
                            break;
                    }
                } else {
                    Toast.makeText(ForgetPasswordActivity.this, "response failed", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                Toast.makeText(ForgetPasswordActivity.this, "response failed", Toast.LENGTH_SHORT).show();
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private Call<SimpleResponse> callResetPasswordService(String var_email) {
        return serviceInterface.forgetPassword(var_email);

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}