package com.abouelgoukh.sporteye.Activities;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.readystatesoftware.viewbadger.BadgeView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pub.devrel.easypermissions.EasyPermissions;
import com.abouelgoukh.sporteye.Activities.dummy.DummyContent;
import com.abouelgoukh.sporteye.Fragments.BrandProductsFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.AboutUsFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.AccessoriesFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.BrandsFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.ContactUsFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.FavoritesFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.HomeFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MaintanceFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MassageFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MyOrdersFragment;
import com.abouelgoukh.sporteye.Fragments.NavBarFragments.MyProfileFragment;
import com.abouelgoukh.sporteye.Fragments.ProductDetailFragment;
import com.abouelgoukh.sporteye.Fragments.SharedProductsFragment;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.model.ModelCart;
import com.abouelgoukh.sporteye.model.ModelCompare;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.BrandModel.Brand;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;

public class MainActivity extends AppCompatActivity implements SharedProductsFragment.OnFragmentInteractionListener,
        BrandsFragment.OnFragmentInteractionListener, AccessoriesFragment.OnFragmentInteractionListener,
        FavoritesFragment.OnFragmentInteractionListener, MyOrdersFragment.OnFragmentInteractionListener,
        ContactUsFragment.OnFragmentInteractionListener, HomeFragment.OnFragmentInteractionListener,
        BrandProductsFragment.OnFragmentInteractionListener, MassageFragment.OnFragmentInteractionListener, SwipeRefreshLayout.OnRefreshListener {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgProfile;
    static Button loginAndlogout;
    private TextView txtName;
    static TextView txtWebsite;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    Bundle Brands_arguments = new Bundle();
    static MenuItem logout;
    private Gson gson;
    private static final String TAG_HOME = "Home";
    private static final String TAG_BICYCLES_BRANDS = "Bicycles";
    private static final String TAG_ACCESSORIES_BRANDS = "Accessories";
    private static final String TAG_OTHER_PRODUCTS = "Products Categories";
    private static final String TAG_MAINTENANCE = "Maintenance";
    private static final String TAG_FAVORITES = "Favorites";
    private static final String TAG_MYORDERS = "My Orders";
    private static final String TAG_CONTACTUS = "Contact Us";
    private static final String TAG_PROFILE = "Profile";
    private static final String TAG_PRODUCT_DETAILS = "Product Details";
    private static final String TAG_Brand_Products = "Brand Product";
    private static final String TAG_ProductsCatogery = "Products Catogery";
    private static final String TAG_SEARCH = "Search";
    private static final String TAG_MESSAGE = "Message";
    private static final String TAG_RESET_PASSWORD = "Reset Password";
    private static final String TAG_ABOUT_US = "about us";
    public static int brandid;
    public static ArrayList<Brand> brandsalbumlist;
    public static String brand_name;
    public static String Brand_type;
    public static String CURRENT_TAG = "";
    public static int navItemIndex = 0;

    public static String APIToken = "";
    SharedPreferences session;
    MenuItem English;
    MenuItem Arabic;
    String flag = "VISIBLE";
    String[] lstSource;
    public static BadgeView messageCenterBadge;
    public static BadgeView messageCenterBadgeCompare;
    ImageView imgMessagesIcon;
    ImageView imgMessagesIconCompare;
    //    SwipeRefreshLayout swipeLayout;
    public static AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    public static FrameLayout frameLayout;
    FrameLayout MainFram;
    FrameLayout imageview;
    FragmentTransaction fragmenttransaction;
    // toolbar titles respected to selected nav menu item
    public static String[] activityTitles;
    ActionBar actionBar;
    // flag to load home Currentfragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = false;
    private Handler mHandler = new Handler();
    Fragment Currentfragment;
    MaterialSearchView searchView;
    FragmentManager manager;
    List<Integer> proId = new ArrayList<>();
    List<Integer> proType = new ArrayList<>();
    public static FloatingActionButton filter;
    public static Controller controller;

    public static SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      LocaleHelper.updateResources(this, LocaleHelper.getLanguage(MainActivity.this));
        setContentView(R.layout.activity_main);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        frameLayout = (FrameLayout) findViewById(R.id.slidecontainer);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbarlayout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            private State state;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    if (state != State.EXPANDED) {
                        swipeRefreshLayout.setEnabled(true);
                    }
                    state = State.EXPANDED;
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    if (state != State.COLLAPSED) {
                        swipeRefreshLayout.setEnabled(false);
                    }
                    state = State.COLLAPSED;
                } else {
                    if (state != State.IDLE) {
//                        swipeRefreshLayout.setEnabled(true);

                    }
                    state = State.IDLE;
                }
            }
        });
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitleEnabled(false);

        filter = (FloatingActionButton) findViewById(R.id.filter);
        controller = (Controller) MainActivity.this.getApplicationContext();




        loadCart();
        loadCompare();
        loadFavorits();
        SharedPreferences session = MainActivity.this.getSharedPreferences("Session", Context.MODE_PRIVATE);
        APIToken = session.getString("APITOKEN", "");
        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // load toolbar titles from string resources
        manager = getSupportFragmentManager();
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);


            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navHeader = navigationView.getHeaderView(0);
            txtName = (TextView) navHeader.findViewById(R.id.name);
            txtWebsite = (TextView) navHeader.findViewById(R.id.email);
            imgProfile = (ImageView) navHeader.findViewById(R.id.profileimg);
            // load nav menu header data
            loadNavHeader();
            // initializing navigation menu
            setUpNavigationView();


        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadRequiredFragment();
        }

        //Load Search Action
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        session = getApplicationContext().getSharedPreferences("Search", MODE_PRIVATE);
        Set<String> set = new HashSet<String>();
        set = session.getStringSet("searchQuery", set);
        lstSource = set.toArray(new String[set.size()]);
        searchaction();
        actionBar = getSupportActionBar();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);

//        View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_titletext_layout, null);
//        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
//                ActionBar.LayoutParams.WRAP_CONTENT,
//                ActionBar.LayoutParams.MATCH_PARENT,
//                Gravity.CENTER);


        View target = findViewById(R.id.cartmenuitem);


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras()!=null&&intent.getExtras().getBoolean("openOrders") ){
            frameLayout.setVisibility(View.GONE);
            appBarLayout.setExpanded(false);
            navItemIndex = 6;
            CURRENT_TAG = TAG_MYORDERS;
            loadRequiredFragment();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            float per = Math.abs(appBarLayout.getY()) / appBarLayout.getTotalScrollRange();
            boolean setExpanded = (per <= 0.5F);
            appBarLayout.setExpanded(setExpanded, true);
        }
        return super.dispatchTouchEvent(event);
    }

    private enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void loadCart() {
        // Loop over all geofence keys in prefs and add to namedGeofences

        controller.myCart = new ModelCart();
        controller.cartOfProductsTypes = new ArrayList<>();
        controller.cartOfProductsID = new ArrayList<>();

        session = getSharedPreferences("Cart", MODE_PRIVATE);
        gson = new Gson();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            Product product = gson.fromJson(jsonString, Product.class);
            if (product != null) {
                controller.setCartOfProducts(product.getId(), product.getType());
                controller.getCart().setProducts(product);
                setupMessagesBadge(controller.getcartProductsArraylistsize());
            }
        }
    }

    private void loadCompare() {
        // Loop over all geofence keys in prefs and add to namedGeofences

        controller.modelCompare = new ModelCompare();
        controller.compareOfProductsTypes = new ArrayList<>();
        controller.compareOfProductsID = new ArrayList<>();

        session = getSharedPreferences("COMPARE", MODE_PRIVATE);
        gson = new Gson();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            Product product = gson.fromJson(jsonString, Product.class);
            if (product != null) {
                controller.setCompareOfProducts(product.getId(), product.getType());
                controller.getCompare().setProducts(product);
                setupMessagesBadgeCompare(controller.getcompareProductsArraylistsize());
            }
        }
    }

    private void loadFavorits() {
        // Loop over all geofence keys in prefs and add to namedGeofences
        session = getSharedPreferences("Favorits", MODE_PRIVATE);
        gson = new Gson();
        Map<String, ?> keys = session.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            String jsonString = session.getString(entry.getKey(), null);
            Product product = gson.fromJson(jsonString, Product.class);
            if (product != null) {
                controller.setFavoriteProducts(product.getId(), product.getType());
                controller.getFavorite().setFavoritsProducts(product);
            }
        }

    }

    @SuppressLint("RestrictedApi")
    private void searchaction() {
//        searchView.setSuggestions(lstSource);
//        searchView.setSuggestionBackground(AppCompatDrawableManager.get().getDrawable(MainActivity.this, R.color.textbuttoncolorwhite));
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

                searchView.showSuggestions();
                findViewById(R.id.frame).setVisibility(View.INVISIBLE);
                appBarLayout.setExpanded(true);
                frameLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);

                flag = "INVISIBLE";
            }

            @Override
            public void onSearchViewClosed() {

                //If closed Search View , lstView will return default
                findViewById(R.id.frame).setVisibility(View.VISIBLE);
                if (CURRENT_TAG.equals(TAG_HOME)) {
                    appBarLayout.setExpanded(true);
                    frameLayout.setVisibility(View.VISIBLE);
                }

                flag = "VISIBLE";
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


//                session = getApplicationContext().getSharedPreferences("Search", MODE_PRIVATE);
//                Set<String> set = new HashSet<String>();
//                set = session.getStringSet("searchQuery", set);
//                set.add(query);
//                Toast.makeText(MainActivity.this, "size " + set.size(), Toast.LENGTH_LONG).show();
//                SharedPreferences.Editor sessionEditor = session.edit();
//                sessionEditor.clear().commit();
//                sessionEditor.putStringSet("searchQuery", set);
//                sessionEditor.apply();
//                sessionEditor.commit();
//                lstSource = set.toArray(new String[set.size()]);
//                searchView.setSuggestions(lstSource);

                navItemIndex = 12;
                CURRENT_TAG = TAG_SEARCH;
                Bundle arguments = new Bundle();
                arguments.putString("target", "SearchResult");
                arguments.putString("searchToken", query);
                Fragment sharedProductsFragmentFragment = new SharedProductsFragment();
                sharedProductsFragmentFragment.setArguments(arguments);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.addToBackStack(CURRENT_TAG);
                fragmentTransaction.replace(R.id.frame, sharedProductsFragmentFragment, CURRENT_TAG).commit();
                setToolbarTitle();
                frameLayout.setVisibility(View.GONE);
                appBarLayout.setExpanded(false);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText != null && !newText.isEmpty()) {
                    List<String> lstFound = new ArrayList<String>();
                    for (String item : lstSource) {
                        if (item.contains(newText))
                            lstFound.add(item);
                    }


                } else {
                    //if search text is null
                    //return default

                }
                return true;
            }

        });


    }


    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     *
     */
    private void loadNavHeader() {
        // name, Email
        session = getSharedPreferences("Session", MODE_PRIVATE);
        if ((session.getString("APITOKEN", null)) != null) {
            txtWebsite.setText(session.getString("UserName", null));

        } else  {

        }


        txtName.setText(getResources().getString(R.string.Welcome));
//        imgProfile.setImageResource(R.drawable.ic_account_circle_black_24dp);
//        Glide.with(getApplicationContext()).load(R.drawable.mohamed).apply(RequestOptions.circleCropTransform()).into(imgProfile);

        txtWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = getSharedPreferences("Session", MODE_PRIVATE);

                if ((session.getString("APITOKEN", null)) == null) {
                    Intent login = new Intent(MainActivity.this, LoginActivity.class);
                    Bundle bundlanimation =
                            ActivityOptions.makeCustomAnimation(getApplicationContext(),
                                    R.anim.login_animation, R.anim.login_animation2).toBundle();
                    startActivity(login, bundlanimation);
                } else {
                    appBarLayout.setExpanded(false);
                    frameLayout.setVisibility(View.GONE);
                    txtWebsite.setText(session.getString("UserName", null));
                    navItemIndex = 8;
                    CURRENT_TAG = TAG_PROFILE;
                    loadRequiredFragment();
                }

            }
        });
    }

    private void addContent(final String tag, final int content_view_id, final Fragment fragment) {
        Runnable mPendingRunnable = new Runnable() {
            public void run() {
                if (!MainActivity.this.manager.popBackStackImmediate(tag, 0) && MainActivity.this.manager.findFragmentByTag(tag) == null) {
                    MainActivity.this.fragmenttransaction = MainActivity.this.manager.beginTransaction();
                    MainActivity.this.fragmenttransaction.addToBackStack(tag);
                    MainActivity.this.fragmenttransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
                    MainActivity.this.fragmenttransaction.replace(content_view_id, fragment, tag);
                    MainActivity.this.fragmenttransaction.commit();
                }
            }
        };
        new Handler().postDelayed(mPendingRunnable, 400);
    }

    public void switchContent(int position, Product product) {
        appBarLayout.setExpanded(false);
        frameLayout.setVisibility(View.GONE);
        navItemIndex = 9;
        CURRENT_TAG = TAG_PRODUCT_DETAILS;
        Bundle arguments = new Bundle();
        Fragment fragment = null;
        Log.d("position adapter", "" + position);
        arguments.putParcelable("singleProduct", product);
        // Start a new Currentfragment
        fragment = new ProductDetailFragment();
        fragment.setArguments(arguments);



//            transaction.replace(R.id.frame, fragment, CURRENT_TAG);
            addContent(CURRENT_TAG, R.id.frame, fragment);


        setToolbarTitle();
    }

    public void switchContent(boolean castit, ArrayList<Brand> brandsalbumlist, int brandid, String brand_name, String Brand_type, Fragment fragment) {
        appBarLayout.setExpanded(false);
        frameLayout.setVisibility(View.GONE);
        MainActivity.brand_name = brand_name;
        if (brandsalbumlist != null) {
            MainActivity.brandsalbumlist = new ArrayList<>();
            MainActivity.brandsalbumlist.addAll(brandsalbumlist);
        }
        if (castit) {
            ((BrandProductsFragment) fragment).mainActivity = this;
        }

        if (Brand_type.equals("OtherProductsSubCatogery")) {
            navItemIndex = 11;
            CURRENT_TAG = brand_name;
        } else {
            navItemIndex = 10;
            CURRENT_TAG = brand_name;
        }

        Bundle arguments = new Bundle();

        MainActivity.brandid = brandid;
        MainActivity.Brand_type = Brand_type;
        arguments.putInt("brandid", MainActivity.brandid);
        arguments.putString("Brand_type", MainActivity.Brand_type);
//        arguments.putParcelableArrayList(brandsalbumlist);
        // Start a new Currentfragment

        fragment.setArguments(arguments);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();


//            transaction.replace(R.id.frame, fragment, CURRENT_TAG);
            addContent(CURRENT_TAG, R.id.frame, fragment);

//        transaction.addToBackStack(CURRENT_TAG);
//        transaction.commit();
        getSupportActionBar().setTitle(brand_name);
//        setToolbarTitle();
    }

    public void deleteContentAddSlideShow(Fragment fragment) {

        FragmentTransaction fragmenttransaction = manager.beginTransaction();
        fragmenttransaction.remove(fragment);
        fragmenttransaction.commit();

    }

    public void switchContentAddSlideShow(Fragment fragment, int size) {
        FragmentTransaction fragmenttransaction = manager.beginTransaction();
        fragmenttransaction.replace(R.id.slidecontainer, fragment, "slidecontainer");
        fragmenttransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmenttransaction.commit();


        if (CURRENT_TAG.equals(TAG_HOME)) {
            filter.setVisibility(View.INVISIBLE);

            if (size > 0) {
                frameLayout.setVisibility(View.VISIBLE);
                appBarLayout.setExpanded(true);
            } else {
                frameLayout.setVisibility(View.GONE);
                appBarLayout.setExpanded(false);
            }
        } else {
            frameLayout.setVisibility(View.VISIBLE);
            appBarLayout.setExpanded(false);
            filter.setVisibility(View.VISIBLE);
        }

    }


    public void switchContent(Fragment fragment, int ItemIndex) {
        appBarLayout.setExpanded(false);
        frameLayout.setVisibility(View.GONE);
        navItemIndex = ItemIndex;
        if (navItemIndex == 13) {
            CURRENT_TAG = TAG_MESSAGE;
        } else {
            CURRENT_TAG = TAG_RESET_PASSWORD;
        }


//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

//            transaction.replace(R.id.frame, fragment, CURRENT_TAG);
            addContent(CURRENT_TAG, R.id.frame, fragment);


//        transaction.addToBackStack(CURRENT_TAG);
//
//        transaction.commit();
        setToolbarTitle();

    }

    /***
     * Returns respected Currentfragment that user
     * selected from navigation menu
     */
    private void validateScreen() {
        // selecting appropriate nav menu item

        // set toolbar product_name

        //Closing drawer on item click
        drawer.closeDrawers();
        // refresh toolbar menu
        invalidateOptionsMenu();
    }


    public void loadRequiredFragment() {

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
//        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
//            if (!manager.getBackStackEntryAt(manager.getBackStackEntryCount() - 1).getName().equals(CURRENT_TAG)) {
//                Currentfragment = getSupportFragmentManager().findFragmentByTag(CURRENT_TAG);
//                if (CURRENT_TAG.equals("Home")) {
//                    Currentfragment.onDetach();
//                }
//                replaceFragment(Currentfragment);
//            }
//        } else {
//            Runnable mPendingRunnable = new Runnable() {
//                @Override
//                public void run() {
        // update the main content by replacing fragments

        Currentfragment = getRequiredFragment();


//                    if (CURRENT_TAG.equals("Home")) {
//                        Currentfragment.onDetach();
//                    }
//                    replaceFragment(Currentfragment);
//                }
//            };
        // If mPendingRunnable is not null, then add to the message queue
//            if (mPendingRunnable != null) {
//                mHandler.post(mPendingRunnable);
//            }

            addContent(activityTitles[navItemIndex], R.id.frame, Currentfragment);
            selectNavMenu();
            validateScreen();

//        }

        setToolbarTitle();
    }

    private void replaceFragment(Fragment fragment) {

            fragmenttransaction = manager.beginTransaction();
            fragmenttransaction.replace(R.id.frame, fragment, CURRENT_TAG);
            fragmenttransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmenttransaction.addToBackStack(CURRENT_TAG);
            fragmenttransaction.commit();

    }

    private Fragment getRequiredFragment() {

        switch (navItemIndex) {
            case 0:
                // home
                return new HomeFragment();
            case 1:
                // Brands
                BrandsFragment BicyclesBrandsFragment = new BrandsFragment();
                Brands_arguments.putString("Brand_type", "Bicycles Brands");
                BicyclesBrandsFragment.setArguments(Brands_arguments);
                appBarLayout.setExpanded(false);
                return BicyclesBrandsFragment;

            case 2:
                BrandsFragment AccessoriesBrandsFragment = new BrandsFragment();
                Brands_arguments.putString("Brand_type", "Accessories Brands");
                AccessoriesBrandsFragment.setArguments(Brands_arguments);
                return AccessoriesBrandsFragment;

            case 3:
                BrandsFragment AccessoriesBrandsFragment2 = new BrandsFragment();
                Brands_arguments.putString("Brand_type", "OtherProductsCatogery");
                AccessoriesBrandsFragment2.setArguments(Brands_arguments);
                return AccessoriesBrandsFragment2;

            case 4:
                MaintanceFragment maintanceFragment = new MaintanceFragment();
                return maintanceFragment;

            case 5:
                FavoritesFragment favoritesFragment = new FavoritesFragment();
                return favoritesFragment;

            case 6:
                MyOrdersFragment myOrdersFragment = new MyOrdersFragment();
                return myOrdersFragment;

            case 7:
                ContactUsFragment contactUsFragment = new ContactUsFragment();
                return contactUsFragment;

            case 8:
                MyProfileFragment myProfileFragment = new MyProfileFragment();
                return myProfileFragment;

            case 9:
                AboutUsFragment aboutUsFragment = new AboutUsFragment();
                return aboutUsFragment;

            default:
                return new SharedProductsFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        collapsingToolbarLayout.setTitle(activityTitles[navItemIndex]);

    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                appBarLayout.setExpanded(false);
                frameLayout.setVisibility(View.GONE);
                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        appBarLayout.setExpanded(true);
                        frameLayout.setVisibility(View.VISIBLE);
                        filter.setVisibility(View.INVISIBLE);

                        break;
                    case R.id.nav_brands:
                        CURRENT_TAG = TAG_BICYCLES_BRANDS;
                        navItemIndex = 1;
                        appBarLayout.setExpanded(false);
                        break;
                    case R.id.nav_accessories:
                        CURRENT_TAG = TAG_ACCESSORIES_BRANDS;
                        navItemIndex = 2;
                        break;
                    case R.id.nav_other_product:
                        CURRENT_TAG = TAG_OTHER_PRODUCTS;
                        navItemIndex = 3;

                        break;
                    case R.id.nav_maintenanc:
                        CURRENT_TAG = TAG_MAINTENANCE;
                        navItemIndex = 4;
                        break;
                    case R.id.nav_Favorites:
                        CURRENT_TAG = TAG_FAVORITES;
                        navItemIndex = 5;
                        break;
                    case R.id.nav_MyOrders:
                        CURRENT_TAG = TAG_MYORDERS;
                        navItemIndex = 6;
                        break;
                    case R.id.nav_ContactUs:
                        CURRENT_TAG = TAG_CONTACTUS;
                        navItemIndex = 7;
                        break;
                    case R.id.nav_profile:
                        if (!MainActivity.APIToken.equals("")) {
                            CURRENT_TAG = TAG_PROFILE;
                            navItemIndex = 8;
                        } else {
                            Intent login = new Intent(MainActivity.this, LoginActivity.class);
                            Bundle bundlanimation =
                                    ActivityOptions.makeCustomAnimation(getApplicationContext(),
                                            R.anim.login_animation, R.anim.login_animation2).toBundle();
                            startActivity(login, bundlanimation);
                            return true;
                        }

                        break;
                    case R.id.nav_about_us:
                        CURRENT_TAG = TAG_ABOUT_US;
                        navItemIndex = 9;
                        break;
                    default:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                }

                loadRequiredFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

        Drawable drawable = getResources().getDrawable(R.drawable.ic_apps_black_24dp);

        actionBarDrawerToggle.setHomeAsUpIndicator(drawable);
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        int x = 1;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }
        if (flag.equals("INVISIBLE")) {
            searchView.closeSearch();
            findViewById(R.id.frame).setVisibility(View.VISIBLE);
            flag = "VISIBLE";
            return;
        }


        if (manager.getBackStackEntryAt(manager.getBackStackEntryCount() - 1).getName().equals("Home")|| manager.getBackStackEntryAt(manager.getBackStackEntryCount() - 1).getName().equals("الرئيسية")) {
            if (!drawer.isDrawerOpen(GravityCompat.START)) {
                showalert();
                return;
            }

        } else {
            getSupportActionBar().setTitle(manager.getBackStackEntryAt(manager.getBackStackEntryCount() - 2).getName());
//                selectNavMenu();
            CURRENT_TAG = manager.getBackStackEntryAt(manager.getBackStackEntryCount() - 2).getName();

            if (CURRENT_TAG == null) {
                CURRENT_TAG = TAG_HOME;
            }
            if (CURRENT_TAG.equals(TAG_HOME) || CURRENT_TAG.equals(TAG_Brand_Products)) {
                frameLayout.setVisibility(View.VISIBLE);
                appBarLayout.setExpanded(true);
            } else {
                frameLayout.setVisibility(View.GONE);
                appBarLayout.setExpanded(false);
            }

        }

        super.onBackPressed();
    }

    private void showalert() {

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);


        adb.setTitle("Do you realy want to exit?");

        //adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();

            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        logout = menu.findItem(R.id.Logout);

        SharedPreferences session = getSharedPreferences("Session", MODE_PRIVATE);
        if ((session.getString("APITOKEN", null)) == null) {
            logout.setTitle(getResources().getString(R.string.login));
            logout.setIcon(getResources().getDrawable(R.drawable.login));
        }

        MenuItem item = menu.findItem(R.id.action_search);
        MenuItem cart = menu.findItem(R.id.cartmenuitem);
        MenuItem compare = menu.findItem(R.id.action_compare);
        MenuItem ChangeLang = menu.findItem(R.id.ChangeLang);

        English = menu.findItem(R.id.English);

        Arabic = menu.findItem(R.id.Arabic);
        if (LocaleHelper.getLanguage(MainActivity.this).equals("en")) {
            English.setChecked(true);
        } else if (LocaleHelper.getLanguage(MainActivity.this).equals("ar")) {
            Arabic.setChecked(true);

        }

        createViewFromMenuItem(cart);
        setupMessagesBadge(controller.getcartProductsArraylistsize());

        createViewFromMenuItemCompare(compare);
        setupMessagesBadgeCompare(controller.getcompareProductsArraylistsize());

        searchView.setMenuItem(item);

        return true;
    }

    private void createViewFromMenuItemCompare(MenuItem compare) {
        compare.setActionView(R.layout.common_messages_indicator_compare);

        if (compare.getActionView().findViewById(R.id.imgMessagesIconCompare) != null) {
            imgMessagesIconCompare = compare.getActionView().findViewById(R.id.imgMessagesIconCompare);

            imgMessagesIconCompare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, CartActivity.class);
                    intent.putExtra("Target", "COMPARE");
                    startActivity(intent);
                }
            });
            messageCenterBadgeCompare = new BadgeView(this, imgMessagesIconCompare);
            messageCenterBadgeCompare.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
            messageCenterBadgeCompare.setBadgeMargin(0);
            messageCenterBadgeCompare.setTextSize(9);
        }

        setupMessagesBadgeCompare(controller.getcompareProductsArraylistsize());

    }

    private void createViewFromMenuItem(MenuItem cart) {

        cart.setActionView(R.layout.common_messages_indicator);

        if (cart.getActionView().findViewById(R.id.imgMessagesIcon) != null) {
            imgMessagesIcon = ((ImageView) cart.getActionView().findViewById(R.id.imgMessagesIcon));

            imgMessagesIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MainActivity.this, CartActivity.class);
                    intent.putExtra("Target", "Cart");
                    startActivity(intent);
                }
            });
            messageCenterBadge = new BadgeView(this, imgMessagesIcon);
            messageCenterBadge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
            messageCenterBadge.setBadgeMargin(0);
            messageCenterBadge.setTextSize(9);
        }

        setupMessagesBadge(controller.getcartProductsArraylistsize());

    }

    public static void setupMessagesBadge(int badgeCnt) {

        if (messageCenterBadge != null && badgeCnt > 0) {
            messageCenterBadge.setText(String.valueOf(badgeCnt));
            messageCenterBadge.show();
        } else if (messageCenterBadge != null && badgeCnt == 0) {
            messageCenterBadge.hide();
        }

    }

    public static void setupMessagesBadgeCompare(int badgeCnt) {

        if (messageCenterBadgeCompare != null && badgeCnt > 0) {
            messageCenterBadgeCompare.setText(String.valueOf(badgeCnt));
            messageCenterBadgeCompare.show();
        } else if (messageCenterBadgeCompare != null && badgeCnt == 0) {
            messageCenterBadgeCompare.hide();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        int id = item.getItemId();


        if (id == R.id.cartmenuitem) {
//            Intent intent = new Intent(MainActivity.this, CartActivity.class);
//            intent.putExtra("Target", "Cart");
//            startActivity(intent);
//            return true;
        }

        if (id == R.id.Logout) {
            if (APIToken.equals("")) {
                Intent login = new Intent(MainActivity.this, LoginActivity.class);
                Bundle bundlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(),
                                R.anim.login_animation, R.anim.login_animation2).toBundle();
                startActivity(login, bundlanimation);
            } else {
                LogOutalert();
            }


        }
        if (id == R.id.English) {
            if (!English.isChecked()) {
                setImage("en");
                English.setChecked(true);


            }

        } else if (id == R.id.Arabic) {
            if (!Arabic.isChecked()) {
                setImage("ar");
                Arabic.setChecked(true);
            }

        }

        // user is in notifications Currentfragment
        // and selected 'Clear All'
//        if (id == R.id.item2) {
//            Toast.makeText(getApplicationContext(), "Clear all notifications!", Toast.LENGTH_LONG).show();
//        }

        return super.onOptionsItemSelected(item);
    }

    private void LogOutalert() {

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);


        adb.setTitle("Do you realy want to LogOut?");

        //adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Logged Out!", Toast.LENGTH_LONG).show();
                SharedPreferences session = getSharedPreferences("Session", MODE_PRIVATE);
                session.edit().clear().commit();
                txtWebsite.setText("LogIn/SignIn");
                APIToken = "";


                logout.setEnabled(false);

            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        adb.show();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(MainActivity.this));
        if (!APIToken.equals("") && logout != null) {
            logout.setTitle("Logout");
            logout.setIcon(getResources().getDrawable(R.drawable.logout));
        }
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawers();
//            return;
//        }
    }

    public void addItemToCartSession(
            String proId, int proIdValue
            , String proType, int proTypeValue
            , String proName, String proNameValue
            , String proImage, String proImageValue
            , String proPrice, int proPriceValue) {
        session = getSharedPreferences("Cart", Context.MODE_PRIVATE);
        SharedPreferences.Editor sessionEditor = session.edit();

        sessionEditor.putInt(proId, proIdValue);
        sessionEditor.putInt(proType, proTypeValue);
        sessionEditor.putString(proName, proNameValue);
        sessionEditor.putString(proImage, proImageValue);
        sessionEditor.putInt(proPrice, proPriceValue);
        sessionEditor.apply();
    }


    @Override
    public void onRefresh() {

        Fragment frg = null;
        frg = getSupportFragmentManager().findFragmentByTag(CURRENT_TAG);
        if (frg != null) {
            frg.onDetach();
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.detach(frg);
            ft.attach(frg);
            ft.commit();
            Handler handler = new Handler();
            Runnable swipdelay = new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            };
            handler.postDelayed(swipdelay, 2000);
        }

    }


    private void updateViews(String languageCode) {
        LocaleHelper.updateResources(this, languageCode);


        txtName.setText(getResources().getString(R.string.Welcome));
//        mDescTextView.setText(resources.getString(R.string.main_activity_desc));
//        mAboutTextView.setText(resources.getString(R.string.main_activity_about));
//        mToTRButton.setText(resources.getString(R.string.main_activity_to_tr_button));
//        mToENButton.setText(resources.getString(R.string.main_activity_to_en_button));

        finish();
        startActivity(getIntent());

    }

    private void setImage(String lang) {

        updateViews(lang);

    }


}

