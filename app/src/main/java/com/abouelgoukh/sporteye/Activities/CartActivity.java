package com.abouelgoukh.sporteye.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;

import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Deleget;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.adapter.CartAdapter;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.other.GridSpacingItemDecoration;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Government;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model.MyOrdersAnswerRespons;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.myoders_model.Order;

public class CartActivity extends AppCompatActivity implements View.OnClickListener, Deleget {
    RecyclerView cartreRecyclerView;
    CartAdapter cartAdapter;
    TextView txttotal, txtsubtotal, carttotalitems;
    Button btn_CHECKOUT;
    Snackbar snackbar;
    Double total = 0.0;
    String target = "";
    static Order orderdetails = null;
    List<Product> orderProducts = new ArrayList<>();
    private ApiServiceInterface mService = ApiUtils.getRetrofitObject();
    ProgressDialog progressDialog;
    CoordinatorLayout CoordinatorLayout_content;
    Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(CartActivity.this));
        setContentView(R.layout.activity_cart);

        CoordinatorLayout_content = (CoordinatorLayout) findViewById(R.id.CoordinatorLayout_content);
        snackbar = Snackbar.make(CoordinatorLayout_content, "No internet connection!", Snackbar.LENGTH_LONG).
                setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
        // Changing message text color
        snackbar.setActionTextColor(Color.RED);
        controller = (Controller) getApplicationContext();
        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        txttotal = (TextView) findViewById(R.id.carttotalprice);
        carttotalitems = (TextView) findViewById(R.id.carttotalitems);
        txttotal.setText(0 + "");
        carttotalitems.setText("(" + controller.getCart().getCartItems().size() + ")");
        btn_CHECKOUT = (Button) findViewById(R.id.btn_CHECKOUT);
        progressDialog = new ProgressDialog(CartActivity.this);
        progressDialog.setMessage("get Order Data...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Toolbar toolbar = (Toolbar) findViewById(R.id.carttoolbar);
        setSupportActionBar(toolbar);
        if (LocaleHelper.getLanguage(this).equals("en")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        } else if (LocaleHelper.getLanguage(this).equals("ar")) {
            toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_forward_24);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CartActivity.super.onBackPressed();
            }
        });

        toolbar.setTitle("My Cart");

        cartreRecyclerView = (RecyclerView) findViewById(R.id.cart_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        cartreRecyclerView.setLayoutManager(mLayoutManager);
        cartreRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(5), true));
        cartreRecyclerView.setItemAnimator(new DefaultItemAnimator());
        target = getIntent().getStringExtra("Target");
        if (target.equals("Order")) {
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    onBackPressed();
                }
            });
            btn_CHECKOUT.setText("More Details");
            getOrderDetails(getIntent().getIntExtra("OrderId", 0));
        }
        if (target.equals("Cart")) {
            prepareToShowCart(target);
        }
        if (target.equals("COMPARE")) {
            btn_CHECKOUT.setVisibility(View.GONE);
            prepareToShowCart(target);
        }
        btn_CHECKOUT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (target.equals("Cart")) {
                    if (MainActivity.controller.isNetworkAvailable()) {
                        checkOut();
                    } else {

                        snackbar.show();
                    }
                }
                if (target.equals("Order")) {
                    orderDetails();
                }

            }
        });


    }

    private void checkOut() {
        SharedPreferences session = getSharedPreferences("Session", MODE_PRIVATE);
        Intent checkOut;
        if ((session.getString("APITOKEN", null)) == null) {
            checkOut = new Intent(CartActivity.this, LoginActivity.class);
            startActivity(checkOut);
        } else if ((session.getString("APITOKEN", null)) != null && Double.parseDouble(txttotal.getText().toString()) > 0 && controller.getCart().getCartItems().size() > 0) {
            checkOut = new Intent(CartActivity.this, CheckOutActivity.class);
            checkOut.putExtra("subtotal", txttotal.getText().toString());
            checkOut.putExtra("Target", "Checkout");
            checkOut.putExtra("Target", "Checkout");
            startActivity(checkOut);

        } else if ((session.getString("APITOKEN", null)) != null || controller.getCart().getCartItems().size() == 0) {
            Toast.makeText(CartActivity.this, "There is no Products in Cart", Toast.LENGTH_SHORT).show();
        }
    }

    private void orderDetails() {
        Intent checkOut = new Intent(CartActivity.this, CheckOutActivity.class);
        checkOut.putExtra("Target", "Show");
        startActivity(checkOut);
    }

    private void prepareToShowCart(String target) {
        if (target.equals("Cart")) {
            cartAdapter = new CartAdapter(this, controller.getCart().getCartItems(), target);
        }
        if (target.equals("COMPARE")) {
            cartAdapter = new CartAdapter(this, controller.getCompare().getCompareItems(), target);
        }

        cartAdapter.deleget = this;
        cartreRecyclerView.setAdapter(cartAdapter);
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void getOrderDetails(int order_id) {

        callgetMyOrdersDetailsService(order_id).enqueue(new Callback<MyOrdersAnswerRespons>() {
            @Override
            public void onResponse(Call<MyOrdersAnswerRespons> call, Response<MyOrdersAnswerRespons> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {

                    if (response.body().getStatus() == 200) {
                        orderdetails = response.body().getOrder();
                        orderProducts = response.body().getOrder().getProduct();
                        cartAdapter = new CartAdapter(CartActivity.this, orderProducts, "Order");
                        cartreRecyclerView.setAdapter(cartAdapter);
                        txttotal.setText(orderdetails.getTotalPrice().toString());
                        carttotalitems.setText("(" + orderProducts.size() + ")");

                    } else if (response.body().getStatus() == 402) {
                        //something is required
                        Toast.makeText(CartActivity.this, "something is required for Product details", Toast.LENGTH_SHORT).show();
                        progressDialog.cancel();
                    }


                } else {
                    int statusCode = response.code();
                    // handle request errors depending on status code
                    Toast.makeText(CartActivity.this, "Connection failed Product details", Toast.LENGTH_SHORT).show();
                    progressDialog.cancel();

                }
            }

            @Override
            public void onFailure(Call<MyOrdersAnswerRespons> call, Throwable t) {


                Toast.makeText(CartActivity.this, "Connection failed Product details", Toast.LENGTH_SHORT).show();
                progressDialog.cancel();

            }

        });

    }

    private Call<MyOrdersAnswerRespons> callgetMyOrdersDetailsService(int order_id) {
        SharedPreferences session = CartActivity.this.getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apiToken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(CartActivity.this);
        return mService.getOrderdetails(apiToken, order_id, lang);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        progressDialog.setCancelable(true);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        total = 0.0;
        if(target.equals("Cart")) {
            for (int x = 0; x < controller.getCart().getCartItems().size(); x++) {

                if (controller.getCart().getCartItems().get(x).getMount() == 0) {
                    controller.getCart().getCartItems().get(x).setMount(1);
                }

                if (controller.getCart().getCartItems().get(x).getPrice_after_discount() != null && controller.getCart().getCartItems().get(x).getPrice_after_discount() > 0) {

                    total = total + (controller.getCart().getCartItems().get(x).getPrice_after_discount() * controller.getCart().getCartItems().get(x).getMount());

                } else {
                    total = total + (controller.getCart().getCartItems().get(x).getPrice() * controller.getCart().getCartItems().get(x).getMount());
                }
            }
        }else if(target.equals("COMPARE")) {
            for (int x = 0; x < controller.getCompare().getCompareItems().size(); x++) {

                if (controller.getCompare().getCompareItems().get(x).getMount() == 0) {
                    controller.getCompare().getCompareItems().get(x).setMount(1);
                }

                if (controller.getCompare().getCompareItems().get(x).getPrice_after_discount() != null && controller.getCompare().getCompareItems().get(x).getPrice_after_discount() > 0) {

                    total = total + (controller.getCompare().getCompareItems().get(x).getPrice_after_discount() * controller.getCompare().getCompareItems().get(x).getMount());

                } else {
                    total = total + (controller.getCompare().getCompareItems().get(x).getPrice() * controller.getCompare().getCompareItems().get(x).getMount());
                }
            }
        }
        txttotal.setText(total + "");
        if (cartAdapter != null) {
            cartAdapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onSaveAddress(Integer Address_id, String addressTxt, Government government, String goven_name) {

    }

    @Override
    public void onDeleteCartitem(Double deleted_item_price, String operation, int amount) {
        if (operation.equals("minus")) {
            total = total - deleted_item_price;
            txttotal.setText(total + "");
            carttotalitems.setText("(" + controller.getCart().getCartItems().size() + ")");
        } else if (operation.equals("plus")) {
            total = total + deleted_item_price;
            txttotal.setText(total + "");
            carttotalitems.setText("(" + controller.getCart().getCartItems().size() + ")");
        } else if (operation.equals("delete")) {
            total = total - (deleted_item_price * amount);
            txttotal.setText(total + "");
            carttotalitems.setText("(" + controller.getCart().getCartItems().size() + ")");
        }
    }
}
