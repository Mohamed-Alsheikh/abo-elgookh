package com.abouelgoukh.sporteye.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.abouelgoukh.sporteye.Deleget;
import com.abouelgoukh.sporteye.Interfaces.IPaymentRequestCallBack;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.bean.Controller;
import com.abouelgoukh.sporteye.dialog.AddAddress;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.helper.PayFortData;
import com.abouelgoukh.sporteye.helper.PayFortPayment;
import com.abouelgoukh.sporteye.helper.PayFortValUPayment;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Address;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.AddressAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.Address_model.Government;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.Product;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.utils.Validation;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager;
import com.payfort.fort.android.sdk.base.callbacks.FortCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutActivity extends AppCompatActivity implements IPaymentRequestCallBack, AdapterView.OnItemSelectedListener, View.OnClickListener, Deleget {

    ApiServiceInterface apiServiceInterface;
    Spinner address_spinner;
    ImageView spinnericon;
    ImageView paymentspinnericon;
    Spinner govern_spinner;
    Spinner payment_spinner;
    ArrayAdapter<String> address_Adapter;
    ArrayAdapter<String> govern_Adapter;
    ArrayAdapter<String> payment_Adapter;
    TextView shipping_cost;
    TextView sub_total;
    TextView total;
    TextView shipping_time;
    Button btn_Submit;
    ImageButton add_address, refresh_checkout_gov;
    EditText mobile_number, alter_mobile_number;
    int address_id;
    String subtotal;
    String Target;
    int pyment_methodint;
    String apiToken;
    String order_code;
    String EMAIL;
    String mobile_number_str;
    ArrayList<Product> items;
    ProgressDialog progressDialog;
    List<Address> addressList = new ArrayList<>();
    List<String> addressListstr = new ArrayList<>();
    List<String> governListstr = new ArrayList<>();
    List<String> paymentMethodListstr = new ArrayList<>();
    public FortCallBackManager fortCallback = null;
    PayFortData payFortData;
    private int insatllmentcode = 333;
    private int RESULT_OK = 200;
    private int RESULT_CANCEL = 111;
    Resources resources;
    Call<AddressAnswerResponse> getAdresses;
    JsonObject main;
    int retry_counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         LocaleHelper.updateResources(this, LocaleHelper.getLanguage(CheckOutActivity.this));

        setContentView(R.layout.activity_check_out);
        subtotal = getIntent().getStringExtra("subtotal");
        Target = getIntent().getStringExtra("Target");

        apiServiceInterface = ApiUtils.getRetrofitObject();
        progressDialog = new ProgressDialog(CheckOutActivity.this);
        progressDialog.setMessage("Processing your Data\nPlease wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);
        progressDialog.show();
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (getAdresses != null) {
                    getAdresses.cancel();
                    onBackPressed();
                }
            }
        });
        Toolbar toolbar =  findViewById(R.id.checkoutttoolbar);
        setSupportActionBar(toolbar);


        if (LocaleHelper.getLanguage(this).equals("en")) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        } else if (LocaleHelper.getLanguage(this).equals("ar")) {
            toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_forward_24);
        }

        toolbar.setTitleTextColor(0xFFFFFFFF);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        LinearLayout d = (LinearLayout) findViewById(R.id.layoutaddressspinner);
        address_spinner = (Spinner) findViewById(R.id.addressspinner);

        spinnericon = (ImageView) findViewById(R.id.spinnericon);
        paymentspinnericon = (ImageView) findViewById(R.id.paymentspinnericon);

        govern_spinner = (Spinner) findViewById(R.id.governspinner);
        payment_spinner = (Spinner) findViewById(R.id.paymentMethodspinner);
        shipping_cost = (TextView) findViewById(R.id.ShippingCost);
        sub_total = (TextView) findViewById(R.id.subtotal);
        total = (TextView) findViewById(R.id.ordertotalprice);
        shipping_time = (TextView) findViewById(R.id.ShippingTime);
        btn_Submit = (Button) findViewById(R.id.btn_Submit);
        add_address = (ImageButton) findViewById(R.id.add_address);
        refresh_checkout_gov = (ImageButton) findViewById(R.id.refresh_checkout_gov);
        mobile_number = (EditText) findViewById(R.id.mobile_Number);
        alter_mobile_number = (EditText) findViewById(R.id.alternativemobile);

        btn_Submit.setOnClickListener(this);
        add_address.setOnClickListener(this);
        refresh_checkout_gov.setOnClickListener(this);

        govern_spinner.setEnabled(false);

        if (Target.equals("Checkout")) {
            paymentMethodListstr.add("Credit Cards");
            paymentMethodListstr.add("Installment");
//            paymentMethodListstr.add("VALU");
            paymentMethodListstr.add("Cash on Delivery");
            sub_total.setText(subtotal);
            Double subtotalCost = Double.parseDouble(subtotal);
            total.setText(subtotalCost + "");
            setRequiredValues();
            initilizePayFortSDK();
            loadAddresses();
        } else if (Target.equals("Show")) {
            btn_Submit.setText("OK");
            address_spinner.setEnabled(false);
            spinnericon.setEnabled(false);
            d.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            govern_spinner.setEnabled(false);
            payment_spinner.setEnabled(false);
            mobile_number.setEnabled(false);
            add_address.setVisibility(View.INVISIBLE);
            refresh_checkout_gov.setVisibility(View.INVISIBLE);
            alter_mobile_number.setEnabled(false);
            addressListstr.add(CartActivity.orderdetails.getAddress());
            governListstr.add(CartActivity.orderdetails.getGovernment());
            if (CartActivity.orderdetails.getPaymentMethod().equals("1")) {
                paymentMethodListstr.add("Credit Cards");
            } else if (CartActivity.orderdetails.getPaymentMethod().equals("2")) {
                paymentMethodListstr.add("Installment");
            }
//            else if (CartActivity.orderdetails.getPaymentMethod().equals("3")) {
//                paymentMethodListstr.add("VALU");
//            }
            else if (CartActivity.orderdetails.getPaymentMethod().equals("3")) {
                paymentMethodListstr.add("Cash on Delivery");
            }
            mobile_number.setText(CartActivity.orderdetails.getMobileFirst());
            shipping_cost.setText(CartActivity.orderdetails.getShippingCost().toString());
            shipping_time.setText(CartActivity.orderdetails.getShippingTime().toString());
            sub_total.setText(CartActivity.orderdetails.getTotalPrice().toString());
            int totalPrice = Integer.parseInt(CartActivity.orderdetails.getShippingCost()) + CartActivity.orderdetails.getTotalPrice();
            total.setText(totalPrice + "");
            setRequiredValues();
            progressDialog.dismiss();
        }
        spinnericon.setOnClickListener(this);
        paymentspinnericon.setOnClickListener(this);
        TextView userAddress = (TextView) findViewById(R.id.userAddress);

    }

    private void initilizePayFortSDK() {
        fortCallback = FortCallback.Factory.create();
    }

    public void showDropDown() {
        address_spinner.performClick();
    }

    public void showPaymentMethodDropDown() {
        payment_spinner.performClick();
    }

    public void loadAddresses() {
        getAdresses = getAddressesService();

        getAdresses.enqueue(new Callback<AddressAnswerResponse>() {
            @Override
            public void onResponse(Call<AddressAnswerResponse> call, Response<AddressAnswerResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        addressList = response.body().getAddress();
                        prepareAddressAndGoven(addressList);
                        Toast.makeText(CheckOutActivity.this, "the data is rendered", Toast.LENGTH_SHORT).show();

                    } else if (response.body().getStatus() == 403) {
                        Toast.makeText(CheckOutActivity.this, "there is no data", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressAnswerResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(CheckOutActivity.this, "there is error please refresh", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<AddressAnswerResponse> getAddressesService() {
        SharedPreferences session = getSharedPreferences("Session", Context.MODE_PRIVATE);
        String apitoken = session.getString("APITOKEN", null);
        String lang = LocaleHelper.getLanguage(CheckOutActivity.this);;
        return apiServiceInterface.getUserAddresses(apitoken, lang);
    }


    private void prepareAddressAndGoven(List<Address> address) {
        governListstr.clear();
        addressListstr.clear();
        for (int x = 0; x < addressList.size(); x++) {
            governListstr.add(addressList.get(x).getGovernment().getName());
            addressListstr.add(address.get(x).getAddress());
        }
        address_Adapter.notifyDataSetChanged();
        govern_Adapter.notifyDataSetChanged();
    }


    private void setRequiredValues() {


        address_Adapter = new ArrayAdapter<>(CheckOutActivity.this, android.R.layout.simple_spinner_item, addressListstr);
        govern_Adapter = new ArrayAdapter<>(CheckOutActivity.this, android.R.layout.simple_spinner_item, governListstr);
        payment_Adapter = new ArrayAdapter<>(CheckOutActivity.this, android.R.layout.simple_spinner_item, paymentMethodListstr);

        addSpinnerAdapter(address_spinner, address_Adapter);
        addSpinnerAdapter(govern_spinner, govern_Adapter);
        addSpinnerAdapter(payment_spinner, payment_Adapter);


    }

    private void addSpinnerAdapter(Spinner spinner, ArrayAdapter<String> Adapter) {


        // Drop down layout style - list view with radio button
        Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(Adapter);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (Target.equals("Checkout")) {
            switch (parent.getId()) {
                case R.id.addressspinner:

                    govern_spinner.setSelection(parent.getSelectedItemPosition());
                    shipping_cost.setText(addressList.get(parent.getSelectedItemPosition()).getGovernment().getShippingCost());
                    sub_total.setText(subtotal);
                    int shippingCost = Integer.parseInt(addressList.get(parent.getSelectedItemPosition()).getGovernment().getShippingCost());
                    Double subtotalCost = Double.parseDouble(subtotal);
                    total.setText(shippingCost + subtotalCost + "");
                    shipping_time.setText(addressList.get(parent.getSelectedItemPosition()).getGovernment().getShipping_time());
                    break;

            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Submit:
                if (Target.equals("Checkout")) {
                    submitAction();
                    break;
                } else if (Target.equals("Show")) {
                    okAction();
                    break;
                }
                break;
            case R.id.add_address:
                openAddressDialog();
                break;
            case R.id.spinnericon:
                showDropDown();
                break;
            case R.id.paymentspinnericon:
                showPaymentMethodDropDown();
                break;
            case R.id.refresh_checkout_gov:
                progressDialog.show();
                loadAddresses();
                break;
            default:
                break;

        }
    }

    private void okAction() {
        super.onBackPressed();
    }

    private void submitAction() {

        Controller controller = (Controller) getApplicationContext();
        apiToken = controller.getprefrancesUserApiToken();
        EMAIL = controller.getprefrancesUser_email();

        if (addressList != null && addressList.size() > 0) {
            address_id = addressList.get(address_spinner.getSelectedItemPosition()).getId();
        } else {
            openAddressDialog();
            Toast.makeText(CheckOutActivity.this, "there is no Addresses \n Please Add New Address", Toast.LENGTH_SHORT).show();
            return;
        }

        mobile_number_str = mobile_number.getText().toString();
        String alter_mobile_number_str = alter_mobile_number.getText() + "";
        items = controller.getCart().getCartItems();
        if (!Validation.validate_mobile(mobile_number_str)) {
            mobile_number.setError("Invalid Mobile Number Format");
            return;
        }
        order_code = String.valueOf(System.currentTimeMillis());

        if (payment_spinner.getSelectedItemPosition() == 0) {
            pyment_methodint = 1;//Credit Cards
        } else if (payment_spinner.getSelectedItemPosition() == 1) {
            pyment_methodint = 2;//Installment
            if (!TextUtils.isEmpty(total.getText().toString())) {
                float purchaseTotal= Float.parseFloat(total.getText().toString());
                if(purchaseTotal<500) {
                    Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.minimumInstallmentValue), Toast.LENGTH_LONG).show();
                    return;
                }
            }else {
                Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.minimumInstallmentValue), Toast.LENGTH_LONG).show();
                return;
            }

        } else if (payment_spinner.getSelectedItemPosition() == 2) {
            pyment_methodint = 3;//cash on Delivery
        }
        progressDialog.show();
        submitOrder(apiToken, order_code, address_id, pyment_methodint, mobile_number_str, items);
    }

    private void requestForPayfortInstallmentPayment() {
        Intent installment = new Intent(CheckOutActivity.this, InstallmentActivity.class);
        installment.putExtra("amount", String.valueOf((int) (Float.parseFloat(subtotal) * 100)));
        startActivityForResult(installment, insatllmentcode);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PayFortPayment.RESPONSE_PURCHASE) {
            if (data != null) {
                fortCallback.onActivityResult(requestCode, resultCode, data);
            }
        } else if (requestCode == insatllmentcode) {
            if (resultCode == RESULT_OK) {
//                progressDialog.show();
                Toast.makeText(CheckOutActivity.this, "payment Process done", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCEL) {
                Toast.makeText(CheckOutActivity.this, "you payment canceled", Toast.LENGTH_SHORT).show();
            }
            openMyOrders();
        }
    }

    private void requestForPayfortPayment() {
        payFortData = new PayFortData();
        if (!TextUtils.isEmpty(total.getText().toString())) {
            payFortData.amount = String.valueOf((int) (Float.parseFloat(total.getText().toString()) * 100));// Multiplying with 100, bcz amount should not be in decimal format
//            payFortData.amount = String.valueOf((int) (Float.parseFloat(subtotal) * 100));// Multiplying with 100, bcz amount should not be in decimal format
            payFortData.command = PayFortPayment.PURCHASE;
            payFortData.currency = PayFortPayment.CURRENCY_TYPE;
            payFortData.customerEmail = EMAIL;
            payFortData.language = PayFortPayment.LANGUAGE_TYPE;
            payFortData.merchantReference = order_code;
            PayFortPayment payFortPayment = new PayFortPayment(this, this.fortCallback, this);
            payFortPayment.requestForPayment(payFortData);

        }
    }

    private void requestForPayfortValUPayment() {
        payFortData = new PayFortData();
        if (!TextUtils.isEmpty(total.getText().toString())) {
            payFortData.amount = String.valueOf((int) (Float.parseFloat(total.getText().toString()) * 100));// Multiplying with 100, bcz amount should not be in decimal format
            payFortData.command = PayFortValUPayment.PURCHASE;
            payFortData.currency = PayFortValUPayment.CURRENCY_TYPE;
            payFortData.customerEmail = EMAIL;
            payFortData.language = PayFortValUPayment.LANGUAGE_TYPE;
            payFortData.setProducts(items);
            payFortData.merchantReference = String.valueOf(System.currentTimeMillis());
            PayFortValUPayment payFortPayment = new PayFortValUPayment(this, this.fortCallback, this);
            payFortPayment.requestForValUPayment(payFortData);

        }
    }

    private void openAddressDialog() {
        // to use the to decorate it
//       AddAddress addAddress=new AddAddress(CheckOutActivity.this,R.style.DialogTheme);
        AddAddress addAddress = new AddAddress(CheckOutActivity.this, "Ckeckout");
        addAddress.deleget = this;
        addAddress.show();

        WindowManager manager = (WindowManager) getSystemService(Activity.WINDOW_SERVICE);
        int width, height;
        Toolbar.LayoutParams params;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
            width = manager.getDefaultDisplay().getWidth() - 100;
            height = (manager.getDefaultDisplay().getHeight()) - 420;
        } else {
            Point point = new Point();
            manager.getDefaultDisplay().getSize(point);
            width = point.x;
            height = (point.y) - 420;
        }

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(addAddress.getWindow().getAttributes());
        lp.width = width;
        lp.height = height;
        addAddress.getWindow().setAttributes(lp);
    }

    public void onSaveAddress(Integer Address_id, String addressTxt, Government government, String goven_name) {
        Address addres = new Address();
        addres.setId(Address_id);
        addres.setAddress(addressTxt);
        addres.setGovernment(government);
        addressList.add(addres);

        governListstr.add(goven_name);
        addressListstr.add(addressTxt);

        address_Adapter.notifyDataSetChanged();
        govern_Adapter.notifyDataSetChanged();
        Toast.makeText(CheckOutActivity.this, addressTxt + "address", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onDeleteCartitem(Double deleted_item_price, String operation, int amount) {

    }


    private void updateOrderStatus() {
        final Call<SimpleResponse> orderStatus = updateOrderStatus(apiToken, order_code);
        orderStatus.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
//                Log.d("ccccccccccc", response.body().toString());
                progressDialog.dismiss();
                if (response.isSuccessful()) {
//                    Toast.makeText(CheckOutActivity.this, "response" + response.body().getStatus(), Toast.LENGTH_SHORT).show();
                    if (response.body().getStatus() == 200) {
                        Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.order_status_changed), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.order_status_not_changed), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.order_status_changed), Toast.LENGTH_SHORT).show();
                }

                openMyOrders();
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.order_status_changed), Toast.LENGTH_SHORT).show();
                openMyOrders();
            }

        });

    }
    private void submitOrder(String apiToken, String order_code, int address_id, final int pyment_methodint, String mobile_number_str, final ArrayList<Product> items) {
        final Call<SimpleResponse> getAdresses = makeOrder(apiToken, order_code, address_id, pyment_methodint, mobile_number_str, items);
        getAdresses.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
//                Log.d("ccccccccccc", response.body().toString());
                progressDialog.dismiss();
                if (response.isSuccessful()) {
//                    Toast.makeText(CheckOutActivity.this, "response" + response.body().getStatus(), Toast.LENGTH_SHORT).show();
                    if (response.body().getStatus() == 200) {
                        Toast.makeText(CheckOutActivity.this, getResources().getString(R.string.order_registered), Toast.LENGTH_SHORT).show();
                        Controller controller = (Controller) CheckOutActivity.this.getApplicationContext();
                        SharedPreferences Cartsession = getSharedPreferences("Cart", Context.MODE_PRIVATE);
                        SharedPreferences.Editor sessionEditor = Cartsession.edit();
                        sessionEditor.clear().apply();
                        controller.getCart().getCartItems().clear();
                        controller.cartOfProductsID.clear();
                        controller.cartOfProductsTypes.clear();
                        MainActivity.setupMessagesBadge(controller.getcartProductsArraylistsize());
                        SharedPreferences session = getSharedPreferences("Session", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = session.edit();
                        editor.remove("order_code");
                        editor.apply();
                        if (pyment_methodint == 1) {
                            //Credit Cards
                            requestForPayfortPayment();
                        } else if (pyment_methodint == 2) {
                            //Installment
                            if (!TextUtils.isEmpty(total.getText().toString())) {
                                Float purchaseTotal= Float.parseFloat(total.getText().toString());
                                if(purchaseTotal>=500)
                                requestForPayfortInstallmentPayment();
                                else
                                    Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.minimumInstallmentValue), Toast.LENGTH_LONG).show();
                            }else
                                Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.minimumInstallmentValue), Toast.LENGTH_LONG).show();

                        } else if (pyment_methodint == 3) {
                            //cash on Delivery
                            showalert();
                        }
                        retry_counter = 0;
                    } else {
                        Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.order_not_registered), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (pyment_methodint == 1) {
                        Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.order_not_registered), Toast.LENGTH_SHORT).show();
                    } else if (pyment_methodint < 1) {
                        if (retry_counter < 2) {
                            doretryRegsterOrder();
                        } else {
                            doWhenErrorOcuredTwoTimes();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                if (pyment_methodint == 1) {
                    Toast.makeText(CheckOutActivity.this, getResources().getText(R.string.order_not_registered), Toast.LENGTH_SHORT).show();
                } else if (pyment_methodint < 1) {
                    if (retry_counter < 2) {
                        doretryRegsterOrder();
                    } else {
                        doWhenErrorOcuredTwoTimes();
                    }
                }
            }

        });
    }

    void doretryRegsterOrder() {
        Gson gson = new Gson();
        SharedPreferences session = getSharedPreferences("Session", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = session.edit();
        String orderData = gson.toJson(main);
        editor.putString("order_code", orderData);
        editor.commit();
//                        Toast.makeText(CheckOutActivity.this, "there is error in connection when go online your order will register", Toast.LENGTH_SHORT).show();
        showMassage();
        retry_counter++;
    }

    void doWhenErrorOcuredTwoTimes() {
        Toast.makeText(CheckOutActivity.this, "there is error in connection when go online your order will register", Toast.LENGTH_SHORT).show();
        Controller controller = (Controller) CheckOutActivity.this.getApplicationContext();
        SharedPreferences Cartsession = getSharedPreferences("Cart", Context.MODE_PRIVATE);
        SharedPreferences.Editor sessionEditor = Cartsession.edit();
        sessionEditor.clear().commit();
        controller.getCart().getCartItems().clear();
        controller.cartOfProductsID.clear();
        controller.cartOfProductsTypes.clear();
        MainActivity.setupMessagesBadge(controller.getcartProductsArraylistsize());
    }

    void showMassage() {
        new AlertDialog.Builder(CheckOutActivity.this)
                .setTitle("We're Sorry")
                .setIcon(R.drawable.ic_error_outline_black_24dp)
                .setMessage("An error has occurred. Please try again")
                .setCancelable(true)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        submitOrder(apiToken, order_code, address_id, pyment_methodint, mobile_number_str, items);
                    }
                }).show();
    }
    private Call<SimpleResponse> updateOrderStatus(String apiToken, String order_code) {


        Gson gson = new Gson();
        main = new JsonObject();

        main.addProperty("apiToken", apiToken);
        main.addProperty("order_code", order_code);
        main.addProperty("new_status", 1);

        return apiServiceInterface.updateOrderStatus(main);
    }
    private Call<SimpleResponse> makeOrder(String apiToken, String order_code, int address_id, int pyment_methodint, String mobile_number_str, ArrayList<Product> items) {


        Gson gson = new Gson();
        main = new JsonObject();

        main.addProperty("apiToken", apiToken);
        main.addProperty("order_code", order_code);
        main.addProperty("merchant_reference", order_code);
        main.addProperty("address_id", address_id);
        main.addProperty("payment_method", pyment_methodint);
        main.addProperty("mobile_first", mobile_number_str);
        main.addProperty("date", "" + new Date());
        main.add("items", gson.toJsonTree(items));
        return apiServiceInterface.makeOrder(main);
    }

    private void showalert() {

        final AlertDialog.Builder adb = new AlertDialog.Builder(this);


        adb.setTitle("Your Order Is Ready");

        //adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                openMyOrders();
            }
        });


        adb.show();
    }


    @Override
    public void onPaymentRequestResponse(int responseType, PayFortData responseData) {
        if (responseType == PayFortPayment.RESPONSE_GET_TOKEN) {
            Toast.makeText(this, "Token not generated", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Token not generated");
            openMyOrders();
        } else if (responseType == PayFortPayment.RESPONSE_PURCHASE_CANCEL) {
            Toast.makeText(this, getResources().getString(R.string.payment_cancelled), Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment cancelled");
            openMyOrders();
        } else if (responseType == PayFortPayment.RESPONSE_PURCHASE_FAILURE) {
            Toast.makeText(this, getResources().getString(R.string.payment_failed) + ": " + responseData.responseMessage, Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment failed");
            openMyOrders();
        } else if (responseType == PayFortValUPayment.RESPONSE_CustomerNOT_REGISTERD_FOR_VALU) {
            Toast.makeText(this, getResources().getString(R.string.payment_failed) + ": " + responseData.responseMessage, Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment failed");
            openMyOrders();
        } else {
//            Handler handler = new Handler();
//            Runnable runnable = new Runnable() {
//                @Override
//                public void run() {
            Toast.makeText(this, getResources().getString(R.string.payment_success), Toast.LENGTH_SHORT).show();
            updateOrderStatus();
//                }
//            };
//            handler.postDelayed(runnable, 10000);
//            Toast.makeText(this, "Payment successful", Toast.LENGTH_SHORT).show();
//            Log.e("onPaymentResponse", "Payment successful");
        }
    }
private void openMyOrders(){
    Intent intent=new Intent(CheckOutActivity.this, MainActivity.class);
    intent.putExtra("openOrders", true);
    startActivity(intent);
    finish();
}
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
