package com.abouelgoukh.sporteye.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.Locale;
import java.util.UUID;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.SMS.SMSVerificationActivity;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.login_registration.Register;
import com.abouelgoukh.sporteye.utils.Validation;

//@RuntimePermissions
public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {


    Button btn_signup;
    TextView txt_login;
    EditText txt_email, txt_mobile_number, txt_password, txt_confirm_password, txt_UserName;
    private String var_email, var_mobile_number, var_password, var_confirm_password, var_UserName;
    ProgressDialog progressDialog;
    ApiServiceInterface serviceInterface;
    Call<Register> RegisterService;
    View signupscrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(SignUpActivity.this));
        setContentView(R.layout.activity_sign_up);

        signupscrollView = findViewById(R.id.signupscrollView);
        signupscrollView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(SignUpActivity.this);
            }
        });
        btn_signup = (Button) findViewById(R.id.btn_signup);
        txt_login = (TextView) findViewById(R.id.txt_login);
        txt_email = (EditText) findViewById(R.id.txt_email);
        txt_mobile_number = (EditText) findViewById(R.id.txt_mobil_number);
        txt_UserName = (EditText) findViewById(R.id.txt_userName);
        txt_password = (EditText) findViewById(R.id.txt_password);
        txt_confirm_password = (EditText) findViewById(R.id.txt_password_confirm);

        serviceInterface = ApiUtils.getRetrofitObject();
        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setMessage("Please Wait ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (RegisterService != null && RegisterService.isExecuted()) {
                    RegisterService.cancel();
                    Toast.makeText(SignUpActivity.this, "Login canceled", Toast.LENGTH_SHORT).show();

                }
            }
        });
        btn_signup.setOnClickListener(this);
        txt_login.setOnClickListener(this);
    }

    public void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
//             Intent intent=new Intent(SignUpActivity.this,SMSVerificationActivity.class);
//             startActivity(intent);
//                SignUpActivityPermissionsDispatcher.excuteRegisterWithPermissionCheck(this);
                excuteRegister();

                break;

            case R.id.txt_login:
                onBackPressed();
                break;

        }
    }

//    @NeedsPermission(Manifest.permission.READ_PHONE_STATE)
    public void excuteRegister() {
        String deviceID = getUniqueDeviceID(this);
        var_email = txt_email.getText().toString();
        var_mobile_number = txt_mobile_number.getText().toString();
        var_mobile_number = txt_mobile_number.getText().toString();
        var_UserName = txt_UserName.getText().toString();
        var_password = txt_password.getText().toString();
        var_confirm_password = txt_confirm_password.getText().toString();

        if (!Validation.validate_Email(var_email)) {
            txt_email.setError(getResources().getString(R.string.Invalid_email ));
            return;
        }
        if (!Validation.validate_name(var_UserName)) {
            txt_UserName.setError(getResources().getString(R.string.Invalid_user_name ));
            return;
        }
        if (!Validation.validate_mobile(var_mobile_number)) {
            txt_mobile_number.setError(getResources().getString(R.string.Invalid_Mobile_Number) );
            return;
        }
        if (!Validation.validate_Password(var_password)) {
            txt_password.setError(getResources().getString(R.string.passwordCond));
            return;
        }

        if (!var_confirm_password.equals(var_password)) {
            txt_confirm_password.setError(getResources().getString(R.string.passwordConfirmation));
            return;
        }

        register(deviceID);
    }

    private void register(String deviceID) {
        progressDialog.show();
        RegisterService = callRegisterService(deviceID);

        RegisterService.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {

                    switch (response.body().getStatus()) {
                        case 200:
                            //success
                            SharedPreferences session = getSharedPreferences("Session", Context.MODE_PRIVATE);
                            SharedPreferences.Editor sessionEditor = session.edit();
                            sessionEditor.putString("EMAIL", var_email);
                            sessionEditor.putString("UserName", var_UserName);
                            sessionEditor.putString("MOBILE", var_mobile_number);
                            sessionEditor.putBoolean("isActive", false);
                            sessionEditor.putBoolean("isVerified", false);
                            sessionEditor.apply();
                            Intent intent = new Intent(SignUpActivity.this, SMSVerificationActivity.class);
                            intent.addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                            Toast.makeText(SignUpActivity.this, "User Successfully Registered", Toast.LENGTH_SHORT).show();
                            break;
                        case 402:
                            //something is required
                            Toast.makeText(SignUpActivity.this, " something is required  ", Toast.LENGTH_SHORT).show();
                            break;
                        case 401:
                            //the phone number is exists
                            Toast.makeText(SignUpActivity.this, "the phone number is exists sign in please", Toast.LENGTH_SHORT).show();
                            Intent loginintent = new Intent(SignUpActivity.this, LoginActivity.class);
                            startActivity(loginintent);
                            break;
                            case 412:
                            //the phone number is exists
                            Toast.makeText(SignUpActivity.this, "you can't create account", Toast.LENGTH_SHORT).show();
                            break;

                    }
                } else {
                    Toast.makeText(SignUpActivity.this, "response failed", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    private Call<Register> callRegisterService(String deviceID) {
        return serviceInterface.register(deviceID, var_email, var_mobile_number, var_password,var_UserName);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        SharedPreferences session = getSharedPreferences("Session", MODE_PRIVATE);
//        if (session.getBoolean("isVerified", false)) {
//            super.onBackPressed();
//            MainActivity.txtWebsite.setText(var_email);
//
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public String getUniqueDeviceID(Activity activity) {
            return  Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

//    @OnPermissionDenied(Manifest.permission.READ_PHONE_STATE)
//    void showDeniedForPhoneState() {
//
//        Toast.makeText(this, "تاكد من اعطاء الصلاحيات للبرنامج ", Toast.LENGTH_SHORT).show();
//    }
//
//    @OnNeverAskAgain(Manifest.permission.READ_PHONE_STATE)
//    void showneverAskAgainForPhoneState() {
//
//        Toast.makeText(this, "تاكد من اعطاء الصلاحيات للبرنامج ", Toast.LENGTH_SHORT).show();
//    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        // NOTE: delegate the permission handling to generated method
//        SignUpActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
//    }
}
