package com.abouelgoukh.sporteye.Activities.SMS;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Random;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.POST;
import retrofit2.http.Query;
import com.abouelgoukh.sporteye.Activities.MainActivity;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SharedProductModel.ProductAnswerResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.activate_acount.ActivationResponse;

public class SMSModel implements SMSContract.Model {
    SMSPresenter presenter;
    Context context;
    Retrofit retrofit;
    PostService postService;
    String Verification_num;
    ApiServiceInterface apiService;
    Call<ActivationResponse> callActivationResponse;
    SharedPreferences session;

    @Inject
    public SMSModel(SMSPresenter presenter, Context context, Retrofit retrofit) {
        session = context.getSharedPreferences("Session", Context.MODE_PRIVATE);
        this.presenter = presenter;
        this.context = context;
        this.retrofit = retrofit;
    }

    @Override
    public void activateAcount(String verificationCode) {

        String phone = session.getString("MOBILE", "");
        apiService = ApiUtils.getRetrofitObject();
        callActivationResponse = callActivationResponseService(verificationCode, phone);

        if (callActivationResponse != null) {

            callActivationResponse.enqueue(new Callback<ActivationResponse>() {
                @Override
                public void onResponse(Call<ActivationResponse> call, Response<ActivationResponse> response) {
                    presenter.view.showProgressDialode(false);
                    if (response.isSuccessful()) {

                        switch (response.body().getStatus()) {
                            case 200:
                                SharedPreferences.Editor sessionEditor = session.edit();
                                sessionEditor.putString("APITOKEN", response.body().getUser().getApiToken());
                                sessionEditor.putBoolean("isVerified", true);
                                sessionEditor.apply();
                                MainActivity.APIToken=response.body().getUser().getApiToken();
                                presenter.view.userVerifiedSuccessfully();
                                break;
                            case 402:
                                //something is required
                                presenter.view.showToast("Some thing is required");
                                break;
                            case 407:
                                //the code is incorrect
                                presenter.view.showToast("Invalid Verification Code");
                                break;
                            case 406:
                                presenter.view.showToast("Already Verified User");
                                break;

                        }
                    } else {
                        presenter.view.showToast("Connection Not success");
                    }
                }

                @Override
                public void onFailure(Call<ActivationResponse> call, Throwable t) {
                    presenter.view.showProgressDialode(false);
                    presenter.view.showToast("Error In Connection");
                }
            });
        }
    }

    private Call<ActivationResponse> callActivationResponseService(String verificationCode, String phone) {

        return apiService.ActivationResponseService(verificationCode, phone);
    }

    @Override
    public void sendSMS(String userName) {
        postService = retrofit.create(PostService.class);
        Call<SimpleResponse> call=callSMS_Service(userName);
        if (call != null) {
            call.enqueue(new Callback<SimpleResponse>() {
                @Override
                public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()==200) {
                            presenter.startDownCounter();
                        }
                    } else {
                        presenter.view.showToast("error try again");
                        presenter.doWhenFinish_OR_Error();
                    }
                }

                @Override
                public void onFailure(Call<SimpleResponse> call, Throwable t) {
                    presenter.view.showToast("error");
                    presenter.doWhenFinish_OR_Error();
                }
            });
        }
    }


    @Override
    public Call<SimpleResponse> callSMS_Service(String userName) {
        SharedPreferences session = context.getSharedPreferences("Session", Context.MODE_PRIVATE);
        String phone = "+2" + session.getString("MOBILE", "");

        return postService.sendCode( phone);
    }

    private interface PostService {
        @POST("/api/sendCode")
        Call<SimpleResponse> sendCode(@Query("mobile") String lan);

        Call<String> getPostList(@Query("username") String yourname, @Query("password") String password, @Query("message") String message, @Query("want_report") String want_report, @Query("msisdn") String msisdn);
    }
}
