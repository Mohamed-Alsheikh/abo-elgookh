package com.abouelgoukh.sporteye.Activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.abouelgoukh.sporteye.Activities.SMS.SMSVerificationActivity;
import com.abouelgoukh.sporteye.AppSignatureHelper;
import com.abouelgoukh.sporteye.R;
import com.abouelgoukh.sporteye.helper.LocaleHelper;
import com.abouelgoukh.sporteye.other.AndroidBug5497Workaround;
import com.abouelgoukh.sporteye.retrofit.ApiServiceInterface;
import com.abouelgoukh.sporteye.retrofit.ApiUtils;
import com.abouelgoukh.sporteye.retrofit.model_retrofit.login_registration.Login;
import com.abouelgoukh.sporteye.utils.Validation;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtsignup, forgetPassword;
    EditText txtuser_name, txtuser_password;
    Button btnlogin;
    ApiServiceInterface apiService;
    Call<Login> callLoginService;
    ProgressDialog progressDialog;
    View loginscrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocaleHelper.updateResources(this, LocaleHelper.getLanguage(LoginActivity.this));
        setContentView(R.layout.activity_login);
        AndroidBug5497Workaround.assistActivity(this);
        loginscrollView =  findViewById(R.id.loginscrollView);
        loginscrollView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(LoginActivity.this);
            }
        });
        txtsignup =  findViewById(R.id.txtsignup);
        forgetPassword =  findViewById(R.id.forgetPassword);
        btnlogin =  findViewById(R.id.btn_signin);

        txtuser_name =  findViewById(R.id.txtusername);
        txtuser_password =  findViewById(R.id.txtloginpassword);


        btnlogin.setOnClickListener(this);
        txtsignup.setOnClickListener(this);
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgetPasswordActivityIntent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                Bundle bundlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(),
                                R.anim.login_animation, R.anim.login_animation2).toBundle();

                startActivity(forgetPasswordActivityIntent, bundlanimation);
            }
        });


        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Login...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (callLoginService != null && callLoginService.isExecuted()) {
                    callLoginService.cancel();
                    Toast.makeText(LoginActivity.this, "Login canceled", Toast.LENGTH_SHORT).show();

                }
            }
        });


    }
    String user_Name;
    String user_Password;
    public static void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtsignup:
                Intent signupintent = new Intent(LoginActivity.this, SignUpActivity.class);
                Bundle bundlanimation =
                        ActivityOptions.makeCustomAnimation(getApplicationContext(),
                                R.anim.login_animation, R.anim.login_animation2).toBundle();
//                signupintent.setFlags(signupintent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(signupintent, bundlanimation);
                break;

            case R.id.btn_signin:
                if (MainActivity.controller.isNetworkAvailable()) {
                    hideSoftKeyboard(LoginActivity.this);
                     user_Name = txtuser_name.getText().toString();
                     user_Password = txtuser_password.getText().toString();
                    if (!Validation.validate_mobile(user_Name)) {
                        txtuser_name.setError("Invalid Mobile Number");
                        break;
                    }
                    if (!Validation.validate_Password(user_Password)) {
                        txtuser_password.setError("Invalid Password");
                        break;
                    }
                    login(user_Name, user_Password);
                } else {
                    Toast.makeText(LoginActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

                }
                break;

            default:
                break;
        }

    }

    private void login(final String user_Name, String user_Password) {

        apiService = ApiUtils.getRetrofitObject();
        callLoginService = callRegisterService(user_Name, user_Password);
        if (callLoginService != null) {
            progressDialog.show();
            callLoginService.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {
                    progressDialog.dismiss();
                    if (response.isSuccessful()) {

                        switch (response.body().getStatus()) {
                            case 200:
                                //success
                                SharedPreferences session = getSharedPreferences("Session", Context.MODE_PRIVATE);
                                SharedPreferences.Editor sessionEditor = session.edit();
                                sessionEditor.putString("APITOKEN", response.body().getUser().getApiToken());
                                MainActivity.APIToken = response.body().getUser().getApiToken();
                                sessionEditor.putString("EMAIL", response.body().getUser().getEmail());
                                sessionEditor.putString("USER", response.body().getUser().getUsername());
                                MainActivity.txtWebsite.setText(response.body().getUser().getUsername());
                                sessionEditor.apply();

                                onBackPressed();
                                MainActivity.logout.setEnabled(true);
                                Toast.makeText(LoginActivity.this, "User Successfully Login", Toast.LENGTH_SHORT).show();
                                break;
                            case 402:
                                //something is required
                                Toast.makeText(LoginActivity.this, " something is required  ", Toast.LENGTH_SHORT).show();
                                break;
                            case 405:
                                //the password is incorrect
                                Toast.makeText(LoginActivity.this, "User Name Or mobile is Incorrect", Toast.LENGTH_SHORT).show();
                                break;
                            case 410:
                                //the password is incorrect
                                Toast.makeText(LoginActivity.this, "User Name Or mobile is Incorrect", Toast.LENGTH_SHORT).show();
                                break;  case 417:
                                //the password is incorrect
                                Toast.makeText(LoginActivity.this, "User Name Or mobile is Incorrect", Toast.LENGTH_SHORT).show();
                                break;
                                case 422:
                                //not verifies user
                                    SharedPreferences session2 = getSharedPreferences("Session", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor sessionEditor2 = session2.edit();
                                    sessionEditor2.putString("MOBILE", user_Name);
                                    sessionEditor2.apply();
                                    Intent intent = new Intent(LoginActivity.this, SMSVerificationActivity.class);
                                    intent.putExtra("UserName",user_Name);
                                    intent.addFlags(
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                break;

                                case 406:
                                    Toast.makeText(LoginActivity.this, "this User is blocked", Toast.LENGTH_SHORT).show();

                                    break;

                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "response failed", Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });
        }
    }

    private Call<Login> callRegisterService(String user_Name, String user_Password) {
        return apiService.login(user_Name, user_Password);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences session = getSharedPreferences("Session", MODE_PRIVATE);
        if (session.getBoolean("isVerified", false)) {
            super.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
