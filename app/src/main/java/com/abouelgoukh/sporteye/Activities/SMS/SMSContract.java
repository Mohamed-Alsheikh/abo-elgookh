package com.abouelgoukh.sporteye.Activities.SMS;

import android.app.Activity;

import com.abouelgoukh.sporteye.retrofit.model_retrofit.SimpleResponse;

import retrofit2.Call;

public interface SMSContract {
    public interface View {
        void changeWaitingCounterText(String counterValue);
        void showToast(String message);
        void changeArrowNavVisibility(boolean visible);
        void showProgressDialode(boolean show);
        void userVerifiedSuccessfully();
        void hideSoftKeyboard(Activity activity);
    }

    public interface Model {
        void sendSMS(String userName);
        Call<SimpleResponse> callSMS_Service(String userName);
        void activateAcount(String verificationCode);
    }

    public interface Presenter {
        void startDownCounter();
        void doWhenFinish_OR_Error();
        void sendSMS(String userName);
        void ValidateVerificationCode(String verificationCode);
        boolean isVerificationCodeValide(String verificationCode);
    }
}
